﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace WebDAVContent.GUID.Notifications
{
    class Notifications
    {
        //TODO Fazer uma fila mais inteligente
        private static Queue<Notification> notificationsQueue;

        private Notifications()
        {
            notificationsQueue = new Queue<Notification>();
        }

        private static Notifications instance;

        public static Notifications getInstance()
        {
            if (instance == null)
                instance = new Notifications();

            return instance;
        }

        public void Enqueue (Notification notification){
            notificationsQueue.Enqueue(notification);
        }

        public Notification Dequeue()
        {
            return notificationsQueue.Dequeue();
        }

        public bool hasNotificatios() {
            return notificationsQueue.Count > 0;
        }
    }
}
