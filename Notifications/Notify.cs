﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebDAVContent.GUID.Notifications
{
    class Notify
    {
        private static Notify instance;

        private Notifications notifications;

        private Notify() { 
            
        }

        public static Notify getInstance() {
            if (instance == null) { 
                instance = new Notify ();
            }
            return instance;
        }

        public void consumeNotifications () {
            Notifications notifications = Notifications.getInstance();
            while (true) {
                
                while (Notifications.getInstance().hasNotificatios()) {
                    Notification notification = notifications.Dequeue();
                    if (notification != null)
                    {
                        ConfigurationForm.getInstance().notifySystemBalloon(notification.TipIcon, notification.TextMessage, notification.TitleMessage, notification.IsVisible, notification.Timeout);
                    }
                }
                System.Threading.Thread.Sleep(10000);
            }
        }

    }
}
