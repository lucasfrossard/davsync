﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebDAVContent.GUID.Notifications
{
    class Notification
    {
        public Notification() : this(ToolTipIcon.None, String.Empty, String.Empty, true, 5000)
        {
        }

        public Notification(ToolTipIcon icon, string title, string text, bool visible, int atimeout)
        {
            tipIcon = icon;
            titleMessage = title;
            textMessage = text;
            isVisible = visible;
            timeout = atimeout;
        }

        private ToolTipIcon tipIcon;

        public ToolTipIcon TipIcon
        {
            get { return tipIcon; }
            set { tipIcon = value; }
        }
        private string titleMessage;

        public string TitleMessage
        {
            get { return titleMessage; }
            set { titleMessage = value; }
        }
        private string textMessage;

        public string TextMessage
        {
            get { return textMessage; }
            set { textMessage = value; }
        }
        private bool isVisible;

        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; }
        }
        private int timeout;

        public int Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }
    }
}
