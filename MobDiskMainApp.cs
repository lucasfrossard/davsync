﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace WebDAVContent.GUID
{
    class MobDiskMainApp
    {
        /**<summary> GUID único do processo</summary>*/
        private static string appGuid = "33873C79-E2F6-4999-9C6E-4D6D3AA824F3";

        /**<summary> Mensagem avisando que já tem um processo sendo executado </summary>*/
        private static readonly string PROCESS_ALREADY_RUNNING_MSG = "Já existe um processo do MobDisk sendo executado.";

        [STAThread]
        static void Main()
        {
            using (Mutex mutex = new Mutex(false, @"Global\" + appGuid))
            {
                if (!mutex.WaitOne(0, false))
                {
                    MessageBox.Show(PROCESS_ALREADY_RUNNING_MSG, "MobDisk", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                GC.Collect();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(ConfigurationForm.getInstance());
            }
        }
    }
}
