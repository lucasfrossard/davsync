﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WebDAVContent.Manager.Exceptions;
using WebDAVContent.Util;
using WebDAVContent.GUID.Sync;

namespace WebDAVContent.Manager
{
    class LocalClientFacade : IFileManagerFacade<FileProperties>
    {
        /** <summary> Instancia unica da Facade </summary>*/
        private static LocalClientFacade instance;

        /** <summary> Construtor privado - singleton </summary>*/
        private LocalClientFacade()
        {
        }

        /**
         * <summary>
         * Obtem instancia unica
         * </summary>
         * <returns>
         * Instancia unica da classe
         * </returns>
         */
        public static LocalClientFacade getInstance()
        {
            if (LocalClientFacade.instance == null)
            {
                LocalClientFacade.instance = new LocalClientFacade();

            }
            return LocalClientFacade.instance;
        }


        /**
         * <summary>
         * Lista os arquivos locais do diretorio. A lista é recursiva.
         * </summary>
         * <param name="pathDescriptor">
         * Diretorio base da lista.
         * </param>
         * <return>
         * Lista de arquivos. A chave eh o caminho relativo e o valor sao as propriedades do arquivo.
         * </return>
         */
        public IDictionary<string, FileProperties> listFilesProperties(PathDescriptor pathDescriptor, bool readResourcePropOnly)
        {
            string fullPath = FilePropertiesUtil.correctSlash(pathDescriptor.getFullPath());
            IDictionary<string, FileProperties> filesPropertiesList = new Dictionary<string, FileProperties>();
            if (Directory.Exists(fullPath))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(fullPath);
                FileProperties dirProperties = new FileProperties();
                string relativeDir = pathDescriptor.RelativePath;
                FilePropertiesUtil.export(dirInfo, relativeDir, dirProperties);
                filesPropertiesList.Add(dirProperties.RelativePath, dirProperties);
                if (!readResourcePropOnly)
                {
                    // arquivos e diretorios existentes nos caminho
                    IEnumerable<string> files = Directory.EnumerateFiles(fullPath);
                    // caminho para o arquivo
                    foreach (String filePath in files)
                    {
                        // obtem o nome do arquivo corrente
                        String[] splittedPath = filePath.Split('/');
                        // TODO obter o path relativo
                        String fileName = splittedPath[splittedPath.Length - 1];

                        // obtem propriedades do arquivo
                        FileProperties fileProperties = obtainFileProperties(pathDescriptor, filePath);
                        filesPropertiesList.Add(fileProperties.RelativePath, fileProperties);
                    }
                    // Lista diretorios
                    IEnumerable<string> directories = Directory.EnumerateDirectories(fullPath);
                    foreach (String d in directories)
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(d);
                        FileProperties fileProperties = new FileProperties();
                        string rootFolder = FilePropertiesUtil.correctSlash(pathDescriptor.RootFolder);
                        string relativeFilePath = d.Substring(rootFolder.Length, d.Length - rootFolder.Length) + @"\";
                        FilePropertiesUtil.export(directoryInfo, relativeFilePath, fileProperties);
                        filesPropertiesList.Add(fileProperties.RelativePath, fileProperties);

                        // Busca arquivos no diretorio;
                        PathDescriptor nextPath = PathDescriptor.generateSyncPathFromAbsolutePath("", pathDescriptor.RootFolder, relativeFilePath);
                        IDictionary<string, FileProperties> newList = this.listFilesProperties(nextPath, readResourcePropOnly);
                        foreach (FileProperties fp in newList.Values)
                        {
                            // TODO isso aqui ta burrão demais! Melhorar isso!
                            filesPropertiesList[fp.RelativePath]=fp;
                        }
                    }
                }
            }
            else if (File.Exists(fullPath)) {
                FileProperties fileProperties = obtainFileProperties(pathDescriptor, fullPath);
                filesPropertiesList.Add(fileProperties.RelativePath, fileProperties);
            }
            else
            {
                throw new InvalidPathDescriptorException("O caminho é invalido.");
            }
            return filesPropertiesList;
        }

        private FileProperties obtainFileProperties(PathDescriptor dir, String filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            FileProperties fileProperties = new FileProperties();
            string rootFolder = FilePropertiesUtil.correctSlash(dir.RootFolder);
            string relativeFilePath = filePath.Substring(rootFolder.Length, filePath.Length - rootFolder.Length);
            FilePropertiesUtil.export(fileInfo, relativeFilePath, fileProperties);
            return fileProperties;
        }

        public FileProperties sendFile(FileInfo file, PathDescriptor destinyFolder)
        {
            throw new NotImplementedException("This method is not implemented yet");
        }

        public void delete(PathDescriptor destiny)
        {
            string fullPath = FilePropertiesUtil.correctSlash(destiny.getFullPath());
            if (Directory.Exists(fullPath)) { 
                Directory.Delete(fullPath,true);
            }
            if (File.Exists(fullPath)) { 
                File.Delete(fullPath);
            }


        }

        public FileInfo receiveFile(PathDescriptor relativeSourceFolderPath, PathDescriptor filename, string relativeDestinyFolderPath)
        {
            throw new NotImplementedException("This method is not implemented yet");
        }

        public FileProperties createDir(PathDescriptor folder) 
        {
            string directory = FilePropertiesUtil.correctSlash(folder.getFullPath());
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            FileProperties dirProperties = new FileProperties();
            FilePropertiesUtil.export(dirInfo, folder, dirProperties);
            return dirProperties;
        }
    }
}
