﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Manager.Exceptions
{
    class InvalidPathDescriptorException : Exception
    {
        public InvalidPathDescriptorException(string errorMessage) : base(errorMessage) { 
        }
    }
}
 