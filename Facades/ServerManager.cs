﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WebDAVContent.Manager
{
    /**
     * Interface para envio de arquivos.
     * // FIXME em construção
     */
    interface ServerManager
    {
        /**
         * Faz upload síncrono (trava o recurso até que o download seja concluído) para o servidor.
         */
        void upload (FileInfo file, String destinyFolder);
        
        /**
         * Faz o upload assíncrono (sem travar o recurso) para o servidor.
         * // FIXME falta colocar o handler
         */
        void uploadAsync (FileInfo file, String destinyFolder);
        
        /**
         * Faz o download síncrono
         */
        FileInfo download (String sourceFolder);
        
        /**
         * Faz o download assíncrono
         */
        FileInfo downloadAsync (String sourceFolder);
        
        /**
         * Cria um novo diretório no servidor.
         */
        void createDir(String folder);
        
        /**
         * Deleta um arquivo no servidor.
         */
        void deleteFile (String destiny);

        /**
         * Copia um arquivo no servidor.
         */
        void copyFile(FileInfo files, String source, String destinyFolder);
    }
}
