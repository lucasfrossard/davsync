﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Sync;

namespace WebDAVContent.Manager
{
    public interface IFileManagerFacade<T> where T : IFileProperties
    {
        /** TODO alterar para que o destino do arquivo seja um objeto que implemente uma interface capaz 
        * de dar o comportamento adequado relativo ao local que o arquivo sera salvo. 
        * Por exemplo, se o arquivo fo ser salvo localmente ou em outro local na rede, este objeto sera capaz de faze-lo.
        * A priore pensei em um objeto que tenha os métodos getPath() - que retorna o local de escrita e getStreamWriter() - que retornar o local de escrita, ou apenas getStream()
        * <param name="relativeDownloadSourceFilePath">
        * Caminho relativo do arquivo a ser baixado.
        * </param>
        * <param name="relativeDestinyFolderPath">
        * Caminho relativo para a pasta em que o arquivo devera ser salvo.
        * </param>
        * <param name="destinyFileName">
        * Nome a ser dado ao arquivo baixado.
        * </param>
        */
        FileInfo receiveFile(PathDescriptor relativeDownloadSourceFilePath, PathDescriptor relativeDestinyFolderPath, string destinyFileName);


        /**
         * <summary>
         * Lista arquivos no diretorio especificado
         * </summary
         * <param name="path">
         * Caminho do diretorio a ser listado
         * </param>
         * <returns>
         * Lista de arquivos e diretorios.
         * </returns>
         */
        IDictionary<string, T> listFilesProperties(PathDescriptor path, bool resourcePropOnly);

        /**
         * <summary>
         * Envia um arquivo.
         * </summary>
         * <param name="file">
         * Arquivo a ser enviado.
         * </param>
         * <param name="relativeDestinyFolderPath">
         * Caminho relativo da pasta de destino. Exemplo: @"\fotos"
         * </param>         
         */
        T sendFile(FileInfo file, PathDescriptor relativeDestinyFolderPath);


        /**
        * <summary>
        * Deleta um arquivo no servidor.
        * </summary>
        * <param name="destiny">
        * Destino a ser removido
        * </param>
        */
        void delete(PathDescriptor destiny);


        /**
         * <summary>
         * Cria um novo diretório.
         * </summary>
         * <param name="folder">
         * Caminho relativo da pasta a ser criada.
         * </param>
         */
        T createDir(PathDescriptor folder);
    }
}
