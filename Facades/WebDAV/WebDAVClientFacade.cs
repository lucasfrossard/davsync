﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Net;

using WebDAVContent.Parsers;
using WebDAVContent.Manager;
using WebDAVContent.Configuration;
using WebDAVContent.Facades.Exceptions;
using WebDAVContent.Facades.WebDAVRequestMethods;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;
using WebDAVContent.GUID.Facades.Exceptions;
using WebDAVContent.GUID.Facades.WebDAV;
using WebDAVContent.GUID.Facades.WebDAVMessages;
using WebDAVContent.GUID.Sync;
using WebDAVContent.GUID.Facades.WebDAV.Operations;
using WebDAVContent.GUID.Log;

namespace WebDAVContent.Manager
{
    /**
     * <summary>
     * Classe com as operacoes WebDAV
     * </summary>
     * TODO verificar conexao com a internet - ping no servidor WebDAV
     * TODO verificar credencicais do WeBDAV
     * TODO aumentar o tamanho do buffer
     * TODO tratar: UriFormatException  (em todos os métodos)
     * TODO tratar: WebException http://msdn.microsoft.com/en-us/library/system.net.webexception.aspx 
     * - verificar o status: http://msdn.microsoft.com/en-us/library/system.net.webexception.status.aspx
     * - verificar a resposta (HttpWebResponse) quando houver e conferir o status (http://msdn.microsoft.com/en-us/library/system.net.httpwebresponse.statuscode.aspx)
     * > Lançado por: HttpWebRequest.GetRequestStream() e GetResponse() 
     */
    class WebDAVClientFacade : IFileManagerFacade<WebDAVFileProperties>
    {

        /** <summary> Tamanho do buffer </summary>*/
        // TODO aumentar o tamanho do buffer
        private static readonly int BUFFER_SIZE = 4096;

        /** <summary> Prefix </summary> */
        public static readonly string URI_PREFIX = "http://";

        public static readonly int MAX_TRIES = 5;


        /** <summary> Instancia única da Facade </summary> */
        private static WebDAVClientFacade instance;

        /** <summary> propriedades </summary>*/
        private static WebDAVProperties properties;

        /** 
         * <summary>
         * Construtor
         * </summary>
         */
        private WebDAVClientFacade(WebDAVProperties properties)
        {
            if (properties == null) {
                throw new ArgumentNullException("Properties must not be null");
            }
            WebDAVClientFacade.properties = properties;
        }

        /** 
         * <summary>
         * Construtor
         * </summary>
         */
        private WebDAVClientFacade()
            : this(WebDAVProperties.DEFAUL_PROPERTIES_FILE_PATH)
        {
            
        }


        /**
         * <summary>
         * Obtem instancia unica do cliente
         * </summary>
         */
        public static WebDAVClientFacade getInstance()
        {
            if (WebDAVClientFacade.instance == null) {
                WebDAVClientFacade.instance = new WebDAVClientFacade();
            }
            return WebDAVClientFacade.instance;
        }


        /**
        * <summary>
        * Obtem instancia unica do cliente
        * </summary>
        */
        public static WebDAVClientFacade getInstance(WebDAVProperties properties)
        {
            if (WebDAVClientFacade.instance == null)
            {
                WebDAVClientFacade.instance = new WebDAVClientFacade(properties);
            }
            return WebDAVClientFacade.instance;
        }

        /**
         * <summary>
         * Construtor privado. Carrega automaticamente um arquivo de configuracao.
         * </summary>
         */
        private WebDAVClientFacade(string propertiesFilePath) {
            if (!File.Exists(propertiesFilePath)) {
                throw new WebDAVClientException("Resource required not found! Resource {"+ propertiesFilePath +"}");
            }
            WebDAVClientFacade.properties = WebDAVProperties.getInstance();
            // carrega as propriedades a partir de um arquivo.
            WebDAVClientFacade.properties.load(propertiesFilePath);
        }


        /**
        * <summary>
        * Gera dados basicos de conexao
        * <summary>
        * <param name="uri">
        * URI de conexao
        * </param>
        * <param name="method">
        * Metodo a ser executado.
        * </param>
        * 
        */
        private HttpWebRequest generateBaseRequest(String uri, WebDAVRequestMethods method)
        {
            return this.generateBaseRequest(uri, method.getName());
        }


        /**
         * <summary>
         * Autenticação
         * </summary>
         * <param name="uri">
         * Local para a notificação de autenticação
         * </param>
         */
        public void authenticate(PathDescriptor authenticationPath)
        {
            try
            {
                WebDAVCredentials credentials = obtainCredential();
                int timeout = WebDAVClientFacade.properties.getTimeout();
                WebDAVAuthenticateOperation operation = new WebDAVAuthenticateOperation(authenticationPath, credentials, timeout);
                this.execute(operation);
            }
            catch (Exception e) {
                LogManager.LogMessage("Failed autheticating:");
                LogManager.LogError(e);
                throw new WebDAVAuthenticateException("Failed autheticating!", e);
            }
        }




        /**
        * <summary>
        * Faz o download síncrono
        * </summary>
        * <param name="relativeDownloadSourceFilePath">
        * Caminho relativo do arquivo a ser baixado.
        * </param>
        * <param name="relativeDestinyFolderPath">
        * Caminho relativo para a pasta em que o arquivo devera ser salvo.
        * </param>
        * <param name="destinyFileName">
        * Nome a ser dado ao arquivo baixado.
        * </param>
        */
        public FileInfo receiveFile(PathDescriptor relativeDownloadSourceFilePath, PathDescriptor relativeDestinyFolderPath, string destinyFileName)
        {
            try
            {
                WebDAVCredentials credentials = obtainCredential();
                int timeout = WebDAVClientFacade.properties.getTimeout();
                WebDAVReceiveFileOperation operation = new WebDAVReceiveFileOperation(relativeDownloadSourceFilePath, relativeDestinyFolderPath, destinyFileName, credentials, timeout);
                this.execute(operation);
                return operation.ReceivedFile;
            }
            catch (Exception e) {
                LogManager.LogMessage("Failed downloading resource:");
                LogManager.LogError(e);
                throw new WebDAVDownloadException("Failed downloading resource!", e);
            }

        }

        /**
         * <summary>
         * Faz upload síncrono (trava o recurso até que o upload seja concluído) para o servidor.
         * </summary>
         * <param name="file">
         * Arquivo a ser enviado.
         * </param>
         * <param name="relativeDestinyFolderPath">
         * Caminho relativo da pasta de destino. Exemplo: @"\fotos"
         * </param>         
         */
        public WebDAVFileProperties sendFile(FileInfo file, PathDescriptor relativeDestinyFolderPath)
        {
            try
            {
                WebDAVCredentials credentials = obtainCredential();
                int timeout = WebDAVClientFacade.properties.getTimeout();
                WebDAVSendFileOperation operation = new WebDAVSendFileOperation(file, relativeDestinyFolderPath, credentials, timeout);
                this.execute(operation);
                string relativePath = relativeDestinyFolderPath.RelativePath + file.Name;
                PathDescriptor filePath = new PathDescriptor(relativeDestinyFolderPath, relativePath);
                IDictionary<string, WebDAVFileProperties> fileProperties = this.listFilesProperties(filePath, true);
                return fileProperties[relativePath];
            } catch (Exception e){
                LogManager.LogMessage("Failed uploading resource:");
                LogManager.LogError(e);
                throw new WebDAVUploadException("Failed uploading resource!", e);    
            }
        }


        /**
         * <summary>
         * Faz a listagem de arquivos presentes no servidor
         * </summary>
         * <param name="path">
         * Caminho a ser listado
         * </param>
         */
        public IDictionary<string, WebDAVFileProperties> listFilesProperties(PathDescriptor path, bool readResourcePropOnly)
        {
            try
            {
                WebDAVCredentials credentials = obtainCredential();
                int timeout = WebDAVClientFacade.properties.getTimeout();
                WebDAVListFilesOperations operation = new WebDAVListFilesOperations(path, readResourcePropOnly, credentials, timeout);
                this.execute(operation);
                return operation.FileList;
            }
            catch (WebException e)
            {
                HttpWebResponse propfindResponse = e.Response as HttpWebResponse;
                LogManager.LogMessage("Failed listing files:");
                LogManager.LogError(e);
                throw new WebDAVListingException("Failed listing files!", e);

                /*if (!propfindResponse.StatusCode.Equals(HttpStatusCode.Unauthorized))
                    throw new WebDAVListingException("Failed listing files!", e);
                else
                    return null;*/
            }
        }


        /** <summary>
         *  Deletes the resource behind the specified Uri.
         * </summary>        
         * <param name="relativePath">
         * Caminho a ser removido.
         * </param>
         */
        public void delete(PathDescriptor relativeDeletePath)
        {
            try
            {
                WebDAVCredentials credentials = obtainCredential();
                int timeout = WebDAVClientFacade.properties.getTimeout();
                WebDAVDeleteOperation operation = new WebDAVDeleteOperation(relativeDeletePath, credentials, timeout);
                this.execute(operation);
            } catch (Exception e){
                LogManager.LogMessage("Failed deleting resource:");
                LogManager.LogError(e);
                throw new WebDAVDeleteException("Failed deleting resource!", e);
            }
        }



        /**
         * <summary>
         * Cria um novo diretório no servidor.
         * </summary
         */
        public WebDAVFileProperties createDir(PathDescriptor folderPath)
        {
            if (!this.exists(folderPath))
            {
                try
                {
                    WebDAVCredentials credentials = obtainCredential();
                    int timeout = WebDAVClientFacade.properties.getTimeout();
                    WebDAVCreateDirectoryOperation operation = new WebDAVCreateDirectoryOperation(folderPath, credentials, timeout);
                    this.execute(operation);
                }
                catch (Exception e)
                {
                    LogManager.LogMessage("Failed creating new directory:");
                    LogManager.LogError(e);
                    throw new WebDAVCreateDirException("Failed creating new directory!", e);
                }
            }
            IDictionary<string, WebDAVFileProperties> fileProperties = this.listFilesProperties(folderPath, true);
            return fileProperties[folderPath.RelativePath];
        }


        private static WebDAVCredentials obtainCredential()
        {
            string username = WebDAVClientFacade.properties.getProperty(WebDAVProperties.USERNAME);
            string password = WebDAVClientFacade.properties.getProperty(WebDAVProperties.PASSWORD);
            WebDAVCredentials credentials = new WebDAVCredentials(username, password);
            return credentials;
        }

        /**
         * <summary>
         * Checks if a resource exists on the specified Uri.
         * </summary>
         * <param name="uri">The Uri.</param>
         * <returns></returns>
         */
        public bool exists(PathDescriptor relativePath)
        {
            WebDAVCredentials credentials = obtainCredential();
            int timeout = WebDAVClientFacade.properties.getTimeout();
            WebDAVExistsOperation operation = new WebDAVExistsOperation(relativePath, credentials, timeout);
            this.execute(operation);
            return operation.Exists;


        }

        private void execute(IWebDAVOperation operation) {
            bool keepTrying = true;
            bool operationExecuted = false;
            for (int i = 1; (i <= MAX_TRIES) && keepTrying; i++) {

                try
                {
                    operation.execute();
                    operationExecuted = true;
                    keepTrying = false;
                }
                catch (WebException e)
                {
                    LogManager.LogError(e);
                    HttpWebResponse propfindResponse = e.Response as HttpWebResponse;
                    
                    // só continua tentando caso ocorra um timeout
                    if (!(e.Status.Equals(WebExceptionStatus.Timeout))) 
                    {
                        //Esse usuário não tem acesso a essa pasta
                        if (!propfindResponse.StatusCode.Equals(HttpStatusCode.Unauthorized))
                        {
                            keepTrying = false;
                            if (i == MAX_TRIES)
                                throw e;
                        }
                        else 
                        {
                            //TODO Retira essa pasta na lista de sicronização
                            if (i == MAX_TRIES)
                                throw e;
                        }
                    }
                    else
                    {
                        if (i == MAX_TRIES)
                            throw e;
                    }

                    throw e;
                    
                }
                finally
                {
                    operation.close();
                }
                System.Threading.Thread.Sleep(500);
            }
            if (!operationExecuted){
                throw new Exception ("Operation failed!");
            }
        }


        private static string correctSlash (string origin){
            origin = origin.Replace("\\", "/");
            return origin;
        }

        public static PathDescriptor generatePathDescriptor(string server, string rootFolder, string relativePath) {
            relativePath = relativePath.Replace("\\", "/");
            PathDescriptor generated = new PathDescriptor(WebDAVClientFacade.URI_PREFIX + server + "/", rootFolder, relativePath);
            return generated;
        
        }

        /**
        * <summary>
        * Gera dados basicos de conexao
        * <summary>
        * <param name="uri">
        * URI de conexao
        * </param>
        * <param name="method">
        * Metodo a ser executado.
        * </param>
        */
        // TODO remover daqui
        private HttpWebRequest generateBaseRequest(String uri, String method)
        {
            uri = correctSlash(uri);
            Uri realUri = new Uri(uri);
            HttpWebRequest request = (HttpWebRequest)System.Net.HttpWebRequest.Create(realUri);
            //Set the User Name and Password
            request.Credentials = new NetworkCredential(WebDAVClientFacade.properties.getProperty(WebDAVProperties.USERNAME), WebDAVClientFacade.properties.getProperty(WebDAVProperties.PASSWORD));
            request.Method = method;
            request.Timeout = WebDAVClientFacade.properties.getTimeout();
            return request;
        }
    }
}
