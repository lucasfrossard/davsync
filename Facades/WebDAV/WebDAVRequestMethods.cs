﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Facades.WebDAVRequestMethods
{

    public sealed class WebDAVRequestMethods
    {

        private readonly String name;
        private readonly int value;

        public static readonly WebDAVRequestMethods GET = new WebDAVRequestMethods(1, "GET");
        public static readonly WebDAVRequestMethods PUT = new WebDAVRequestMethods(2, "PUT");
        public static readonly WebDAVRequestMethods MOVE = new WebDAVRequestMethods(3, "MOVE");
        public static readonly WebDAVRequestMethods PROPFIND = new WebDAVRequestMethods(4, "PROPFIND");
        public static readonly WebDAVRequestMethods MKCOL = new WebDAVRequestMethods(5, "MKCOL");
        public static readonly WebDAVRequestMethods DELETE = new WebDAVRequestMethods(6, "DELETE");
        public static readonly WebDAVRequestMethods HEAD = new WebDAVRequestMethods(7, "HEAD");

        private WebDAVRequestMethods(int value, String name)
        {
            this.name = name;
            this.value = value;
        }

        public String getName()
        {
            return name;
        }

        override
        public String ToString()
        {
            return name;
        }

    }
}
