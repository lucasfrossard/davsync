﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Facades.WebDAV
{
    class WebDAVCredentials
    {
        /** <summary> Nome de usuario </summary> */
        private string webDAVUser;

        public string WebDAVUser
        {
            get { return webDAVUser; }
        }

        /** <summary> Senha </summary> */
        private string webDAVPassword;

        public string WebDAVPassword
        {
            get { return webDAVPassword; }
        }

        /**
         * <summary>
         * Construtor
         * </summary>
         */
        public WebDAVCredentials(String username, String password) {
            this.webDAVUser = username;
            this.webDAVPassword = password;
        }

    }
}
