﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Common;

namespace WebDAVContent.GUID.Facades.WebDAVMessages
{
    class HttpWebResponseMessages : Messages<string>
    {
        public static readonly string OK = "OK";
        public static readonly string UNAUTHORIZED = "UNAUTHORIZED";
        public static readonly string PROHIBITED = "PROHIBITED";
        public static readonly string RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";
        public static readonly string UNKOWN_RESPONSE = "UNKOWN_RESPONSE";

        // singleton
        private static HttpWebResponseMessages instance;

        private HttpWebResponseMessages()
        {
            this.addMessage(OK, "Operação concluída com sucesso!");
            this.addMessage(UNAUTHORIZED, "Acesso não autorizado!");
            this.addMessage(PROHIBITED, "Operação pribida!");
            this.addMessage(RESOURCE_NOT_FOUND, "Endereço inválido!");
            this.addMessage(UNKOWN_RESPONSE, "Resposta desconhecida!");
        }

        // singleton
        public static HttpWebResponseMessages getInstance() {
            if (HttpWebResponseMessages.instance == null)
            {
                HttpWebResponseMessages.instance = new HttpWebResponseMessages();
            }
            return HttpWebResponseMessages.instance;

        }
    }
}
