﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using WebDAVContent.GUID.Facades.WebDAVMessages;

namespace WebDAVContent.GUID.Facades.WebDAV
{
    class HttpWebResponseMessageFactory
    {
        private static HttpWebResponseMessageFactory instance;
        
        private HttpWebResponseMessageFactory() { 
        
        }

        public static HttpWebResponseMessageFactory getInstance() {
            if (HttpWebResponseMessageFactory.instance == null)
            {
                HttpWebResponseMessageFactory.instance = new HttpWebResponseMessageFactory();
            }
            return HttpWebResponseMessageFactory.instance;
        }

        public string getMessage(HttpWebResponse response)
        {
            HttpWebResponseMessages instance = HttpWebResponseMessages.getInstance();

            if (response.StatusCode.Equals(HttpStatusCode.OK) || response.StatusCode.Equals(HttpStatusCode.Created))
            {
                return instance.getMessage(HttpWebResponseMessages.OK);
            }
            else if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
            {
                // Autenticacao falhou
                return instance.getMessage(HttpWebResponseMessages.UNAUTHORIZED);

            }
            else if (response.StatusCode.Equals(HttpStatusCode.Forbidden))
            {
                // Acesso proibido
                return instance.getMessage(HttpWebResponseMessages.PROHIBITED);
            }
            else if (response.StatusCode.Equals(HttpStatusCode.NotFound))
            {
                // Endereco que se quer acessar é invalido
                return instance.getMessage(HttpWebResponseMessages.RESOURCE_NOT_FOUND);
            }
            else
            {
                // Resposta desconhecida
                return instance.getMessage(HttpWebResponseMessages.UNKOWN_RESPONSE);
            }
        }
    }
}
