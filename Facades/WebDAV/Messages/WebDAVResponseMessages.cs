﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Common;

namespace WebDAVContent.GUID.Facades.WebDAVMessages
{
    /***/
    class WebDAVResponseMessages : Messages<string>
    {
        public static readonly string AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";        

        // singleton
        private static WebDAVResponseMessages instance;

        private WebDAVResponseMessages()
        {
            this.addMessage(AUTHENTICATION_FAILED, "Autenticação falhou!");
        }

        // singleton
        public static WebDAVResponseMessages getInstance()
        {
            if (WebDAVResponseMessages.instance == null)
            {
                WebDAVResponseMessages.instance = new WebDAVResponseMessages();
            }
            return WebDAVResponseMessages.instance;

        }

        
    }
}
