﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.Manager
{
    public class WebDAVFileProperties : FileProperties
    {
        String multiStatus = String.Empty;
        String status = String.Empty;
        String displayName = String.Empty;
        String eTag = String.Empty;
        String filePathNameURI = String.Empty;

        Boolean isHidden = false;
        
        public String MultiStatus
        {
            get { return multiStatus; }
            set { multiStatus = value; }
        }

        public String Status
        {
            get { return status; }
            set { status = value; }
        }

        public String DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public Boolean IsHidden
        {
            get { return isHidden; }
            set { isHidden = value; }
        }

        public String ETag
        {
            get { return eTag; }
            set { eTag = value; }
        }

        public String FilePathNameURI
        {
            get { return correctSlash(filePathNameURI); }
            set { filePathNameURI = correctSlash(value); }
        }


        override
        public bool Equals(Object fileProperties)
        {
            bool a = base.Equals(fileProperties) ;
            if (fileProperties is WebDAVFileProperties)
            {
                bool equals = a;
                WebDAVFileProperties fp = (WebDAVFileProperties)fileProperties;
                equals = equals && fp.ETag.Equals(this.ETag);
                return equals;
            }
            return false;
        }
        
    }
}
