﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace WebDAVContent.GUID.Facades.WebDAV
{
    public class WebDavRequestCreator : IWebRequestCreate
    {
        public WebRequest Create(Uri uri)
        {
            return new WebDavRequest(uri);
        }
    }
}
