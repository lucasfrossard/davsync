﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace WebDAVContent.GUID.Facades.WebDAV
{
    class WebDavRequest : WebRequest
    {
        private Uri _uri;

        public WebDavRequest(Uri uri)
        {
            _uri = uri;
        }
        public override string ContentType { get; set; }
        public override Uri RequestUri { get { return _uri; } }
    }
}
