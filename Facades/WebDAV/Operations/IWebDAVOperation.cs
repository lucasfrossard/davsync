﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    /**
     * <summary>
     * Interface de uma operação WedDAB
     * </summary>
     */
    interface IWebDAVOperation
    {
        /**
         * <summary>
         * Executa a operação
         * </summary>
         */
        void execute();
        /**
         * 
         * <summary>
         * Fecha as streams
         * </summary>
         */
        void close();
    }
}
