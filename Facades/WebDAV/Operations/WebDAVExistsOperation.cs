﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Sync;
using System.Net;
using WebDAVContent.Facades.WebDAVRequestMethods;
using WebDAVContent.GUID.Log;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    class WebDAVExistsOperation : WebDAVOperation
    {
        /** <summary> Caminho a cuja existencia sera verificada </summary> */
        private PathDescriptor relativePathToCheck;
        /** <summary> Verifica se um caminho existe ou não </summary> */
        private bool exists = false;

        public bool Exists
        {
            get { return exists; }
        }

        public WebDAVExistsOperation(PathDescriptor relativePathToCheck, WebDAVCredentials credentials, int timeout) : base (credentials, timeout) {
            this.relativePathToCheck = relativePathToCheck;
        }

        protected override void executeOperation() {
            try
            {
                
                String uri = correctSlash(this.relativePathToCheck.getFullPath());
                HttpWebRequest webRequest = this.generateBaseRequest(uri, WebDAVRequestMethods.HEAD);
                webResponse = (HttpWebResponse)webRequest.GetResponse();
                this.exists =  webResponse.StatusCode == HttpStatusCode.OK;
            }
            catch (WebException e)
            {
                if (e.Response != null)
                {
                    if (e.Response is HttpWebResponse)
                    {
                        HttpWebResponse response = (HttpWebResponse)e.Response;
                        if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            this.exists = false;
                            return;
                        }
                
                    }
                }
                LogManager.LogError(e);
                throw (e);
            }
        }
    }
}
