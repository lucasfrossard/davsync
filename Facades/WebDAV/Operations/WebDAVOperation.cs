﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using WebDAVContent.Facades.WebDAVRequestMethods;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    abstract class WebDAVOperation : IWebDAVOperation
    {
        /** <summary> Credenciais </summary> */
        protected WebDAVCredentials credentials;
        /** <summary> Timeout da operação </summary> */
        protected int operationTimeout;
        /** <summary> Resposta da requisição</summary> */
        protected HttpWebResponse webResponse = null;


        /**
         * <summary>
         * Construtor
         * </summary>
         */
        public WebDAVOperation(WebDAVCredentials credentials, int timeout) {
            this.credentials = credentials;
            this.operationTimeout = timeout;
        }

        /**
         * <inheritdoc />
         */
        public void execute() {
            this.executeOperation();
            this.close();
        }

        /**
         * <inheritdoc />
         */
        protected abstract void executeOperation();

        /**
         * <inheritdoc />
         */
        public virtual void close() {
            if (webResponse != null)
            {
                webResponse.Close();
                webResponse = null;
            }
        }

        /**
         * <summary>
         * Gera dados basicos de conexao
         * <summary>
         * <param name="uri">
         * URI de conexao
         * </param>
         * <param name="method">
         * Metodo a ser executado.
         * </param>
         */
        // TODO remover daqui
        protected HttpWebRequest generateBaseRequest(String uri, String method)
        {
            uri = correctSlash(uri);
            Uri realUri = new Uri(uri);

            //realUri.AbsolutePath = Uri.UnescapeDataString(realUri.AbsolutePath);
            //Uri uri = new Uri(HttpUtility.UrlPathEncode(infospaceUrl));
            uri = Uri.EscapeUriString(uri);

            //Para trocar de protocolo:
            //WebRequest.RegisterPrefix("dav", new WebDavRequestCreator());
            //WebRequest req = WebRequest.Create("dav://customHost.contoso.com/");

            HttpWebRequest request = (HttpWebRequest)System.Net.HttpWebRequest.Create(realUri);
            request.Credentials = new NetworkCredential(this.credentials.WebDAVUser, this.credentials.WebDAVPassword);
            request.Method = method;
            request.Timeout = this.operationTimeout;

            return request;
        }


        protected HttpWebRequest generateBaseRequest(String uri, WebDAVRequestMethods method)
        {
            return this.generateBaseRequest(uri, method.getName());
        }
        

        protected static string correctSlash(string origin)
        {
            origin = origin.Replace("\\", "/");
            return origin;
        }
    }
}
