﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Sync;
using System.Net;
using WebDAVContent.Facades.WebDAVRequestMethods;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    class WebDAVDeleteOperation : WebDAVOperation
    {


        private PathDescriptor relativeDeletePath;

        public WebDAVDeleteOperation(PathDescriptor relativeDeletePath, WebDAVCredentials credentials, int timeout) : base (credentials, timeout)
        {
            this.relativeDeletePath = relativeDeletePath;
        }

        protected override void executeOperation() {
            string uri = correctSlash(relativeDeletePath.getFullPath());
            HttpWebRequest webRequest = this.generateBaseRequest(uri, WebDAVRequestMethods.DELETE);
            webResponse = (HttpWebResponse)webRequest.GetResponse();
        }
    }
}
