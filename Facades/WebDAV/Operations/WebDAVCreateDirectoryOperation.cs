﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Sync;
using System.Net;
using WebDAVContent.Facades.WebDAVRequestMethods;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    class WebDAVCreateDirectoryOperation : WebDAVOperation
    {

        /** <summary> Caminho da pasta a ser criada </summary> */
        private PathDescriptor folderPath;

        public WebDAVCreateDirectoryOperation(PathDescriptor folderPath, WebDAVCredentials credentials, int timeout) : base (credentials, timeout)
        {
            this.folderPath = folderPath;
        }

        protected override void executeOperation()
        {
            string uri = correctSlash(this.folderPath.getFullPath());
            char elementAt = uri.ElementAt(uri.Length - 1);
            if (!(uri.ElementAt(uri.Length - 1) == '/'))
            {
                throw new ArgumentException("Directory must ends with /!");
            }
            HttpWebRequest webRequest = this.generateBaseRequest(uri, WebDAVRequestMethods.MKCOL);
            this.webResponse = (HttpWebResponse)webRequest.GetResponse();
        }
    }
}
