﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WebDAVContent.GUID.Sync;
using System.Net;
using WebDAVContent.Facades.Exceptions;
using WebDAVContent.GUID.Log;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    class WebDAVSendFileOperation : WebDAVFileOperation
    {
        FileInfo file;
        PathDescriptor relativeDestinyFolderPath;

        public WebDAVSendFileOperation(FileInfo file, PathDescriptor relativeDestinyFolderPath, WebDAVCredentials credentials, int timeout) : base (credentials, timeout)
        {
            this.file = file;
            this.relativeDestinyFolderPath = relativeDestinyFolderPath;
        }

        protected override void executeOperation()
        {
            string uri = correctSlash(this.relativeDestinyFolderPath.getFullPath() + file.Name);
            HttpWebRequest webRequest = this.generateBaseRequest(uri, WebRequestMethods.Http.Put); ;

            //byte[] buffer = new byte[BUFFER_SIZE];
            byte[] buffer = Encoding.UTF8.GetBytes(uri);
            int bytesRead;
            int totalSent = 0;
            try
            {
                // Read the local file
                this.localStream = file.OpenRead();
                // Disable write buffer to avoid OutOfMemory situations with large files
                webRequest.AllowWriteStreamBuffering = true;
                // Have to set the ContentLength when AllowWriteStreamBuffering is disabled
                webRequest.ContentLength = localStream.Length;
                // Get the request stream
                this.remoteStream = webRequest.GetRequestStream();

                // Loop through stream until no bytes are there anymore
                do
                {
                    // bRun = _listUploads.Contains(keyValuePair);
                    // Console.WriteLine("Uplading! " + totalSent + " of " + localStream.Length);
                    // Read into the buffer from the remote stream
                    bytesRead = localStream.Read(buffer, 0, buffer.Length);
                    // Write into the file stream
                    this.remoteStream.Write(buffer, 0, bytesRead);

                    totalSent += bytesRead;
                } while (bytesRead > 0 && totalSent <= localStream.Length);

                this.close();


                // TODO: Extract possible status values from the response
                // TODO tratar resposta
                // Todo, antes este cara tinha um try/cath só para ele
                this.webResponse = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (Exception e)
            {
                // TODO tratar respostas
                LogManager.LogError(e);
                totalSent = -1;
                throw new WebDAVUploadException("Fail uploading file", e);
            }
        }
    }
}
