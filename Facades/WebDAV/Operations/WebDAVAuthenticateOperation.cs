﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;
using WebDAVContent.Facades.WebDAVRequestMethods;
using WebDAVContent.GUID.Sync;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    class WebDAVAuthenticateOperation : WebDAVOperation
    {
        private PathDescriptor authenticationPath;

        public WebDAVAuthenticateOperation(PathDescriptor path, WebDAVCredentials credentials, int timeout) :  base (credentials, timeout) {
            this.authenticationPath = path;
        }

        protected override void executeOperation() {
            // Cria a requisicao
            // TODO: verificar se este cara pode ser um mebro da classe
            // TODO: verificar conexão com a internet

            string uri = correctSlash(this.authenticationPath.getFullPath());
            HttpWebRequest request = this.generateBaseRequest(uri, WebDAVRequestMethods.HEAD);

            webResponse = (HttpWebResponse)request.GetResponse();

            if (!(webResponse.StatusCode.Equals(HttpStatusCode.OK) || webResponse.StatusCode.Equals(HttpStatusCode.Created)))
            {
                throw new WebDAVAuthenticateException("Fail authenticating!");
            }
            // autenticado com sucesso, só alegria!
        }

    }
}
