﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using WebDAVContent.GUID.Sync;
using System.IO;
using WebDAVContent.Facades.WebDAVRequestMethods;
using System.Net;
using WebDAVContent.Parsers;
using WebDAVContent.GUID.Log;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    class WebDAVListFilesOperations : WebDAVOperation
    {
        /** <summary> Caminho a ser listado</summary> */
        private PathDescriptor path;
        /** <summary> Indica que somente as propriedades do endereço devem ser retornadas <summary> */
        private bool resourcePropOnly;
        /** <summary> Retorno:  Lista de arquivos </summary> */
        private IDictionary<string, WebDAVFileProperties> fileList =  new Dictionary<string, WebDAVFileProperties>();

        public IDictionary<string, WebDAVFileProperties> FileList
        {
            get { return fileList; }
        }

        private Stream responseStream = null;

        public WebDAVListFilesOperations(PathDescriptor path, bool resourcePropOnly,  WebDAVCredentials credentials, int timeout) : base(credentials, timeout) {
            this.path = path;
            this.resourcePropOnly = resourcePropOnly;
        }

        protected override void executeOperation (){
            string uri = correctSlash(path.getFullPath());
            HttpWebRequest request = this.generateBaseRequest(uri, WebDAVRequestMethods.PROPFIND);

            //Monta a requisição do Propfind
            String strBody = "<?xml version='1.0'?>"
                    + "<d:propfind xmlns:d='DAV:'>"
                    + "<d:allprop/>"
                    + "</d:propfind>";

            //Codifica o corpo da raquisicao usando UTF-8.
            Byte[] bytes = Encoding.UTF8.GetBytes(strBody);
            request.ContentLength = bytes.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close(); // Tem que fechar aqui mesmo!


            //Adiciona propriedades no cabeçalho
            //request.ContentType = "text/xml;  charset=\"ISO-8859-1\""";
            request.ContentType = "text/xml;";
            //request.Headers.Add("charset", "ISO-8859-1");
            request.Headers.Add("Brief", "t");

            if (this.resourcePropOnly)
            {
                // Obtem apenas propriedades do caminho
                request.Headers.Add("Depth", "0");
            }
            else {
                // Obtem propriedade de tudo, no caso de diretorio
                request.Headers.Add("Depth", "1");
            }
            request.Headers.Add("Translate", "f");

            //Obtém a resposta da requisição
            this.webResponse = (HttpWebResponse)request.GetResponse();
            this.responseStream = webResponse.GetResponseStream();
           
            // tem que fechar tudo aqui para ele não travar as requisiões recursivas.
            IDictionary<string, WebDAVFileProperties> listedFiles = LSParser.getInstance().parse(responseStream, path);
            this.close();
            
            // verifica permissão de diretórios antes de inserir na lista
            foreach (KeyValuePair<string, WebDAVFileProperties> kvp in listedFiles){
                if (kvp.Value.IsDirectory)
                {
                    PathDescriptor authenticationPath = new PathDescriptor(path, kvp.Value.RelativePath);
                    WebDAVAuthenticateOperation authenticate = new WebDAVAuthenticateOperation(authenticationPath, this.credentials, this.operationTimeout);
                    // somente adiciona a lista se tiver permissão de abrir o diretório
                    if (this.checkPermission(authenticate))
                    {
                        this.fileList.Add(kvp);
                    }
            
                } else {
                    this.fileList.Add(kvp);
                }
            }    

            IDictionary<string, WebDAVFileProperties> auxilesCopy = new Dictionary<string, WebDAVFileProperties>();
            // faz copia para poder adicionar itens na coleção sem problemas
            foreach (KeyValuePair<string, WebDAVFileProperties> kvp in this.FileList) {
                auxilesCopy[kvp.Key] = kvp.Value;
            }

            if (!this.resourcePropOnly){
                foreach (KeyValuePair<string, WebDAVFileProperties> kvp in auxilesCopy) {
                // Itera somente quando é necessário obter propriedades de elementos filhos

                    if (kvp.Value.IsDirectory)
                    {
                        PathDescriptor dirPath = new PathDescriptor(path, kvp.Value.RelativePath);
                        if (!dirPath.Equals(path))
                        {
                            WebDAVListFilesOperations listNewDirectoriesAndFiles = new WebDAVListFilesOperations(dirPath, this.resourcePropOnly, this.credentials, this.operationTimeout);
                            listNewDirectoriesAndFiles.execute();
                            IDictionary<string, WebDAVFileProperties> newDirectoriesAndFiles = listNewDirectoriesAndFiles.FileList;
                            foreach (string key in newDirectoriesAndFiles.Keys)
                            {
                                if (!(this.fileList.Keys.Contains(key)))
                                {
                                    this.fileList.Add(key, newDirectoriesAndFiles[key]);
                                }
                            }
                        }
                    }
                }
            }
        }


        private bool checkPermission(WebDAVAuthenticateOperation authenticateOperation) {
            bool permited = false;
            try
            {
                authenticateOperation.execute();
                permited = true;
            }
            catch (WebException e) {
                HttpWebResponse propfindResponse = e.Response as HttpWebResponse;
                authenticateOperation.close();
                if (!(propfindResponse != null && (propfindResponse.StatusCode.Equals(HttpStatusCode.Unauthorized) || (propfindResponse.StatusCode.Equals(HttpStatusCode.Forbidden))))) {
                    LogManager.LogMessage(propfindResponse.StatusCode.ToString());
                    throw e;
                }
            }
            return permited;
        
        }

        public override void close()
        {
            base.close();
            if (this.responseStream != null)
            {
                this.responseStream.Close();
                this.responseStream.Dispose();
                this.responseStream = null;
            }

        }
    }
}
