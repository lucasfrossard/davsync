﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Sync;
using System.IO;
using System.Net;
using WebDAVContent.Facades.WebDAVRequestMethods;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    class WebDAVReceiveFileOperation : WebDAVFileOperation
    {
        /** <summary> Arquivo recebido </summary> */
        private FileInfo receivedFile;

        /** <summary> Retorno: arquivo recebido </summary> */
        public FileInfo ReceivedFile
        {
            get { return receivedFile; }
        }

        private PathDescriptor relativeDownloadSourceFilePath;

        private PathDescriptor relativeDesitinyFolderPath;

        private string destinyFileName;

        public WebDAVReceiveFileOperation (PathDescriptor relativeDownloadSourceFilePath, PathDescriptor relativeDestinyFolderPath, string destinyFileName, WebDAVCredentials credentials, int timeout) : base (credentials, timeout) {
            this.relativeDownloadSourceFilePath = relativeDownloadSourceFilePath;
            this.relativeDesitinyFolderPath = relativeDestinyFolderPath;
            this.destinyFileName = destinyFileName;
        }

        protected override void executeOperation() {
            // Cria a requisicao
            // TODO: verificar se este cara pode ser um mebro da classe
            // TODO: verificar conexão com a internet
            string uri = correctSlash(this.relativeDownloadSourceFilePath.getFullPath());
            HttpWebRequest webRequest = this.generateBaseRequest(uri, WebDAVRequestMethods.GET);
            this.webResponse = (HttpWebResponse)webRequest.GetResponse();

            //Create the buffer for storing the bytes read from the server
            byte[] bytes = new byte[BUFFER_SIZE];
            int bytesRead = 0;
            //Indicates how many bytes were read 
            long totalBytesRead = 0;
            //Indicates how many total bytes were read
            long contentLength = 0;
            //Indicates the length of the file being downloaded

            //Read the content length
            contentLength = Convert.ToInt64(this.webResponse.GetResponseHeader(RESPONSE_HEADER_CONTENT_LENGTH));

            //Create a new file to write the downloaded data to

            // TODO criar diretorio
            string dir = this.relativeDesitinyFolderPath.getFullPath();
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            
            string filepath = this.relativeDesitinyFolderPath.getFullPath() + destinyFileName;
            //string filepath = this.relativeDesitinyFolderPath.getFullPath() + Uri.UnescapeDataString(destinyFileName);

            // TODO Lock?
            this.localStream = new FileStream(filepath, FileMode.Create, FileAccess.Write);

            //Get the stream from the server
            this.remoteStream = this.webResponse.GetResponseStream();

            do
            {
                //Read from the stream
                bytesRead = this.remoteStream.Read(bytes, 0, bytes.Length);

                if (bytesRead > 0)
                {
                    totalBytesRead += bytesRead;

                    //Write to file
                    this.localStream.Write(bytes, 0, bytesRead);
                }

            } while (bytesRead > 0);

            //Close the response
            webResponse.Close();
            webResponse = null;
            this.receivedFile = new FileInfo(filepath);
        }


    }
}
