﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace WebDAVContent.GUID.Facades.WebDAV.Operations
{
    abstract class WebDAVFileOperation : WebDAVOperation
    {
        // TODO aumentar o tamanho do buffer
        /** <summary> Tamanho do buffer </summary>*/
        protected static readonly int BUFFER_SIZE = 4096;
        /** <summary> Tamanho do conteudo a ser recuperado </summary> */
        protected static readonly string RESPONSE_HEADER_CONTENT_LENGTH = "Content-Length";

        /** <summary> Stream remoto do arquivo da requisição </summary> */
        protected Stream remoteStream = null;
        /** <summary> Stream local do arquivo </summary> */
        protected FileStream localStream = null;
        


        public WebDAVFileOperation(WebDAVCredentials credentials, int timeout) : base(credentials, timeout) { }

        /**
         * <inheritdoc />
         */
        public override void close()
        {
            base.close();
            if (remoteStream != null)
            {
                remoteStream.Close();
                remoteStream.Dispose();
            }
            if (localStream != null)
            {
                localStream.Close();
                localStream.Dispose();
            }
        }
    }
}
