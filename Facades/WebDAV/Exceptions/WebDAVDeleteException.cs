﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;

namespace WebDAVContent.Facades.Exceptions
{
    class WebDAVDeleteException : WebDAVException
    {
        public WebDAVDeleteException(string errorMessage, Exception e)
            : base(errorMessage, e)
        { 
        }

        public WebDAVDeleteException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
