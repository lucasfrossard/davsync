﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Facades.WebDAV.Exceptions
{
    class WebDAVExistsException : WebDAVException
    {
        public WebDAVExistsException(String message, Exception e) : base(message, e) { 
        
        }

        public WebDAVExistsException(String message)
            : base(message)
        {

        }
    }
}
