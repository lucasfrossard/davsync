﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Facades.WebDAV.Exceptions
{
    /***/
    public class WebDAVException : Exception
    {
        public WebDAVException(String message, Exception e) : base(message, e) { 
        
        }

        public WebDAVException(String message)
            : base(message)
        {

        }
    }
}
