﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;

namespace WebDAVContent.Facades.Exceptions
{
    class WebDAVDownloadException : WebDAVException
    {
        public WebDAVDownloadException(string errorMessage, Exception e)
            : base(errorMessage, e)
        { 
        }
    }
}
