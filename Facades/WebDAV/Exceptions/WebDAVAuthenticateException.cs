﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Facades.WebDAV.Exceptions
{
    public class WebDAVAuthenticateException : WebDAVException
    {
        public WebDAVAuthenticateException(string errorMessage)
            : base(errorMessage)
        { 
        }

        public WebDAVAuthenticateException(string errorMessage, Exception e)
            : base(errorMessage, e)
        {
        }
    }
}
