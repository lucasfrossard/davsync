﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Facades.WebDAV.Exceptions
{
    public class WebDAVCreateDirException : WebDAVException
    {
        public WebDAVCreateDirException(string errorMessage)
            : base(errorMessage)
        { 
        }

        public WebDAVCreateDirException(string errorMessage, Exception e)
            : base(errorMessage, e)
        {
        }
    }
}
