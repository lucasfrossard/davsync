﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;

namespace WebDAVContent.Facades.Exceptions
{

    class WebDAVClientException : WebDAVException
    {

        public WebDAVClientException(string errorMessage)
            : base(errorMessage)
        { 
        }

        public WebDAVClientException(string errorMessage, Exception e)
            : base(errorMessage, e)
        {
        }
    }
}
