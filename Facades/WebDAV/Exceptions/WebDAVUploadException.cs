﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;

namespace WebDAVContent.Facades.Exceptions
{
    /**
     * <summary></summary>
     */
    class WebDAVUploadException : WebDAVException
    {
        public WebDAVUploadException(string errorMessage, Exception e)
            : base(errorMessage, e)
        { 
        }
    }
}
