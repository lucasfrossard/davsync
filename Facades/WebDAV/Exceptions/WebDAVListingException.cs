﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;

namespace WebDAVContent.Facades.Exceptions
{
    class WebDAVListingException : WebDAVException
    {
        public WebDAVListingException(string errorMessage, Exception e) : base(errorMessage, e) { 
        }

        public WebDAVListingException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
