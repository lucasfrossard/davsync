﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;

namespace WebDAVContent.GUID.Facades
{
    public interface IFileProperties
    {



        bool IsDirectory
        {
            get;
            set;
        }

        long Length
        {
            get;
            set;
        }

        DateTime LastModifiedTime
        {
            get;
            set;
        }

        DateTime CreationTime
        {
            get;
            set;
        }

        String getRelativePath();

        String RelativePath
        {
            get;
            set;
        }


        String Name
        {
            get;
        }

        String AbsolutePath
        {
            get;
            set;
        }

        String getRelativeDirectory(bool startTrailing);

        String RelativeDirectory
        {
            get;
        }

        /**
         * <summary>
         * Verifica se os arquivos apontam para o mesmo caminho.
         * </summary>
         * <param name="fileProperties">
         * Propriedades de um arquivo.
         * </param>
         * 
         */
        bool hasSamePath(FileProperties fileProperties);

    }
}
