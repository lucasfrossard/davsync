﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Manager
{

    /**
     * <summary>
     * Propriedades sobre a listagem de pastas/arquivos
     * </summary>
     */
    class ListFilesProperties 
    {
        String namespacePrefix;

        public String NamespacePrefix
        {
            get { return namespacePrefix; }
            set { namespacePrefix = value; }
        }

        // Data de criacao do arquivo
        DateTime creationTime;

        public DateTime CreationTime
        {
            get { return creationTime; }
            set { creationTime = value; }
        }

        // Data da ultima modificacao
        DateTime lastModifiedTime;

        public DateTime LastModifiedTime
        {
            get { return lastModifiedTime; }
            set { lastModifiedTime = value; }
        }

        // caminho absoluto
        String absolutePath = "";

        public String AbsolutePath
        {
            get { return absolutePath; }
            set { absolutePath = value; }
        }

        int status;

        public int Status
        {
            get { return status; }
            set { status = value; }
        }

        bool isHidden;

        public bool IsHidden
        {
            get { return isHidden; }
            set { isHidden = value; }
        }

        String displayName;

        public String DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }
    }
}
