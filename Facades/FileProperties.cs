﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Facades;
using WebDAVContent.GUID.Facades;
using WebDAVContent.Util;
using WebDAVContent.Configuration;

namespace WebDAVContent.Manager
{
    public class FileProperties : IFileProperties
    {
        /**<summary> caminho do arquivo - deve ser relativo a pasta base de arquivos do programa. </summary> */
        String relativePath = String.Empty;
        /**<summary> caminho absoluto </summary>*/
        String absolutePath = String.Empty;

        /**<summary> tamanho </summary> */
        long length = 0;
        /**<summary> Data de criacao do arquivo </summary> */
        DateTime creationTime;        
        /**<summary> Data da ultima modificacao </summary> */
        DateTime lastModifiedTime;
        /**<summary> Indica se é um diretório </summary> */
        bool isDirectory = false;

        public bool IsDirectory
        {
            get { return isDirectory; }
            set { isDirectory = value; }
        }

        public long Length
        {
            get { return length; }
            set { length = value; }
        }
        
        public DateTime LastModifiedTime
        {
            get { return lastModifiedTime; }
            set { lastModifiedTime = value; }
        }
        
        public DateTime CreationTime
        {
            get { return creationTime; }
            set { creationTime = value; }
        }


        public String getRelativePath() {
                return correctSlash(this.RelativePath);
        }

        /** <summary> Caminho relativo para o arquivo (/exemplo/arquivo) ou pasta (/exemplo/). </summary> */
        public String RelativePath
        {
            get { return correctSlash(this.relativePath); }
            set { 
                this.relativePath = correctSlash(value); 
            }
        }


        public String AbsolutePath
        {
            get { return correctSlash(absolutePath); }
            set { absolutePath = correctSlash(value); }
        }


        public String getRelativeDirectory(bool startTrailing) {
            if (startTrailing)
            {
                return correctSlash(this.RelativeDirectory);
            }
            else
            {
                return correctSlash(this.RelativeDirectory.Substring(1, this.RelativeDirectory.Length - 1));
            }
        }

        public String RelativeDirectory
        {
            get {
                String path = correctSlash(this.RelativePath);
                if (this.RelativePath.Length > 0 && this.RelativePath.ElementAt(path.Length - 1) == '/')
                {
                    return this.RelativePath;
                }
                else { 
                    return FilePropertiesUtil.extractDirectoryPath(this.RelativePath);
                }
            }
        }

        public String Name
        {
            get {
                return FilePropertiesUtil.extractName(this.RelativePath);
            }
        }

        /**
         * <summary>
         * Verifica se os arquivos apontam para o mesmo caminho.
         * </summary>
         * <param name="fileProperties">
         * Propriedades de um arquivo.
         * </param>
         * 
         */
        public bool hasSamePath(FileProperties fileProperties)
        {
            return fileProperties.relativePath.Equals(this.relativePath);
        }

        /**
         * <summary>
         * Um objeto deste tipo é considerado igual ao outro caso todas as propriedades, menos o caminho absoluto, 
         * sejam iguais. Ou seja, se a ultima data de modificacao, caminho relativo, hora de criação, tamanho e hashcode forem iguais.
         * </summary>
         */
        override
        public bool Equals(Object fileProperties) {
            if (fileProperties is FileProperties) {
                bool equals = true;
                FileProperties fp = (FileProperties) fileProperties;
                equals = equals && fp.RelativePath.Equals(this.RelativePath);
                equals = equals && fp.creationTime.Equals(this.creationTime);
                equals = equals && fp.lastModifiedTime.Equals(this.lastModifiedTime);
                equals = equals && fp.length.Equals(this.length);
                equals = equals && (fileProperties.GetHashCode() == this.GetHashCode());
                equals = equals && (fp.IsDirectory == this.IsDirectory);
                return equals;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.relativePath.GetHashCode();
        }

        protected static string correctSlash (string value){
            value = value.Replace("\\", "/");
            return value;
        }
         
    }
}
