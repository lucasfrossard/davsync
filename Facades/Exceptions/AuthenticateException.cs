﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Facades.Exceptions
{
    class AuthenticateException : Exception
    {
        public AuthenticateException(String message, Exception e) : base (message, e) { 
        
        }

        public AuthenticateException(String message)
            : base(message)
        {

        }
    }
}
