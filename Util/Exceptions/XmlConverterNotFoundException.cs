﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Util.Exceptions
{
    class XmlConverterNotFoundException : Exception
    {
        public XmlConverterNotFoundException(String errorMessage) : base(errorMessage) { }
    }
}
