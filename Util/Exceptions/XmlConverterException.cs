﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Util.Exceptions
{
    class XmlConverterException : Exception
    {
        public XmlConverterException(string errorMessage) : base(errorMessage)
        {
        }

        public XmlConverterException(string errorMessage, Exception e) : base(errorMessage, e)
        {
        }
    }
}
