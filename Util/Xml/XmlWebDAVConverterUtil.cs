﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Configuration;
using WebDAVContent.Manager;
using System.Xml;
using System.Xml.Linq;

namespace WebDAVContent.Util
{
    class XmlWebDAVConverterUtil : XmlConverterUtil<WebDAVFileProperties>
    {
        /** <summary> Instancia única do XmlWebDAVConverterUtil </summary> */
        private static XmlWebDAVConverterUtil instance = null;

        /**<summary> prefixo para o namespace do arquivo de propriedades da mirlo </summary> */
        private static readonly string WEBDAV_NS_FILE_PROPERTIES_PREFIX = "MFP";
        /**<summary> prefixo para o nome do namespace do arquivo de propriedades da mirlo </summary> */
        private static readonly string WEBDAV_FILE_PROPERTIES_NAMESPACE_MIRLO = "Mirlo:WebDAVFileProperties";


        /**<summary> tag que indentificar o elemento com as propriedades do arquivo </summary>*/
        private static readonly string WEBDAV_FILE_PROPERTIE_ELEMENT = "WebDavFileProperties";
        /**<summary> tag que indica o elemento que contem o caminho relativo do arquivo </summary>*/
        private static readonly string WEBDAV_FILE_PROPERTIE_ETAG = "etag";


        /** Construtor privado para o Singleton*/
        private XmlWebDAVConverterUtil() { }

        /** <summary> Obtem instancia unica do conversor de XML </summary>*/
        public static XmlConverterUtil<WebDAVFileProperties> getInstance()
        {
            if (XmlWebDAVConverterUtil.instance == null)
            {
                XmlWebDAVConverterUtil.instance = new XmlWebDAVConverterUtil();
            }
            return XmlWebDAVConverterUtil.instance;
        }


        /**
         * <summary>
         * Exporta o conteudo do objeto que contem as informacoes de um arquivo para o XmlWriter
         * </summary>
         * <param name="origin">
         * Arquivo com as propriedades do arquivo a ser exportado.
         * </param>
         * <param name="destiny">
         * Xml a ser escrito
         * </param>
         */
        override
        protected void exportToXML(WebDAVFileProperties origin, XmlWriter destiny)
        {
            base.exportToXML(origin, destiny);
            destiny.WriteStartElement(WEBDAV_NS_FILE_PROPERTIES_PREFIX, WEBDAV_FILE_PROPERTIE_ELEMENT, WEBDAV_FILE_PROPERTIES_NAMESPACE_MIRLO);
            // xmlns:bk="Mirlo:FileProperties"
            destiny.WriteAttributeString("xmlns", WEBDAV_NS_FILE_PROPERTIES_PREFIX, null, WEBDAV_FILE_PROPERTIES_NAMESPACE_MIRLO);

            // etag
            destiny.WriteElementString(WEBDAV_NS_FILE_PROPERTIES_PREFIX, WEBDAV_FILE_PROPERTIE_ETAG, null, origin.ETag);
            destiny.WriteEndElement();
        }





        /**
         * <summary>
         * Extrai dados do elemento para um FileProperties
         * </summary>
         * <param name="filePropertiesElement">
         * Elemento com as informacoes do arquivo.
         * </param>
         */
        override
        protected WebDAVFileProperties extractFileProperties(XElement filePropertiesElement, WebDAVFileProperties fileProperties)
        {
            base.extractFileProperties(filePropertiesElement, fileProperties);
            // variavel auziliar para fazer o parse
            XElement commonFileProperties = filePropertiesElement.Element(getNameWithMFPNamespace(WEBDAV_FILE_PROPERTIE_ELEMENT));
            XElement aux = null;

            // caminho relativo: <MFP:filerelativepath>a/b/c/d</MFP:filerelativepath>
            aux = commonFileProperties.Element(getNameWithMFPNamespace(WEBDAV_FILE_PROPERTIE_ETAG));
            fileProperties.ETag = aux.Value;
            return fileProperties;
        }

        private static XName getNameWithMFPNamespace(String elementName)
        {
            // Cria um namespace para o fazer o parse
            XNamespace aNamespace = WEBDAV_FILE_PROPERTIES_NAMESPACE_MIRLO;
            return aNamespace + elementName;

        }
    }
}
