﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using System.Xml;
using WebDAVContent.Util.Exceptions;
using System.IO;
using System.Xml.Linq;
using WebDAVContent.GUID.Log;

namespace WebDAVContent.Util
{
    public abstract class XmlConverterUtil<T> : IXmlConverterUtil<T> where T : FileProperties, new()
    {

        /** FilePropertiesList **/
        /**<summary> prefixo para o namespace do arquivo de lista de propriedades da mirlo </summary> */
        private static readonly string NS_FILE_PROPERTIES_LIST_PREFIX = "MFPL";
        /**<summary> prefixo para o nome do namespace do arquivo de lista de propriedades da mirlo </summary> */
        private static readonly string FILE_PROPERTIES_LIST_NAMESPACE_MIRLO = "Mirlo:FilePropertiesList";

        private static readonly string FILE_PROPERTIE_LIST_ELEMENT = "filePropertiesList";

        /** FileProperties */

        /**<summary> prefixo para o namespace do arquivo de propriedades da mirlo </summary> */
        private static readonly string NS_FILE_PROPERTIES_PREFIX = "MFP";
        /**<summary> prefixo para o nome do namespace do arquivo de propriedades da mirlo </summary> */
        private static readonly string FILE_PROPERTIES_NAMESPACE_MIRLO = "Mirlo:FileProperties";

        /**<summary> tag que indentificar o elemento com as propriedades do arquivo </summary>*/
        private static readonly string FILE_PROPERTIES_ELEMENT = "fileProperties";
        /**<summary> tag que indentificar o elemento com as propriedades do arquivo </summary>*/
        protected static readonly string COMMON_FILE_PROPERTIES_ELEMENT = "commonFileProperties";
        /**<summary> tag que indica o elemento que contem o caminho relativo do arquivo </summary>*/
        private static readonly string FILE_PROPERTY_RELATIVE_PATH_ELEMENT = "fileRelativePath";
        /**<summary> tag que indica o elemento que contem o caminho absoluto do arquivo </summary>*/
        private static readonly string FILE_PROPERTY_ABSOLUTE_PATH_ELEMENT = "fileAbsolutePath";
        /**<summary> tag que indica o elemento que contem o tamanho do arquivo */
        private static readonly string FILE_PROPERTY_FILE_LENGTH_ELEMENT = "fileLength";
        /**<summary> tag que indica o elemento que contem a data de criacao do arquivo </summary>*/
        private static readonly string FILE_PROPERTY_CREATION_TIME_ELEMENT = "creationTime";
        /**<summary> tag que indica o elemento que contem a data de  modificacao do elemento </summary>*/
        private static readonly string FILE_PROPERTY_LAST_MODIFIED_TIME_ELEMENT = "lastModifiedTime";
        /**<summary> tag que indica o tipo do elemento (diretorio ou arquivo) </summary>*/
        private static readonly string FILE_PROPERTY_RESOURCE_TYPE = "resourcetype";

        /**<summary> tempo definido em tick </summary>*/
        private static readonly string TIME_PROPERTY_TICK_ELEMENT = "tick";
        /**<summary> milisegundos </summary>*/
        private static readonly string TIME_PROPERTY_MILLISECOND_ELEMENT = "milliseconds";
        /**<summary> segundos </summary>*/
        private static readonly string TIME_PROPERTY_SECOND_ELEMENT = "seconds";
        /**<summary> minutos </summary>*/
        private static readonly string TIME_PROPERTY_MINUTE_ELEMENT = "minutes";
        /**<summary> horas </summary>*/
        private static readonly string TIME_PROPERTY_HOUR_ELEMENT = "hours";
        /**<summary> dia </summary>*/
        private static readonly string TIME_PROPERTY_DAY_ELEMENT = "day";
        /**<summary> mes </summary>*/
        private static readonly string TIME_PROPERTY_MONTH_ELEMENT = "month";
        /**<summary> ano </summary> */
        private static readonly string TIME_PROPERTY_YEAR_ELEMENT = "year";


        /**
         * Valores para o resource type
         */
        /** <summary> Arquivo </summary> */
        private static readonly string RESOURCE_FILE = "file";
        /** <summary> Diretorio </summary> */
        private static readonly string RESOURCE_DIRECTORY = "directory";

        /**
         * <summary>
         * Exporta lista de arquivos para um XML.
         * </summary>
         * <param name="origin">
         * Origem da exportacao: lista de objetos com detalhes de diversos arquivos.
         * </param>
         * <param name="destiny">
         * Destino da exportacao do XML: XML com as informacoes da lista de arquivos.
         * </param>
         */
        public void exportToXML(IEnumerable<T> origin, StringBuilder destiny)
        {
            // TODO capturar e lancar as excecoes
            // identificador
            XmlWriter xmlWriter = XmlWriter.Create(destiny);
            xmlWriter.WriteStartElement(NS_FILE_PROPERTIES_LIST_PREFIX, FILE_PROPERTIE_LIST_ELEMENT, FILE_PROPERTIES_LIST_NAMESPACE_MIRLO);
            // xmlns:bk="Mirlo:FileProperties"
            xmlWriter.WriteAttributeString("xmlns", NS_FILE_PROPERTIES_LIST_PREFIX, null, FILE_PROPERTIES_LIST_NAMESPACE_MIRLO);
            foreach (T fileProperties in origin)
            {
                this.exportFileProperties(xmlWriter, fileProperties);
            }
            xmlWriter.WriteEndElement();
            xmlWriter.Flush();
            xmlWriter.Close();
        }

        private void exportFileProperties(XmlWriter xmlWriter, T fileProperties)
        {
            xmlWriter.WriteStartElement(NS_FILE_PROPERTIES_PREFIX, FILE_PROPERTIES_ELEMENT, FILE_PROPERTIES_NAMESPACE_MIRLO);
            this.exportToXML(fileProperties, xmlWriter);
            xmlWriter.WriteEndElement();
        }

        /**
        * <summary>
        * Exporta o objeto para o formato XML.
        * </summary>
        * <param name="origin">
         * Origem da exportacao: Objeto com propriedades de um arquivo.
        * </param>
         * <param name="destiny">
         * Destino da exportacao: String com o XML com as inform~coes do arquivo.
         * </param>
        */
        public void exportToXML(T origin, StringBuilder destiny)
        {
            try
            {
                // TODO melhoria futuras: utilizar o XDocument aqui
                // identificador
                XmlWriter xmlWriter = XmlWriter.Create(destiny);
                this.exportFileProperties(xmlWriter, origin);
                xmlWriter.Flush();
                xmlWriter.Close();
            }
            catch (Exception ex)
            {
                LogManager.LogMessage("Failed to export XML:");
                LogManager.LogError(ex);

                throw new XmlConverterException("Failed to export XML", ex);
            }
        }

        /**
         * <summary>
         * Exporta o conteudo do objeto que contem as informacoes de um arquivo (FileProperties) para o XmlWriter
         * </summary>
         * <param name="origin">
         * Arquivo com as propriedades do arquivo a ser exportado.
         * </param>
         * <param name="destiny">
         * Xml a ser escrito
         * </param>
         */
        virtual
        protected void exportToXML(T origin, XmlWriter destiny)
        {
            destiny.WriteStartElement(NS_FILE_PROPERTIES_PREFIX, COMMON_FILE_PROPERTIES_ELEMENT, FILE_PROPERTIES_NAMESPACE_MIRLO);
            // xmlns:bk="Mirlo:FileProperties"
            destiny.WriteAttributeString("xmlns", NS_FILE_PROPERTIES_PREFIX, null, FILE_PROPERTIES_NAMESPACE_MIRLO);

            // caminho relativo do arquivo
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, FILE_PROPERTY_RELATIVE_PATH_ELEMENT, null, origin.RelativePath);
            // caminho absoluto do arquivo
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, FILE_PROPERTY_ABSOLUTE_PATH_ELEMENT, null, origin.AbsolutePath);
            // tamanho do arquivo
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, FILE_PROPERTY_FILE_LENGTH_ELEMENT, null, origin.Length.ToString());
            // tipo do recurso - diretorio ou arquivo
            string resourcetype = "";
            if (origin.IsDirectory){
                resourcetype = XmlConverterUtil<T>.RESOURCE_DIRECTORY;
            } else {
                resourcetype = XmlConverterUtil<T>.RESOURCE_FILE;
            }

            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, FILE_PROPERTY_RESOURCE_TYPE, null, resourcetype);
            
            {
                // hora de criacao
                destiny.WriteStartElement(NS_FILE_PROPERTIES_PREFIX, FILE_PROPERTY_CREATION_TIME_ELEMENT, null);
                this.exportToXML(origin.CreationTime, destiny);
                destiny.WriteEndElement(); // fecha tag de hora de criacao
                // hora da ultima modificacao
                destiny.WriteStartElement(NS_FILE_PROPERTIES_PREFIX, FILE_PROPERTY_LAST_MODIFIED_TIME_ELEMENT, null);
                this.exportToXML(origin.LastModifiedTime, destiny);
                destiny.WriteEndElement(); // fecha tag de hora de criacao
            }
            destiny.WriteEndElement();
        }

        /**
         * <summary>
         * Exporta data para xml.
         * </summary>
         * <param name="origin">
         * Data a ser esportada.
         * </param>
         * <param name="destiny">
         * Objeto xml em que a data deve ser escrita.
         * </param>
         */
        private void exportToXML(DateTime origin, XmlWriter destiny)
        {
            // tick
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_TICK_ELEMENT, null, origin.Ticks.ToString());
            // millisegundos
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_MILLISECOND_ELEMENT, null, origin.Millisecond.ToString());
            // segundos
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_SECOND_ELEMENT, null, origin.Second.ToString());
            // minutos
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_MINUTE_ELEMENT, null, origin.Minute.ToString());
            // horas
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_HOUR_ELEMENT, null, origin.Hour.ToString());
            // dia
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_DAY_ELEMENT, null, origin.Day.ToString());
            // mes
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_MONTH_ELEMENT, null, origin.Month.ToString());
            // ano
            destiny.WriteElementString(NS_FILE_PROPERTIES_PREFIX, TIME_PROPERTY_YEAR_ELEMENT, null, origin.Year.ToString());
        }


        /**
         * <summary>
         * Importa lista de propriedade de arquivos a partir de um xml.
         * </summary>
         * <param name="origin">
         * Origem dos dados a serem importados.
         * </param>
         * <param name="destiny">
         * Destino dos itens importados.
         * </param>
         */
        public void importFromXML(String origin, IDictionary<string, T> destiny)
        {
            // TODO capturar e lancar as excecoes
            StringReader stringReader = new StringReader(origin);
            this.importFromXML(stringReader, destiny);

        }


        /**
        * <summary>
        * Importa lista de propriedade de arquivos a partir de um xml.
        * </summary>
        * <param name="origin">
        * Origem dos dados a serem importados.
        * </param>
        * <param name="destiny">
        * Destino dos itens importados, em que a chave é o caminho relativo do arquivo e o valor são as propriedades
        * </param>
        */
        public void importFromXML(TextReader origin, IDictionary<string, T> destiny)
        {
            // Configuracao do XML
            // gera o XDocument a partir do XML
            XDocument doc = XDocument.Load(origin);
            // Obtem elementos do XML
            IEnumerable<XElement> elements = doc.Elements();
            // Este XML possui apenas um elemento que possui filhos
            XElement first = elements.First();

            IEnumerable<XElement> filePropertiesXML = first.Elements(XmlFilePropertiesConverterUtil.getNameWithMFPNamespace(FILE_PROPERTIES_ELEMENT));


            foreach (XElement xElement in filePropertiesXML)
            {
                // Chama o parser
                T fileProperties = new T();
                this.extractFileProperties(xElement, fileProperties);
                // inclui na lista
                destiny.Add(fileProperties.RelativePath, fileProperties);
            }
        }


        /**
         * <summary>
         * Importa dados do XML para o objeto que contem as informacoes do arquivo.
         * </summary>
         * <param name="origin">
         * String com o Xml a ser importado para o objeto.
         * </param>
         * <param name="destiny">
         * Objeto de destino da exportacao: Objeto que contera as propriedades do arquivo.
         * </param>
         */
        public void importFromXML(String origin, T destiny)
        {
            try
            {

                StringReader stringReader = new StringReader(origin);
                // Configuracao do XML
                // gera o XDocument a partir do XML
                XDocument doc = XDocument.Load(stringReader);
                // Obtem elementos do XML
                IEnumerable<XElement> elements = doc.Elements();
                // Este XML possui apenas um elemento que possui filhos
                XElement first = elements.First();


                // geracao do objeto
                this.extractFileProperties(first, destiny);
            }
            catch (Exception e)
            {
                LogManager.LogMessage("Failed to import XML");
                LogManager.LogError(e);
                throw new XmlConverterException("Failed to import XML", e);
            }
        }



        /**
         * <summary>
         * Extrai dados do elemento para um FileProperties
         * </summary>
         * <param name="filePropertiesElement">
         * Elemento com as informacoes do arquivo.
         * </param>
         */
        virtual
        protected T extractFileProperties(XElement filePropertiesElement, T fileProperties)
        {
            // variavel auziliar para fazer o parse
            XElement commonFileProperties = filePropertiesElement.Element(getNameWithMFPNamespace(COMMON_FILE_PROPERTIES_ELEMENT));
            XElement aux = null;

            // caminho relativo: <MFP:filerelativepath>a/b/c/d</MFP:filerelativepath>
            aux = commonFileProperties.Element(getNameWithMFPNamespace(FILE_PROPERTY_RELATIVE_PATH_ELEMENT));
            fileProperties.RelativePath = aux.Value;
            // caminho absoluto: <MFP:fileabsolutepath>c:/temp/a/b/c/d</MFP:fileabsolutepath>
            aux = commonFileProperties.Element(getNameWithMFPNamespace(FILE_PROPERTY_ABSOLUTE_PATH_ELEMENT));
            fileProperties.AbsolutePath = aux.Value;
            // tamanho do arquivo <MFP:filelength>10</MFP:filelength>
            aux = commonFileProperties.Element(getNameWithMFPNamespace(FILE_PROPERTY_FILE_LENGTH_ELEMENT));
            fileProperties.Length = Int64.Parse(aux.Value); fileProperties.Length = Int64.Parse(aux.Value);
            // Indica se é um diretório ou não
            aux = commonFileProperties.Element(getNameWithMFPNamespace(FILE_PROPERTY_RESOURCE_TYPE));
            if (aux != null && aux.Value.Equals(RESOURCE_DIRECTORY)) {
                fileProperties.IsDirectory = true;
            }

            // Data de criacao: <MFP:creationtime>
            XElement timeElement = commonFileProperties.Element(getNameWithMFPNamespace(FILE_PROPERTY_CREATION_TIME_ELEMENT));
            fileProperties.CreationTime = this.importDateTimeFromXML(timeElement);

            // Data da ultima alteracao: <MFP:lastmodifiedtime>
            timeElement = commonFileProperties.Element(getNameWithMFPNamespace(FILE_PROPERTY_LAST_MODIFIED_TIME_ELEMENT));
            fileProperties.LastModifiedTime = this.importDateTimeFromXML(timeElement);
            return fileProperties;
        }


        /**
         * <summary>
         * Importa data do elemento XML
         * </summary>
         * <param name="timeElement">
         * Elemento que contem a data a ser extraida.
         * </param>
         * <returns>
         * Um objeto com o data.
         * </return>
         */
        private DateTime importDateTimeFromXML(XElement timeElement)
        {
            DateTime aDateTime; //retorno
            XElement aux; // auxilia no parser
            // tick : <MFP:tick>634643415401758000</MFP:tick>
            aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_TICK_ELEMENT));
            // Tenta obter a data de criacao a partir da propriedade ticks
            if (aux.Value.Trim().Length > 0)
            {
                long time = Int64.Parse(aux.Value);
                aDateTime = new DateTime(time);
            }
            else
            {
                // caso contrario, obtem data a partir dos outros atributos
                // milisegundos
                aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_MILLISECOND_ELEMENT));
                String milliseconds = aux.Value;
                // segundos
                aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_SECOND_ELEMENT));
                String seconds = aux.Value;
                //minuots
                aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_MINUTE_ELEMENT));
                String minutes = aux.Value;
                // hora
                aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_HOUR_ELEMENT));
                String hour = aux.Value;
                // dia
                aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_DAY_ELEMENT));
                String day = aux.Value;
                // mes
                aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_MONTH_ELEMENT));
                String month = aux.Value;
                // ano
                aux = timeElement.Element(getNameWithMFPNamespace(TIME_PROPERTY_YEAR_ELEMENT));
                String year = aux.Value;
                // Converte para inteiro
                Int32 msec = Int32.Parse(milliseconds);
                Int32 sec = Int32.Parse(seconds);
                Int32 min = Int32.Parse(minutes);
                Int32 h = Int32.Parse(hour);
                Int32 d = Int32.Parse(day);
                Int32 mon = Int32.Parse(month);
                Int32 y = Int32.Parse(year);
                aDateTime = new DateTime(y, mon, d, h, min, sec, msec);
            }
            return aDateTime;
        }

        protected static XName getNameWithMFPNamespace(String elementName)
        {
            // Cria um namespace para o fazer o parse
            XNamespace aNamespace = FILE_PROPERTIES_NAMESPACE_MIRLO;
            return aNamespace + elementName;

        }
    }
}
