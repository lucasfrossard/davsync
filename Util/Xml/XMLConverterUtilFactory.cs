﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using WebDAVContent.Util.Exceptions;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.Util
{
    class XMLConverterUtilFactory<T> where T : IFileProperties
    {
        /**
         * <summary>
         * Fabrica para conversores de XML
         * </summary>
         */
        private XMLConverterUtilFactory() { 
        }

        public static IXmlConverterUtil<T> getXmlConverterUtil (Type t) {
            if (t.Equals(typeof(WebDAVFileProperties)))
            {
                return (IXmlConverterUtil<T>)XmlWebDAVConverterUtil.getInstance();
            }
            else if (t.Equals(typeof(FileProperties))) { 
                return (IXmlConverterUtil<T>)XmlFilePropertiesConverterUtil.getInstance();
            }
            throw new XmlConverterNotFoundException ("Could not find apropriate converter");
        } 
    }
}
