﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using System.IO;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.Util
{
    public interface IXmlConverterUtil<T> where T : IFileProperties
    {
        void exportToXML(IEnumerable<T> origin, StringBuilder destiny);
        void exportToXML(T origin, StringBuilder destiny);
        void importFromXML(String origin, IDictionary<string, T> destiny);
        void importFromXML(TextReader origin, IDictionary<string, T> destiny);
        void importFromXML(String origin, T destiny);

    }
}
