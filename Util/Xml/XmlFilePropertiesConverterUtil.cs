﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using WebDAVContent.Util.Exceptions;

namespace WebDAVContent.Util
{
    public class XmlFilePropertiesConverterUtil : XmlConverterUtil<FileProperties>
    {
        /** <summary> Instancia única do XmlFilePropertiesUtil </summary> */
        private static XmlFilePropertiesConverterUtil instance = null;

        /** Construtor privado para o Singleton*/
        private XmlFilePropertiesConverterUtil() { }

        /** <summary> Obtem instancia unica do conversor de XML </summary>*/
        public static XmlFilePropertiesConverterUtil getInstance(){
            if (XmlFilePropertiesConverterUtil.instance == null) {
                XmlFilePropertiesConverterUtil.instance = new XmlFilePropertiesConverterUtil();
            }
            return XmlFilePropertiesConverterUtil.instance;
        }
    }
}
