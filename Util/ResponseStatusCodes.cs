﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Util
{
    class ResponseStatusCodes
    {

        public ResponseStatusCodes() {   }
        
        //Requisicao bem sucedida e valor retornado com sucesso
        private static readonly String statusOK = "200";
        
        //A estrtutura foi criada inteiramente com sucesso
        private static readonly String statusCreated = "201";
                
        //A estrtutura foi criada inteiramente com sucesso
        private static readonly String statusMultiStatus = "207";

        //Servidor nao permitiu o acesso ao local requisitado
        private static readonly String statusUnauthorized = "401";

        //Servidor nao aceita a modificaçao no local requisitado
        private static readonly String statusForbidden = "403";

        //Servidor nao encontrou o arquivo
        private static readonly String statusNotFound = "404";

        //O servidor nao suporta o tipo de requisicao do corpo
        private static readonly String statusUnsupported = "415";

        //O Servidor nao tem espaço suficiente após a execução dessa requisição
        private static readonly String statusInsufficientStorage = "507";

        public static String StatusOK
        {
            get { return ResponseStatusCodes.statusOK; }
        }

        public static String StatusCreated
        {
            get { return ResponseStatusCodes.statusCreated; }
        }

        public static String StatusMultiStatus
        {
            get { return ResponseStatusCodes.statusMultiStatus; }
        }

        public static String StatusUnauthorized1
        {
            get { return ResponseStatusCodes.statusUnauthorized; }
        }

        public static String StatusForbidden
        {
            get { return ResponseStatusCodes.statusForbidden; }
        }

        public static String StatusNotFound
        {
            get { return ResponseStatusCodes.statusNotFound; }
        }

        public static String StatusUnsupported
        {
            get { return ResponseStatusCodes.statusUnsupported; }
        }

        public static String StatusInsufficientStorage
        {
            get { return ResponseStatusCodes.statusInsufficientStorage; }
        } 

    }
}
