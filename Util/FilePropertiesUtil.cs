﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WebDAVContent.Manager;
using WebDAVContent.GUID.Sync;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.Util
{
    class FilePropertiesUtil
    {

        /**
         * <summary> Classe utilitaria </summary>
         */
        private FilePropertiesUtil() { 
        }

        public static void export(DirectoryInfo dirInfo, PathDescriptor dir, IFileProperties dirProperties){
            string relativeDir = dir.RelativePath;
            FilePropertiesUtil.export(dirInfo, relativeDir, dirProperties);
        }


        public static void export(FileInfo fileInfo, PathDescriptor dir, IFileProperties fileProperties)
        {
            string filePath = fileInfo.FullName;
            string rootFolder = correctSlash(dir.RootFolder);
            string relativeFilePath = filePath.Substring(rootFolder.Length, filePath.Length - rootFolder.Length);
            FilePropertiesUtil.export(fileInfo, relativeFilePath, fileProperties);
        }

        /**
         * <summary> 
         * Exporta propriedades do arquivo 
         * </summary>
         * <param name="fileInfo">
         * Origem dos dados.
         * </param>
         * <param name="relativeFilepath">
         * Caminho relativo do arquivo.
         * </param>
         * <param name="fileProperties">
         * Destino dos dados.
         * </param>
         */
        public static void export(FileInfo fileInfo, string relativeFilepath, IFileProperties fileProperties)
        {
            fileProperties.RelativePath = relativeFilepath;
            fileProperties.CreationTime = fileInfo.CreationTime;
            fileProperties.LastModifiedTime = fileInfo.LastWriteTime;
            fileProperties.Length = fileInfo.Length;
            fileProperties.AbsolutePath = fileInfo.FullName;
        }

        /**
        * <summary> 
        * Exporta propriedades do arquivo 
        * </summary>
        * <param name="fileInfo">
        * Origem dos dados.
        * </param>
        * <param name="relativeFilepath">
        * Caminho relativo do arquivo.
        * </param>
        * <param name="fileProperties">
        * Destino dos dados.
        * </param>
        */
        public static void export(DirectoryInfo directoryInfo, string relativeFilepath, IFileProperties fileProperties)
        {
            fileProperties.RelativePath = relativeFilepath;
            fileProperties.CreationTime = directoryInfo.CreationTime;
            fileProperties.LastModifiedTime = directoryInfo.LastWriteTime;
            fileProperties.AbsolutePath = directoryInfo.FullName;
            fileProperties.IsDirectory = true;
        }

        /**
         * <summary>
         * Obtem diretorio do caminho específicado.
         * </summary>
         * <param name="path">
         * Caminho a se obter o diretorio.
         * </param>
         * <returns>
         * Caso o caminho seja um diretório, retorna ele próprio. Caso contrário retorna o diretório do arquivo.
         * </return>
         */
        public static string extractDirectoryPath(string path) {
            path = path.Replace("\\","/");
            string[] splitted = path.Split(new Char[] { '/' });
            bool ignoreLastElement = path.Length > 0 && !(path.ElementAt(path.Length - 1) == '/');
            string aPath = "/";
            for (int i = 0; i < splitted.Length; i++)
            {
                int l = splitted[i].Trim().Length;
                if (!(i == (splitted.Length - 1) && ignoreLastElement) && (splitted[i].Trim().Length > 0))
                {
                    aPath = aPath + splitted[i] + "/";
                }

            }
            return aPath;
        }


        /**
        * <summary>
        * Obtem nome do arquivo ou diretorio.
        * </summary>
        * <param name="path">
        * Caminho a se obter o nome do arquivo ou diretorio
        * </param>
        * <returns>
        * Nome do diretorio, sem /, ou nome do arquivo.
        * </return>
        */
        public static string extractName(string path)
        {
            path = path.Replace("\\", "/");
            string[] splitted = path.Split(new Char[] { '/' });
            
            for (int i = splitted.Length - 1; i >= 0 ; i--)
            {
                int length = splitted[i].Trim().Length;
                if (length > 0)
                {
                    return splitted[i];
                }

            }
            return "";
        }

        public static string correctSlash(string folder)
        {
            folder = folder.Replace("/", @"\");
            return folder;
        }

    }
}
