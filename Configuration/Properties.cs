﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WebDAVContent.Configuration.Exceptions;
using WebDAVContent.GUID.Log;

namespace WebDAVContent.Configuration
{
    /**
     * <summary>
     * Classe abtrata com informações do Webdav do usuário
     * </summary>
     * TODO melhorar o tratamento das exceções!
    */
    abstract class Properties : IProperties
    {   
        /** <summary> Propriedades armazenadas </summary> */
        private IDictionary<string, string> propertiesBag;

        public abstract string getConfDirectoriesPropertie();

        /** <summary> Construtor privado */
        protected Properties() {
            this.propertiesBag = new Dictionary<string, string>();
        }

        /**
        * <summary>
        * Obtem o valor de uma propriedade
        * </summay>
        * <param name="propertieKey">
        * Valor chave da propriedade
        * </param> 
        */
        public string getProperty(string propertyKey)
        {
            string value = null;
            this.propertiesBag.TryGetValue(propertyKey, out value);
            return value;
        }

        /**
        * <summary>
        * Seta o valor de uma propriedade
        * </summary>
        * <param name="propertyKey">
        * Chave da propriedade
        * </param>
        * <param name="value">
        * Valor da propriedade.
        * </param>
        */
        public void AddProperty(string propertyKey, string value)
        {
            this.propertiesBag.Add(propertyKey, value);
        }


        /**
         * <summary>
         * Seta propriedade
         * </summary>
         * <param name="propertyKey">
         * Chave da propriedade
         * </param>
         * <param name="value">
         * Valor da propriedade
         * </param>
         * 
         */
        public void setProperty(string propertyKey, string value) {
            if (this.propertiesBag.ContainsKey(propertyKey))
            {
                this.propertiesBag[propertyKey] = value;
            }
            else {
                this.AddProperty(propertyKey, value);
            }
        }


        /**
         * <summary>
         * Carrega propriedades de um arquivo
         * </summary>
         * <param name="confFile">
         * Arquivo de configuracao
         * </param>
         */
        public void load(string confFile)
        {
            // le arquivo, ignora linhas vazias, e linhas começadas com #
            // separa as linhas por =, trim nas strings resultantes
            // inclui chave valor
            StreamReader fileReader;

            if(File.Exists(confFile))
                fileReader = File.OpenText(confFile);
            else
            {
                string oi = Directory.GetCurrentDirectory();
                throw new PropertiesException("Arquivo de propriedades não existe");
            }

            if (fileReader != null)
            {
                while (!fileReader.EndOfStream)
                {
                    string line = fileReader.ReadLine();
                    line = line.Trim();
                    // linhas vazias e linhas comecadas com comentario, e linhas que começam com '[' 
                    if (line.Length > 0 && line.ElementAt(0) != '#' && line.ElementAt(0) != '[')
                    {
                        string[] splited = line.Split('=');
                        // linhas deve estar separadas por "chave = valor"
                        if (splited.Length == 2)
                        {
                            string key = splited[0].Trim();
                            string value = splited[1].Trim();

                            this.propertiesBag[key] = value;
                        }
                        else
                        {
                            throw new PropertiesException("Invalid entry on file!");
                        }
                    }
                }

                fileReader.Close();
            }
        }


        /**
         * <summary>
         * Guarda as propriedades em um arquivo
         * </summary>
         * <param name="confFile">
         * Arquivo de configuracao
         * </param>
         */
        public void save(string confFile)
        {
            // salva o arquivo
            String directoryServName = getConfDirectoriesPropertie().Split('\\')[0];

            // Cria o diretorio de configuracao
            if (!Directory.Exists(directoryServName))
            {
                Directory.CreateDirectory(directoryServName);
            }
            
            StreamWriter fileWriter = new StreamWriter(confFile);

            try
            {
                foreach (KeyValuePair<string, string> keyValurPair in this.propertiesBag)
                {
                    fileWriter.WriteLine(keyValurPair.Key + "=" + keyValurPair.Value);
                }

                fileWriter.Close();
            }
            catch 
            {
                LogManager.LogMessage("Invalid entry on file!");
                throw new PropertiesException("Invalid entry on file!");
            }
        }
    }
}
