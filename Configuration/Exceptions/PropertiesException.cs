﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Configuration.Exceptions
{
    class PropertiesException : Exception
    {
                /**
         * <summary>
         * Construtor
         * </summary>
         */
        public PropertiesException(string erroMessage, Exception e)
            : base(erroMessage, e)
        {
        }

        /**
         * <summary>
         * Construtor
         * </summary>
         */
        public PropertiesException(string erroMessage)
            : base(erroMessage)
        {
        }
    }
}
