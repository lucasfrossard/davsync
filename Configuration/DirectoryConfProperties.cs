﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using WebDAVContent.Configuration.Exceptions;

namespace WebDAVContent.Configuration
{
    class DirectoryConfProperties : Properties
    {
        /*CAUTION: Se mudar esses valores deve ser mudados no instalador também*/
        /** <summary> Caminho do arquivo de configuracao do diretorio </summary>*/
        public static readonly string DEFAUL_PROPERTIES_FILE_PATH = @"conf\directories.conf";
        /** Propriedades (devem ser iguais as entradas no arquivo) */
        /** <summary> Nome da propriedade do diretorio local pessoal </summary>*/
        public static readonly string LOCAL_PERSONAL_DIR = "local_personal_directory";
        /** <summary> Nome da propriedade do diretorio local corporativo </summary> */
        public static readonly string LOCAL_CORPORATE_DIR = "local_corporate_directory";
        /** <summary> Nome da propriedade do diretorio local pessoal </summary>*/
        public static readonly string SERVER_PERSONAL_DIR = "serve_personal_directory";
        /** <summary> Nome da propriedade do diretorio local corporativo </summary> */
        public static readonly string SERVER_CORPORATE_DIR = "server_corporate_directory";

        /** <summary> Instancia unica do objeto </summary> */
        private static DirectoryConfProperties instance;

        /**
         * <summary>
         * Construtor privado para o singleton 
         * </summary>
         */
        private DirectoryConfProperties() : base () {
            this.AddProperty(LOCAL_PERSONAL_DIR,"");
            this.AddProperty(LOCAL_CORPORATE_DIR, "");
            this.AddProperty(SERVER_CORPORATE_DIR, "");
            this.AddProperty(SERVER_PERSONAL_DIR, "");
        }

        /**
         * <summary>
         * Obtem instancia unica do objeto.
         * </summary>
         */
        public static DirectoryConfProperties getInstance()
        {
            if (DirectoryConfProperties.instance == null)
            {
                DirectoryConfProperties.instance = new DirectoryConfProperties();
            }
            return DirectoryConfProperties.instance;
        }

        public override string getConfDirectoriesPropertie()
        {
            return DirectoryConfProperties.DEFAUL_PROPERTIES_FILE_PATH;
        } 
    }
}
