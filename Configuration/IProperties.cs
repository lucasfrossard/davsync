﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Configuration
{
    public interface IProperties
    {
        /**
         * <summary>
         * Obtem o valor de uma propriedade
         * </summay>
         * <param name="propertieKey">
         * Valor chave da propriedade
         * </param>
         */
        string getProperty(string propertieKey);

        /**
         * <summary>
         * Seta o valor de uma propriedade
         * </summary>
         * <param name="propertieKey">
         * Chave da propriedade
         * </param>
         * <param name="value">
         * Valor da propriedade.
         * </param>
         */
        void AddProperty(string propertieKey, string value);

        /**
        * <summary>
        * Carrega propriedades de um arquivo
        * </summary>
        * <param name="confFile">
        * Arquivo de configuracao
        * </param>
        */
        void load(string confFile);

        /**
         * <summary>
         * Seta propriedade
         * </summary>
         * <param name="propertieKey">
         * Chave da propriedade
         * </param>
         * <param name="value">
         * Valor da propriedade
         * </param>
         * 
         */
        void setProperty(string propertieKey, string value);
    }
}
