﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Configuration
{
    class WebDAVProperties : Properties
    {
        /*CAUTION: Se mudar esses valores deve ser mudados no instalador também*/
        /** <summary> Caminho do arquivo de configuracao do diretorio </summary>*/
        public static readonly string DEFAUL_PROPERTIES_FILE_PATH = @"conf\server.conf";
        /** Propriedades (devem ser iguais as entradas no arquivo) */
        /** <summary> Nome da propriedade que contem o nome de usuario </summary>*/
        public static readonly string USERNAME = "username";
        /** <summary> Nome da propriedade que contem a senha </summary> */
        public static readonly string PASSWORD = "password";
        /** <summary> Nome da propriedade que contem o endereco do servidor </summary> */
        public static readonly string SERVER_ADDRESS = "server_address";
        /** <summary> Timeout </summary> */
        public static readonly string TIMEOUT = "timeout";
        /** <summary> Instancia unica da objeto </summary> */
        private static WebDAVProperties instance;
        //TODO tirar este cara daqui
        private static bool keepSyncroning = true;

        public bool KeepSyncroning
        {
            get { return WebDAVProperties.keepSyncroning; }
            set { WebDAVProperties.keepSyncroning = value; }
        }

        /**
         * <summary>
         * Construtor privado para o singleton
         * </summary>
         */
        private WebDAVProperties() : base () {
            this.AddProperty(USERNAME,"");
            this.AddProperty(PASSWORD, "");
            this.AddProperty(SERVER_ADDRESS,"");
            this.AddProperty(TIMEOUT, "20000");
        }

        /**
         * <summary>
         * Obtem instancia unica das propriedades
         * </summary>
         */
        public static WebDAVProperties getInstance() {
            if (WebDAVProperties.instance == null) {
                WebDAVProperties.instance = new WebDAVProperties();
            }
            return WebDAVProperties.instance;
        }

        public override string getConfDirectoriesPropertie()
        {
            return WebDAVProperties.DEFAUL_PROPERTIES_FILE_PATH;
        }

        public int getTimeout() {
            return Int32.Parse(this.getProperty(TIMEOUT));
        }
    }
}
