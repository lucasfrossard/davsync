﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebDAVContent.Manager;
using WEBDAVTests.TestFiles;
using WebDAVContent.Util;

namespace WEBDAVTests
{
    [TestClass]
    public class XmlWebDAVConverterUtilTest
    {
        private TestContext testContextInstance;

        /**
         * <summary>
         * Gets or sets the test context which provides
         * information about and functionality for the current test run.
         * </summary>
         */
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /** 
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void exportFilePropertiesToXMLTest()
        {
            WebDAVFileProperties fileProperties = generateFileProperties(10, @"a\b\c\d", false);
            StringBuilder actual = new StringBuilder();
            XmlWebDAVConverterUtil.getInstance().exportToXML(fileProperties, actual);
            String value = actual.ToString();
            Assert.IsTrue(value.Trim().Length > 0);
        }

        /**
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void importFilePropertiesFromXMLTest()
        {
            WebDAVFileProperties fileProperties = generateFileProperties(10, @"a\b\c\d", false);
            // String expected = null; // TODO: Initialize to an appropriate value
            StringBuilder exportDestiny = new StringBuilder();
            XmlWebDAVConverterUtil.getInstance().exportToXML(fileProperties, exportDestiny);
            WebDAVFileProperties filePropertiesDestiny = new WebDAVFileProperties();
            XmlWebDAVConverterUtil.getInstance().importFromXML(exportDestiny.ToString(), filePropertiesDestiny);
            Assert.AreEqual(fileProperties, filePropertiesDestiny);

            fileProperties = generateFileProperties(10, @"a\b\c\d", true);
            // String expected = null; // TODO: Initialize to an appropriate value
            exportDestiny = new StringBuilder();
            XmlWebDAVConverterUtil.getInstance().exportToXML(fileProperties, exportDestiny);
            filePropertiesDestiny = new WebDAVFileProperties();
            XmlWebDAVConverterUtil.getInstance().importFromXML(exportDestiny.ToString(), filePropertiesDestiny);
            Assert.AreEqual(fileProperties, filePropertiesDestiny);


            // Assert.Inconclusive("Verify the correctness of this test method.");
        }


        /**
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void exportFilePropertiesListToXMLTest()
        {
            ISet<WebDAVFileProperties> filePropertiesList = XmlWebDAVConverterUtilTest.generateFilePropertiesList();
            StringBuilder xml = new StringBuilder();
            XmlWebDAVConverterUtil.getInstance().exportToXML(filePropertiesList, xml);
            String actual = xml.ToString();
            Assert.IsTrue(xml.ToString().Trim().Length > 0);
        }


        /**
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void importFilePropertiesListFromXML()
        {
            ISet<WebDAVFileProperties> filePropertiesList = XmlWebDAVConverterUtilTest.generateFilePropertiesList();
            StringBuilder actual = new StringBuilder();
            XmlWebDAVConverterUtil.getInstance().exportToXML(filePropertiesList, actual);

            IDictionary<string, WebDAVFileProperties> importedList = new Dictionary<string, WebDAVFileProperties>();
            XmlWebDAVConverterUtil.getInstance().importFromXML(actual.ToString(), importedList);
            Assert.AreEqual(filePropertiesList.Count, importedList.Count);
            foreach (WebDAVFileProperties fileProperties in filePropertiesList)
            {
                Assert.IsTrue(importedList.ContainsKey(fileProperties.RelativePath));
            }
        }

        /**
         * <summary>
         * Gera lista de propriedades de arquivo.
         * </summary>
         */
        private static ISet<WebDAVFileProperties> generateFilePropertiesList()
        {
            ISet<WebDAVFileProperties> filePropertiesList = new HashSet<WebDAVFileProperties>();

            filePropertiesList.Add(generateFileProperties(10, @"a\b\c\d\", true));
            filePropertiesList.Add(generateFileProperties(20, @"adgh\hk\hj", false));
            filePropertiesList.Add(generateFileProperties(14, @"ajkh\jhjku\fedk", true));
            return filePropertiesList;
        }

        /**
         * <summary>
         * Gera propriedades.
         * </summary>
         */
        private static WebDAVFileProperties generateFileProperties(int length, string relativePath, bool directory)
        {
            WebDAVFileProperties fileProperties = new WebDAVFileProperties(); // TODO: Initialize to an appropriate value
            fileProperties.Length = 10;
            fileProperties.AbsolutePath = TestSetup.TEST_BASE_DIR + @"\" + relativePath;
            fileProperties.RelativePath = relativePath;
            fileProperties.CreationTime = DateTime.Now;
            fileProperties.LastModifiedTime = DateTime.Now;
            fileProperties.ETag = "kljfksdflsdjfklds";
            fileProperties.IsDirectory = directory;
            return fileProperties;
        }
    }
}
