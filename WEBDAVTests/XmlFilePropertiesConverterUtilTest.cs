﻿using WebDAVContent.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebDAVContent.Manager;
using System.Text;
using System.Collections.Generic;
using WEBDAVTests.TestFiles;

namespace WEBDAVTests
{
    
    
    /// <summary>
    ///This is a test class for XMLExporterUtilTest and is intended
    ///to contain all XMLExporterUtilTest Unit Tests
    ///</summary>
    [TestClass()]
    public class XmlFilePropertiesConverterUtilTest
    {


        private TestContext testContextInstance;

        /**
         * <summary>
         * Gets or sets the test context which provides
         * information about and functionality for the current test run.
         * </summary>
         */
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /** 
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void exportFilePropertiesToXMLTest()
        {
            FileProperties fileProperties = generateFileProperties(10, @"a\b\c\d");
            StringBuilder actual = new StringBuilder();
            XmlFilePropertiesConverterUtil.getInstance().exportToXML(fileProperties, actual);
            String value = actual.ToString();
            Assert.IsTrue(value.Trim().Length > 0);
        }

        /**
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void importFilePropertiesFromXMLTest()
        {
            FileProperties fileProperties = generateFileProperties(10, @"a\b\c\d");
            // String expected = null; // TODO: Initialize to an appropriate value
            StringBuilder actual = new StringBuilder();
            XmlFilePropertiesConverterUtil.getInstance().exportToXML(fileProperties, actual);
            FileProperties filePropertiesDestiny = new FileProperties();
            XmlFilePropertiesConverterUtil.getInstance().importFromXML(actual.ToString(), filePropertiesDestiny);
            Console.ReadLine();
            //Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }


        /**
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void exportFilePropertiesListToXMLTest()
        {
            ISet<FileProperties> filePropertiesList = XmlFilePropertiesConverterUtilTest.generateFilePropertiesList();
            StringBuilder xml = new StringBuilder();
            XmlFilePropertiesConverterUtil.getInstance().exportToXML(filePropertiesList, xml);
            Assert.IsTrue(xml.ToString().Trim().Length > 0);
        }


        /**
         * <summary>
         * A test for exportToXML
         * </summary>
         */
        [TestMethod()]
        public void importFilePropertiesListFromXML(){
            ISet<FileProperties> filePropertiesList = XmlFilePropertiesConverterUtilTest.generateFilePropertiesList();
            StringBuilder actual = new StringBuilder();
            XmlFilePropertiesConverterUtil.getInstance().exportToXML(filePropertiesList, actual);

            IDictionary< string, FileProperties> importedList = new Dictionary <string, FileProperties>();
            XmlFilePropertiesConverterUtil.getInstance().importFromXML(actual.ToString(), importedList);
            Assert.AreEqual(filePropertiesList.Count, importedList.Count);
            foreach (FileProperties fileProperties in filePropertiesList){
                Assert.IsTrue(importedList.ContainsKey(fileProperties.RelativePath));
            }
        }

        /**
         * <summary>
         * Gera lista de propriedades de arquivo.
         * </summary>
         */
        private static ISet<FileProperties> generateFilePropertiesList()
        {
            ISet<FileProperties> filePropertiesList = new HashSet<FileProperties>();
 
            filePropertiesList.Add(generateFileProperties(10, @"a\b\c\d"));
            filePropertiesList.Add(generateFileProperties(20, @"adgh\hk\hj"));
            filePropertiesList.Add(generateFileProperties(14, @"ajkh\jhjku\fedk"));
            return filePropertiesList;
        }

        /**
         * <summary>
         * Gera propriedades.
         * </summary>
         */
        private static FileProperties generateFileProperties(int length, string relativePath)
        {
            FileProperties fileProperties = new FileProperties(); // TODO: Initialize to an appropriate value
            fileProperties.Length = 10;
            fileProperties.AbsolutePath = TestSetup.TEST_BASE_DIR + @"\" + relativePath;
            fileProperties.RelativePath = relativePath;
            fileProperties.CreationTime = DateTime.Now;
            fileProperties.LastModifiedTime = DateTime.Now;
            return fileProperties;
        }
    }
}
