﻿using WebDAVContent.Sync;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WEBDAVTests.TestFiles;
using WebDAVContent.Manager;
using WebDAVContent.GUID.Sync;
using System.IO;
using WebDAVContent.Configuration;

namespace WEBDAVTests
{
    
    
    /**
     * <summary>
     * This is a test class for SyncManagerTest and is intended
     * to contain all SyncManagerTest Unit Tests
     * </summary>
     */
    [TestClass()]
    public class SyncManagerTest
    {
        /** <summary> Diretorio de sincronizacao. </summary> */
        // TODO verificar se existe localmente e no servidor, se não, criar
        private static readonly string SYNC_DIR = TestSetup.TEST_BASE_DIR + @"sync\folder\";
        /** <summary> Diretório dos snapshots locais. </summary> */
        private static readonly string LOCAL_SNAPSHOT_DIR = TestSetup.TEST_BASE_DIR + @"sync\snapshot\local";
        /** <summary> Diretório dos snapshots do servidor. </summary> */
        private static readonly string SERVER_SNAPSHOT_DIR = TestSetup.TEST_BASE_DIR + @"sync\snapshot\server";

        private static readonly WebDAVProperties davProperties = WebDAVProperties.getInstance();

        [ClassInitialize()]
        public static void setupTestEnvironment(TestContext context)
        {
            // setup basico
            TestSetup.setupTestEnviroment();

            Directory.CreateDirectory(SYNC_DIR);
            Directory.CreateDirectory(LOCAL_SNAPSHOT_DIR);
            Directory.CreateDirectory(SERVER_SNAPSHOT_DIR);

            SyncManagerTest.davProperties.setProperty(WebDAVProperties.USERNAME, "frossard");
            SyncManagerTest.davProperties.setProperty(WebDAVProperties.PASSWORD, "123456");
            SyncManagerTest.davProperties.setProperty(WebDAVProperties.SERVER_ADDRESS, "192.168.1.3");


            // TODO verificar se um diretório a ser sincronizado existe utilizando as funcoes WEBDAV

        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for sync
        ///</summary>
        [TestMethod()]
        public void syncTest()
        {
            DirectoryConfProperties dirConfProp = DirectoryConfProperties.getInstance();

            // sincroniza
            DirectoryConfProperties dirConfProperties = DirectoryConfProperties.getInstance();
            dirConfProperties.setProperty(DirectoryConfProperties.LOCAL_PERSONAL_DIR, SYNC_DIR);
            dirConfProperties.setProperty(DirectoryConfProperties.LOCAL_CORPORATE_DIR, SYNC_DIR);
            dirConfProperties.setProperty(DirectoryConfProperties.SERVER_PERSONAL_DIR, "");
            dirConfProperties.setProperty(DirectoryConfProperties.SERVER_CORPORATE_DIR, "");
            SyncManager syncManager = SyncManager.getInstance(dirConfProperties); // TODO: Initialize to an appropriate value

            // Origem da sincronizacao
            // TODO remover conteúdo da snapshot
            string personalLocalSnapShotFolder = LOCAL_SNAPSHOT_DIR;
            PathDescriptor localSyncDir = new PathDescriptor("",SYNC_DIR,"");
            LocalClientFacade localClientFacade = LocalClientFacade.getInstance();
            LocationController<FileProperties> localController = new LocationController<FileProperties>(localClientFacade, personalLocalSnapShotFolder, localSyncDir);
            
            // Dados do destino da sincronização
            string personalServerSnapShotFolder = SERVER_SNAPSHOT_DIR;
            string server = SyncManagerTest.davProperties.getProperty(WebDAVProperties.SERVER_ADDRESS);
            PathDescriptor serverSyncDir = WebDAVClientFacade.generatePathDescriptor(server, "", "");
            WebDAVClientFacade davClient = WebDAVClientFacade.getInstance(SyncManagerTest.davProperties);
            LocationController<WebDAVFileProperties> serverController = new LocationController<WebDAVFileProperties>(davClient, personalServerSnapShotFolder,serverSyncDir);
            

            string syncDir = SYNC_DIR;
            syncManager.sync<FileProperties, WebDAVFileProperties>(localController, serverController);
            // muda arquivos locais
            // muda arquivos no servidor
            // sincroniza
            // deve ter conflitos de atualizacao
        }
    }
}
