﻿using WebDAVContent.Sync;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebDAVContent.Manager;
using System.Collections.Generic;
using System.Text;
using System.IO;
using WEBDAVTests.TestFiles;
using WebDAVContent.Sync.Snapshot;
using WebDAVContent.Configuration;
using WebDAVContent.GUID.Sync;

namespace WEBDAVTests
{
    
    
    /// <summary>
    ///This is a test class for SnapshotManagerTest and is intended
    ///to contain all SnapshotManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SnapshotManagerTest
    {

        /** <summary> Caminho dos snapshots </summary> */
        private static readonly string SNAPSHOT_DIR = TestSetup.TEST_BASE_DIR + @"snapshot\";
        /** <summary> Caminho dos snapshots dos arquivos locais </summary> */
        private static readonly string LOCAL_SNAPSHOT_DIR = SNAPSHOT_DIR + @"local\";
        /** <summary> Caminho dos snapshots dos arquivos do servidor </summary> */
        private static readonly string SERVER_SNAPSHOT_DIR = SNAPSHOT_DIR + @"server\";

        /** <summary> Propriedades de conexao com o servidor </summary>*/
        private static WebDAVProperties webDAVProperties = WebDAVProperties.getInstance();

        private TestContext testContextInstance;

        /**
         * <summary>
         * Gets or sets the test context which provides
         * information about and functionality for the current test run.
         * </summary>
         */
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        /**
         * <summary> 
         * Prepara o ambiente de testes
         * </summary>
         */
        [ClassInitialize()]
        public static void setupTestEnvironment(TestContext context)
        {
            // setup basico
            TestSetup.setupTestEnviroment();
            // cria diretorios de snopshot
            SnapshotManagerTest.webDAVProperties.setProperty(WebDAVProperties.USERNAME, "frossard");
            SnapshotManagerTest.webDAVProperties.setProperty(WebDAVProperties.PASSWORD, "123456");
            SnapshotManagerTest.webDAVProperties.setProperty(WebDAVProperties.SERVER_ADDRESS, "192.168.1.3");
            Directory.CreateDirectory(LOCAL_SNAPSHOT_DIR);
            Directory.CreateDirectory(SERVER_SNAPSHOT_DIR);
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /** 
         * <summary>
         * A test for saveSnapshot
         * </summary>
         */
        [TestMethod()]
        public void saveLocalSnapshotTest()
        {
            LocalClientFacade fileManagerFacade = LocalClientFacade.getInstance(); 
            PathDescriptor dir = new PathDescriptor("", TestSetup.BASE_TEMP_DIR,""); 
            // carrega arquivos a serem salvos no snaptshot
            IDictionary<string, FileProperties> actual = fileManagerFacade.listFilesProperties(dir, false);
            // conta antes de salvar o snapshot
            string[] filesBefore = Directory.GetFiles(LOCAL_SNAPSHOT_DIR);
            // salva o snapshot
            SnapshotManager<FileProperties>.getInstance().saveSnapshot(actual.Values, LOCAL_SNAPSHOT_DIR);
            // conta depois
            string[] filesAfter = Directory.GetFiles(LOCAL_SNAPSHOT_DIR);
            // Tem que conter um arquivo a mais
            Assert.IsTrue(filesBefore.Length+1 == filesAfter.Length);
        }

        /**
         * <summary>
         * A test for loadOldestSnapshot. Este teste tem que ser executado após o teste acima.
         * </summary>
         */
        [TestMethod()]
        public void loadOldestLocalSnapshotTest()
        {
            IDictionary<string, FileProperties> actual = SnapshotManager<FileProperties>.getInstance().loadOldestSnapshot(LOCAL_SNAPSHOT_DIR);
            Assert.IsTrue(actual.Count > 0);
        }


        /**
         * <summary>
         * Testa o metodo de remocao de arquivos de snaphots antigos.
         * </summary>
         */
        [TestMethod()]
        public void deleteOldestLocalSnapshotsTest() {
            string tempSnapshotDir = SnapshotManagerTest.SNAPSHOT_DIR + @"temp";

            if (Directory.Exists(tempSnapshotDir))
            {
                Directory.Delete(tempSnapshotDir, true);
            }
            // cria um diretorio temporario
            Directory.CreateDirectory(tempSnapshotDir);
            
            LocalClientFacade fileManagerFacade = LocalClientFacade.getInstance();
            PathDescriptor baseDir = new PathDescriptor("", TestSetup.BASE_TEMP_DIR,"");
            IDictionary<string, FileProperties> actual = fileManagerFacade.listFilesProperties(baseDir, false);
            // arquivos de snapshots que devem ser mantidos
            ICollection<string> snapshotsKeepedFilenames = new HashSet<string>();
            // salva varios snapshots
            for (int i = 0; i < 5; i++)
            {
                string snapshotFilename = SnapshotManager<FileProperties>.getInstance().saveSnapshot(actual.Values, tempSnapshotDir);
                if (i > 2)
                {
                    snapshotsKeepedFilenames.Add(snapshotFilename.Replace("\\","/"));
                }
            }
            // remove
            SnapshotManager<FileProperties>.getInstance().deleteOldestSnapshots(2, tempSnapshotDir);
            // Lista arquivos do diretorio de snapshot
            PathDescriptor snapshotDirectory = new PathDescriptor("", tempSnapshotDir, "");
            IDictionary<string, FileProperties> afterDelete = fileManagerFacade.listFilesProperties(snapshotDirectory, false);
            Assert.IsTrue(afterDelete.Values.Count == 3);
            // verifica que os que sobraram são os mais recentes
            foreach(string fp in afterDelete.Keys){
                bool test = false;
                foreach (String keeped in snapshotsKeepedFilenames) {
                    if (keeped.Contains(fp)) {
                        test = true;
                    }
                }
                Assert.IsTrue(test);
            }

            
            Directory.Delete(tempSnapshotDir, true);
        }
   
            /** 
         * <summary>
         * A test for saveSnapshot
         * </summary>
         */
        [TestMethod()]
        public void saveServerSnapshotTest()
        {
            WebDAVClientFacade fileManagerFacade = WebDAVClientFacade.getInstance(SnapshotManagerTest.webDAVProperties);
            string server = SnapshotManagerTest.webDAVProperties.getProperty(WebDAVProperties.SERVER_ADDRESS);
            PathDescriptor dir = WebDAVClientFacade.generatePathDescriptor(server,"","");
            // carrega arquivos a serem salvos no snaptshot
            IDictionary<string, WebDAVFileProperties> actual = fileManagerFacade.listFilesProperties(dir, false);
            // conta antes de salvar o snapshot
            string[] filesBefore = Directory.GetFiles(SERVER_SNAPSHOT_DIR);
            // salva o snapshot
            SnapshotManager<WebDAVFileProperties>.getInstance().saveSnapshot(actual.Values, SERVER_SNAPSHOT_DIR);
            // conta depois
            string[] filesAfter = Directory.GetFiles(SERVER_SNAPSHOT_DIR);
            // Tem que conter um arquivo a mais
            Assert.IsTrue(filesBefore.Length+1 == filesAfter.Length);
        }

        /**
         * <summary>
         * A test for loadOldestSnapshot. Este teste tem que ser executado após o teste acima.
         * </summary>
         */
        [TestMethod()]
        public void loadOldestServerSnapshotTest()
        {
            IDictionary<string, WebDAVFileProperties> actual = SnapshotManager<WebDAVFileProperties>.getInstance().loadOldestSnapshot(SERVER_SNAPSHOT_DIR);
            Assert.IsTrue(actual.Count > 0);
        }
    }


}

