﻿using WebDAVContent.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WEBDAVTests.TestFiles;
using System.IO;
using WebDAVContent.GUID.Sync;

namespace WEBDAVTests
{
    
    
    /**
     * <summary>
     * This is a test class for LocalFacadeTest and is intended
     * to contain all LocalFacadeTest Unit Tests
     * </summary>
     */
    [TestClass()]
    public class LocalFacadeTest{


        /**
        * <summary> 
        * Prepara o ambiente de testes
        * </summary>
        */
        [ClassInitialize()]
        public static void setupTestEnvironment(TestContext context)
        {
            // setup basico
            TestSetup.setupTestEnviroment();
            // TODO criar um arquivo para garantir tamanho maior do que um
            FileInfo file = new FileInfo(TestSetup.BASE_TEMP_DIR + "oi");
            if (!file.Exists) {
                StreamWriter sw = file.CreateText();
                sw.WriteLine("oi");
                sw.Flush();
                sw.Close();
            }
        }


        /**
         * <summary>
         * A test for listFiles
         * </summary>
         */
        [TestMethod()]
        public void listFilesTest()
        {
            LocalClientFacade target = LocalClientFacade.getInstance();
            PathDescriptor directory = new PathDescriptor("", TestSetup.BASE_TEMP_DIR,"");
            IDictionary<string, FileProperties> actual = target.listFilesProperties(directory, false);
            Assert.AreEqual<Boolean>(true, actual.Count > 0);
        }
    }
}
