﻿using WebDAVContent.Sync;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebDAVContent.Manager;
using WEBDAVTests.TestFiles;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using WebDAVContent.Sync.Snapshot;
using WebDAVContent.Configuration;
using WebDAVContent.GUID.Sync;

namespace WEBDAVTests
{
    
    
    /// <summary>
    ///This is a test class for ChangeManagerTest and is intended
    ///to contain all ChangeManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ChangeManagerTest
    {

        /** <summary> Caminho para o diretorio de teste para o gerenciador de mudancas. </summary> */
        private static readonly string DISCOVER_TEST_PATH = TestSetup.TEST_BASE_DIR + @"discover\";
        /** <summary> Caminho para o diretorio de teste para diretorio a ser sincronizado. </summary> */
        private static readonly string DISCOVER_TEMPFILES_SYNC_DIR = DISCOVER_TEST_PATH + @"temp\";
        /** <summary> Caminho para o diretorio de teste para diretorio que deve conter os snapshots. </summary> */
        private static readonly string DISCOVER_LOCAL_SNAPSHOT_DIR = DISCOVER_TEST_PATH + @"localsnapshot\";
        /** <summary> Caminho para o diretorio de teste para diretorio que deve conter os snapshots. </summary> */
        private static readonly string DISCOVER_SERVER_SNAPSHOT_DIR = DISCOVER_TEST_PATH + @"serversnapshot\";

        /** <summary> Propriedades de conexao com o servidor </summary>*/
        private static WebDAVProperties webDAVProperties = WebDAVProperties.getInstance();

        private TestContext testContextInstance;

        /**
         * <summary>
         * Gets or sets the test context which provides
         * information about and functionality for the current test run.
         * </summary>
         */
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        /**
         * <summary> 
         * Prepara o ambiente de testes
         * </summary>
         */
        [ClassInitialize()]
        public static void setupTestEnvironment(TestContext context)
        {
            // setup basico
            TestSetup.setupTestEnviroment();

            ChangeManagerTest.webDAVProperties.setProperty(WebDAVProperties.USERNAME, "frossard");
            ChangeManagerTest.webDAVProperties.setProperty(WebDAVProperties.PASSWORD, "123456");
            ChangeManagerTest.webDAVProperties.setProperty(WebDAVProperties.SERVER_ADDRESS, "192.168.1.3");
            
            Directory.CreateDirectory(DISCOVER_TEST_PATH);
            Directory.CreateDirectory(DISCOVER_TEMPFILES_SYNC_DIR);
            Directory.CreateDirectory(DISCOVER_LOCAL_SNAPSHOT_DIR);
            Directory.CreateDirectory(DISCOVER_SERVER_SNAPSHOT_DIR);
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup()]
        public static void unsetTestEnvironment()
        {
            if (Directory.Exists(DISCOVER_TEST_PATH))
            {
                Directory.Delete(DISCOVER_TEST_PATH,true);
            }
        }
        
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion


        /**
         * <summary>
         * A test for discoverChanges
         * </summary>
         */
        [TestMethod()]
        public void discoverLocalChangesTest()
        {
            PathDescriptor syncdir = new PathDescriptor("", DISCOVER_TEMPFILES_SYNC_DIR,"");
            IFileManagerFacade<FileProperties> fileManagerFacade = LocalClientFacade.getInstance();
            ChangeManager<FileProperties> changeManager = new ChangeManager<FileProperties>(fileManagerFacade);
            // deleta os velhos snapshots
            SnapshotManager<FileProperties>.getInstance().deleteOldestSnapshots(0, DISCOVER_LOCAL_SNAPSHOT_DIR);
            IDictionary<string, FileProperties> snapshot = fileManagerFacade.listFilesProperties(syncdir, false);
            SnapshotManager<FileProperties>.getInstance().saveSnapshot(snapshot.Values, DISCOVER_LOCAL_SNAPSHOT_DIR);

            /** TESTA A DETECCAO DA CRIACAO DE UM NOVO ARQUIVO */
            // cria arquivo
            FileInfo fi = TestSetup.createFile("", ChangeManagerTest.DISCOVER_TEMPFILES_SYNC_DIR);
            // deve descobrir um novo arquivo, zero atualizacoes e zero remocoes
            ChangeManagerTest.testAssertChanges(1, 0, 0, fileManagerFacade, syncdir, DISCOVER_LOCAL_SNAPSHOT_DIR);

            /** TESTA DETECÇAO DE ALTERACAO DE ARQUIVO */
            // salva snapshot novo
            // cria snapshot e salva snapshot
            // deleta o snapshot velho
            SnapshotManager<FileProperties>.getInstance().deleteOldestSnapshots(0, DISCOVER_LOCAL_SNAPSHOT_DIR);
            snapshot = fileManagerFacade.listFilesProperties(syncdir, false);
            SnapshotManager<FileProperties>.getInstance().saveSnapshot(snapshot.Values, DISCOVER_LOCAL_SNAPSHOT_DIR);
            // altera o arquivo
            ChangeManagerTest.updateFile(fi);
            // deve descrobrir uma atualizacao, zero atualizacoes e zero removoes
            ChangeManagerTest.testAssertChanges(0, 1, 0, fileManagerFacade, syncdir, DISCOVER_LOCAL_SNAPSHOT_DIR);

            /*** TESTA A DETECCAO DE REMOCAO */
            // salva o snapshot
            // cria e salva snapshot e deleta os mais antigos
            snapshot = regenerateSnapshot<FileProperties>(fileManagerFacade, syncdir, DISCOVER_LOCAL_SNAPSHOT_DIR);
            // remove o arquivo
            fi.Delete();
            // deve descobrir uma remocao, zero atualizacoes e zero arquivos novos
            ChangeManagerTest.testAssertChanges(0, 0, 1, fileManagerFacade, syncdir, DISCOVER_LOCAL_SNAPSHOT_DIR);

            /** TESTA A CRIACAO, ATUALIZACAO E REMOCAO DE ARQUIVOS */
            // cria e salva snapshot e deleta os velhos
            snapshot = regenerateSnapshot<FileProperties>(fileManagerFacade, syncdir, DISCOVER_LOCAL_SNAPSHOT_DIR);

            // cria 2 novos arquivo
            FileInfo toBeUpdatedFile = TestSetup.createFile("_TOBEUP", syncdir.getFullPath());
            FileInfo toBeDeletedFile = TestSetup.createFile("", syncdir.getFullPath());
            // cira e salva snapshot e deleta os velhos
            snapshot = regenerateSnapshot<FileProperties>(fileManagerFacade, syncdir, DISCOVER_LOCAL_SNAPSHOT_DIR);
            // alterar arquivo anterior, remove um outro e cria um novo
            FileInfo newFile = TestSetup.createFile("", syncdir.getFullPath());
            ChangeManagerTest.updateFile(toBeUpdatedFile);
            toBeDeletedFile.Delete();
            // deve descobrir uma atualizacao, uma remocao e um novo arquivo.
            ChangeManagerTest.testAssertChanges(1, 1, 1, fileManagerFacade, syncdir, DISCOVER_LOCAL_SNAPSHOT_DIR);
        }

        private static IDictionary<string, T> regenerateSnapshot<T>(IFileManagerFacade<T> fileManagerFacade, PathDescriptor relativeSyncDir, string relativeSnapshotDir) where T : FileProperties
        {
            IDictionary<string, T> snapshot = snapshot = fileManagerFacade.listFilesProperties(relativeSyncDir, false);
            SnapshotManager<T>.getInstance().saveSnapshot(snapshot.Values, relativeSnapshotDir);
            SnapshotManager<T>.getInstance().deleteOldestSnapshots(1, relativeSnapshotDir);
            return snapshot;
        }

        private static void updateFile(FileInfo fi)
        {
            FileStream fs = fi.OpenWrite();
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine("Just fixing it!");
                sw.Flush();
                sw.Close();
            }
        }

        private static void testAssertChanges<T>(int brandNew, int updated, int removed, IFileManagerFacade<T> fileManagerFacade, PathDescriptor relativeSyncDirPath,string snapshotDir) where T : FileProperties
        {
            ChangeManager<T> cm = new ChangeManager<T>(fileManagerFacade);
            cm.discoverChanges(relativeSyncDirPath, snapshotDir);
            Assert.IsTrue(cm.NewItems.Count == brandNew);
            Assert.IsTrue(cm.UpdatedItems.Count == updated);
            Assert.IsTrue(cm.removedItems.Count == removed);
        }

        private static void testAssertServerChanges<T>(int brandNew, int updated, int removed, IFileManagerFacade<T> fileManagerFacade, PathDescriptor relativeSyncDirPath, string snapshotDir) where T : FileProperties
        {
            ChangeManager<T> cm = new ChangeManager<T>(fileManagerFacade);
            cm.discoverChanges(relativeSyncDirPath, snapshotDir);
            Assert.IsTrue(cm.NewItems.Count >= brandNew);
            Assert.IsTrue(cm.UpdatedItems.Count == updated);
            Assert.IsTrue(cm.removedItems.Count == removed);
        }

        /**
         * <summary>
         * A test for discoverChanges
         * </summary>
         */
        [TestMethod()]
        public void discoverServerChangesTest()
        {
            IFileManagerFacade<WebDAVFileProperties> fileManagerFacade = WebDAVClientFacade.getInstance(ChangeManagerTest.webDAVProperties);
            ChangeManager<WebDAVFileProperties> changeManager = new ChangeManager<WebDAVFileProperties>(fileManagerFacade);
            // deleta os velhos snapshots
            SnapshotManager<WebDAVFileProperties>.getInstance().deleteOldestSnapshots(0, ChangeManagerTest.DISCOVER_SERVER_SNAPSHOT_DIR);
            string aBase = WebDAVClientFacade.URI_PREFIX+ChangeManagerTest.webDAVProperties.getProperty(WebDAVProperties.SERVER_ADDRESS) + "/";
            PathDescriptor syncDir = new PathDescriptor(aBase, "","");
            /** TESTA A DETECCAO DA CRIACAO DE UM NOVO ARQUIVO */
            // cria arquivo
            FileInfo fi = TestSetup.createFile("", ChangeManagerTest.DISCOVER_TEMPFILES_SYNC_DIR);
            WebDAVFileProperties fp = fileManagerFacade.sendFile(fi, syncDir);
            // deve descobrir um novo arquivo, zero atualizacoes e zero remocoes
            ChangeManagerTest.testAssertServerChanges(1, 0, 0, fileManagerFacade, syncDir, DISCOVER_SERVER_SNAPSHOT_DIR);

            /** TESTA DETECÇAO DE ALTERACAO DE ARQUIVO */
            // salva snapshot novo
            // cria snapshot e salva snapshot
            IDictionary<string, WebDAVFileProperties> snapshot = fileManagerFacade.listFilesProperties(syncDir, false);
            SnapshotManager<WebDAVFileProperties>.getInstance().saveSnapshot(snapshot.Values, DISCOVER_SERVER_SNAPSHOT_DIR);
            // altera o arquivo
            PathDescriptor toBeDeletedPath = new PathDescriptor(syncDir.ABase,"", fi.Name);
            fileManagerFacade.delete(toBeDeletedPath);
            fileManagerFacade.sendFile(fi, syncDir);
            // deve descrobrir uma atualizacao, duas atualizacoes e zero removoes
            ChangeManagerTest.testAssertChanges(0, 1, 0, fileManagerFacade, syncDir, DISCOVER_SERVER_SNAPSHOT_DIR);

            /*** TESTA A DETECCAO DE REMOCAO */
            // salva o snapshot
            // cria e salva snapshot e deleta os mais antigos
            snapshot = regenerateSnapshot<WebDAVFileProperties>(fileManagerFacade, syncDir, DISCOVER_SERVER_SNAPSHOT_DIR);
            // remove o arquivo
            toBeDeletedPath = new PathDescriptor(syncDir.ABase,"", fi.Name);
            fileManagerFacade.delete(toBeDeletedPath);
            fi.Delete();
            // deve descobrir uma remocao, uma atualizacoes e zero arquivos novos
            ChangeManagerTest.testAssertChanges(0, 0, 1, fileManagerFacade, syncDir, DISCOVER_SERVER_SNAPSHOT_DIR);

            /** TESTA A CRIACAO, ATUALIZACAO E REMOCAO DE ARQUIVOS */
            // cria e salva snapshot e deleta os velhos
            snapshot = regenerateSnapshot<WebDAVFileProperties>(fileManagerFacade, syncDir, DISCOVER_SERVER_SNAPSHOT_DIR);

            // cria 2 novos arquivo
            FileInfo toBeUpdatedFile = TestSetup.createFile("_TOBEUP", DISCOVER_TEMPFILES_SYNC_DIR);
            fileManagerFacade.sendFile(toBeUpdatedFile, syncDir);
            FileInfo toBeDeletedFile = TestSetup.createFile("_TOBEDEL", DISCOVER_TEMPFILES_SYNC_DIR);
            fileManagerFacade.sendFile(toBeDeletedFile, syncDir);
            // cira e salva snapshot e deleta os velhos
            snapshot = regenerateSnapshot<WebDAVFileProperties>(fileManagerFacade, syncDir, DISCOVER_SERVER_SNAPSHOT_DIR);
            // alterar arquivo anterior, remove um outro e cria um novo
            FileInfo newFile = TestSetup.createFile("THE_NEW", DISCOVER_TEMPFILES_SYNC_DIR);
            // novo arquivo
            fileManagerFacade.sendFile(newFile, syncDir);
            ChangeManagerTest.updateFile(toBeUpdatedFile);
            // atualizacao
            toBeDeletedPath = new PathDescriptor(syncDir.ABase, "", toBeUpdatedFile.Name );
            fileManagerFacade.delete(toBeDeletedPath);
            fileManagerFacade.sendFile(toBeUpdatedFile, syncDir);
            // remocao
            toBeDeletedPath = new PathDescriptor(syncDir.ABase, "", toBeDeletedFile.Name);
            fileManagerFacade.delete(toBeDeletedPath);
            toBeDeletedFile.Delete();
            toBeUpdatedFile.Delete();
            newFile.Delete();
            // deve descobrir duas atualizacao, uma remocao e um novo arquivo.
            ChangeManagerTest.testAssertChanges(1, 1, 1, fileManagerFacade, syncDir, DISCOVER_SERVER_SNAPSHOT_DIR);
        }
    }
}
