﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WEBDAVTests.TestFiles
{
    class TestSetup
    {
        /**
         * <summary>
         * Base do diretorio temporario
         * </summary>
         */
        public static readonly string BASE_TEMP_DIR = @"c:\temp\";
        /**
         * <summary>
         * Diretorio base para os testes.
         * </summary>
         */
        public static readonly string TEST_BASE_DIR = TestSetup.BASE_TEMP_DIR + @"test\";

        /**
         * <summary>
         * Classe utilitaria
         * </summary>
         */
        private TestSetup() { 
        
        }

        /**
         * Configura o ambiente de teste
         */
        public static void setupTestEnviroment () {
            Directory.CreateDirectory(TestSetup.TEST_BASE_DIR);
        }

        /**
         * <summary>
         * Cria um arquivo para testes
         * </summary>
         * <param name="sufix">
         * Sufixo.
         * </param>
         * <param name="dir">
         * Diretório do arquivo.
         * </param>
         */
        public static FileInfo createFile(string sufix, string dir)
        {
            string filename = DateTime.Now.Ticks.ToString();
            filename = filename + "_" + sufix + ".txt";
            string filepath = dir + @"\" + filename;
            FileInfo file = new FileInfo(filepath);
            using (StreamWriter sw = file.CreateText())
            {
                sw.WriteLine(filename + " content");
                sw.Flush();
                sw.Close();
            }
            return file;
        }

    }
}
