﻿using WebDAVContent.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace WEBDAVTests
{
    
    
    /// <summary>
    ///This is a test class for FilePropertiesUtilTest and is intended
    ///to contain all FilePropertiesUtilTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FilePropertiesUtilTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for extractDirectoryPath
        ///</summary>
        [TestMethod()]
        public void extractDirectoryPathTest()
        {
            string path = "/oi/atum";
            string expected = "/oi/";
            string actual;
            actual = FilePropertiesUtil.extractDirectoryPath(path);
            Assert.AreEqual(expected, actual);
            path = "/oi/";
            expected = "/oi/";
            actual = FilePropertiesUtil.extractDirectoryPath(path);
            Assert.AreEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for extractName
        ///</summary>
        [TestMethod()]
        public void extractNameTest()
        {
            string path = "/blabla/bleble/bliblu/lalala/";
            string expected = "lalala";
            string actual;
            actual = FilePropertiesUtil.extractName(path);
            Assert.AreEqual(expected, actual);

            path = "/blabla/bleble/bliblu/lalala/issa";
            expected = "issa";
            actual = FilePropertiesUtil.extractName(path);
            Assert.AreEqual(expected, actual);

        }
    }
}
