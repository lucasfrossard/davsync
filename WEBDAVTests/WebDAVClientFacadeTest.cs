﻿using WebDAVContent.Manager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using WEBDAVTests.TestFiles;
using WebDAVContent.Configuration;
using WebDAVContent.Util;
using System.Collections.Generic;
using System.Threading;
using WebDAVContent.GUID.Sync;

namespace WEBDAVTests
{
    
    
    /// <summary>
    ///This is a test class for WebDAVClientFacadeTest and is intended
    ///to contain all WebDAVClientFacadeTest Unit Tests
    ///</summary>
    [TestClass()]
    public class WebDAVClientFacadeTest
    {

        /** <summary> Diretorio de teste para as operacoes com o WebDAV </summary> */
        private static readonly string WebDAV_TEST_DIR = TestSetup.TEST_BASE_DIR + @"WebDAVOps\";

        /** <summary> Diretorio de arquivos de testes para as operacoes com o WebDAV </summary>*/
        private static readonly string WebDAV_TEST_CONTENT_DIR = WebDAV_TEST_DIR + @"files\";

        /** <summary> Diretorio de arquivos de testes para as operacoes com </summary> */
        private static readonly string WebDAV_TEST_TOBEUPLOADED_DIR = WebDAV_TEST_DIR + @"upload\";

        /** <summary> Diretorio de arquivos de testes para as operacoes com </summary> */
        private static readonly string WebDAV_TEST_TOBEDOWNLOADED_DIR = WebDAV_TEST_DIR + @"download\";

        /** <summary> Diretorio de configuracao de propriedades do WebDAV </summary> */
        private static readonly string WebDAV_TEST_CONF_DIR = WebDAV_TEST_DIR + @"conf\";


        private TestContext testContextInstance;

        private static WebDAVProperties davProperties = WebDAVProperties.getInstance();
            

        /** <summary>
         * Gets or sets the test context which provides
         * information about and functionality for the current test run.
         * </summary>
         */
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        /**
         * <summary> 
         * Prepara o ambiente de testes
         * </summary>
         */
        [ClassInitialize()]
        public static void setupTestEnvironment(TestContext context)
        {
            

            // setup basico
            TestSetup.setupTestEnviroment();
            // cria diretorio de teste do WebDAV
            Directory.CreateDirectory(WebDAV_TEST_DIR);
            // cria diretorio do arquivo de configuracao
            Directory.CreateDirectory(WebDAV_TEST_CONF_DIR);
            // cria diretorio de arquivos
            Directory.CreateDirectory(WebDAV_TEST_CONTENT_DIR);
            // cria diretorio de arquivos a serem uploadeados
            Directory.CreateDirectory(WebDAV_TEST_TOBEUPLOADED_DIR);
            // cria diretorio de arquivos a serem uploadeados
            Directory.CreateDirectory(WebDAV_TEST_TOBEDOWNLOADED_DIR);

            WebDAVClientFacadeTest.davProperties.setProperty(WebDAVProperties.USERNAME, "frossard");
            WebDAVClientFacadeTest.davProperties.setProperty(WebDAVProperties.PASSWORD, "123456");
            WebDAVClientFacadeTest.davProperties.setProperty(WebDAVProperties.SERVER_ADDRESS, "192.168.1.3");
            
        }
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /**
         * <summary>
         * A test for upload
         * </summary>
         */
        [TestMethod()]
        public void uploadTest()
        {

            WebDAVClientFacade webDAVClient = WebDAVClientFacade.getInstance(WebDAVClientFacadeTest.davProperties);
            FileInfo file = TestSetup.createFile("DAV_UPLOAD_TEST_", WebDAV_TEST_TOBEUPLOADED_DIR);
            PathDescriptor desitnyFoder = this.generatePathDescriptor("");
            webDAVClient.sendFile(file, desitnyFoder);
            webDAVClient.sendFile(file, desitnyFoder);
        }

        private PathDescriptor generatePathDescriptor(string sufix)
        {
            string server = WebDAVClientFacadeTest.davProperties.getProperty(WebDAVProperties.SERVER_ADDRESS);
            PathDescriptor path = WebDAVClientFacade.generatePathDescriptor(server,"", sufix);
            return path;
        }
        /**
        * <summary>
        * A test for upload
        * </summary>
        */
        [TestMethod()]
        public void listFiles()
        {

            WebDAVClientFacade webDAVClient = WebDAVClientFacade.getInstance(WebDAVClientFacadeTest.davProperties);
            PathDescriptor destinyFolder = this.generatePathDescriptor("");
            // TODO adicionar arquivo
            IDictionary <string, WebDAVFileProperties> files =  webDAVClient.listFilesProperties(destinyFolder, false);
            Assert.IsTrue(files.Count > 0);
            // TODO chamar o create e ver se ele que está quebrando as pernas ...
        }

        /**
        * <summary>
        * A test for upload
        * </summary>
        */
        [TestMethod()]
        public void CreateDirTest()
        {

            WebDAVClientFacade webDAVClient = WebDAVClientFacade.getInstance(WebDAVClientFacadeTest.davProperties);
            DateTime now = DateTime.Now;
            PathDescriptor destinyFolder = this.generatePathDescriptor(now.Ticks+"/");
            bool test = webDAVClient.exists(destinyFolder);
            Assert.IsFalse(test);
            webDAVClient.createDir(destinyFolder);
            Thread.Sleep(1000);
            test = webDAVClient.exists(destinyFolder);
            Assert.IsTrue(test);
        }

        /**
         * <summary>
         * A test for download
         * </summary>
         */
        [TestMethod()]
        public void downloadTest()
        {
            
            WebDAVClientFacade webDAVClient =  WebDAVClientFacade.getInstance(WebDAVClientFacadeTest.davProperties);
            FileInfo fileUploaded = TestSetup.createFile("DAV_DOWNLOAD_TEST_", WebDAV_TEST_TOBEUPLOADED_DIR);
            PathDescriptor destinyFolder = this.generatePathDescriptor("");
            webDAVClient.sendFile(fileUploaded, destinyFolder);



            PathDescriptor relativeDownloadSourceFilePath = this.generatePathDescriptor(fileUploaded.Name);
            PathDescriptor relativeDestinyFolderPath = new PathDescriptor(WebDAVClientFacadeTest.WebDAV_TEST_TOBEDOWNLOADED_DIR,"", "");
            string destinyFileName = fileUploaded.Name;

            FileInfo fileDownloaded = webDAVClient.receiveFile(relativeDownloadSourceFilePath, relativeDestinyFolderPath, destinyFileName);
            Assert.IsTrue(fileUploaded.Exists);
            Assert.IsTrue(fileDownloaded.Exists);
            Assert.AreEqual(fileDownloaded.Name, fileUploaded.Name);
            Assert.AreEqual(fileDownloaded.Length, fileUploaded.Length);
        }

        /// <summary>
        ///A test for delete
        ///</summary>
        [TestMethod()]
        public void deleteAndExistsTest()
        {
            WebDAVClientFacade webDAVClient = WebDAVClientFacade.getInstance(WebDAVClientFacadeTest.davProperties);
            FileInfo fileUploaded = TestSetup.createFile("DAV_DELETE_TEST", WebDAV_TEST_TOBEUPLOADED_DIR);
            PathDescriptor destinyFolder = this.generatePathDescriptor("");
            webDAVClient.sendFile(fileUploaded, destinyFolder);
            PathDescriptor relativeDelete = this.generatePathDescriptor(fileUploaded.Name);
            bool exists = webDAVClient.exists(relativeDelete);
            Assert.IsTrue(exists);
            exists = webDAVClient.exists(relativeDelete);
            Assert.IsTrue(exists);
            string relativeDestinyFolderPath = WebDAVClientFacadeTest.WebDAV_TEST_TOBEDOWNLOADED_DIR;
            string destinyFileName = fileUploaded.Name;
            webDAVClient.delete(relativeDelete);
            exists = webDAVClient.exists(relativeDelete);
            Assert.IsFalse(exists);
        }

        /// <summary>
        ///A test for authenticate
        ///</summary>
        [TestMethod()]
        public void authenticateTest()
        {
            WebDAVClientFacade webDAVClient = WebDAVClientFacade.getInstance(WebDAVClientFacadeTest.davProperties); // TODO: Initialize to an appropriate value
            PathDescriptor destinyFolder = this.generatePathDescriptor("");
            webDAVClient.authenticate(destinyFolder);
            // Se não lançou exeção é porque funcionou
            
        }
    }
}
