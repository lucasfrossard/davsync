﻿using WebDAVContent.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WEBDAVTests.TestFiles;
using System.IO;

namespace WEBDAVTests
{
    
    
    /**
     * <summary>
     * This is a test class for DirectoryConfPropertiesTest and is intended
     * to contain all DirectoryConfPropertiesTest Unit Tests
     * </summary>
     */
    [TestClass()]
    public class DirectoryConfPropertiesTest
    {
        /**
         * <summary>
         * Diretorio de teste do arquivo de configuracao
         * </summary>
         */
        private static readonly string TEST_CONF_DIR = TestSetup.TEST_BASE_DIR + @"\conf";
        /**
         * <summary>
         * Nome do arquivo de configuracao
         * </summary>
         */
        private static readonly string TEXT_CONF_FILENAME = ".directories.conf";
        /**
         * <summary>
         * Caminho para o arquivo de configuracao
         * </summary>
         */
        private static readonly string TEXT_CONF_FILEPATH = TEST_CONF_DIR + @"\" + TEXT_CONF_FILENAME;

        private TestContext testContextInstance;

        /**
         * <summary>
         * Gets or sets the test context which provides
         * information about and functionality for the current test run.
         * </summary>
         */
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        /**
         * <summary> 
         * Prepara o ambiente de testes
         * </summary>
         */
        [ClassInitialize()]
        public static void setupTestEnvironment(TestContext context)
        {
            // setup basico
            TestSetup.setupTestEnviroment();
            // cria diretorio do arquivo de configuracao
            Directory.CreateDirectory(TEST_CONF_DIR);
            // cria arquivo de configuracao
            DirectoryConfPropertiesTest.createConfigurationFile();
        }
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion





        /**
         * <summary>
         * A test for load
         * </summary>
         */
        [TestMethod()]
        public void loadTest()
        {
            DirectoryConfProperties target = DirectoryConfProperties.getInstance(); // TODO: Initialize to an appropriate value
            string directoryConfFile = TEXT_CONF_FILEPATH; // TODO: Initialize to an appropriate value
            target.load(directoryConfFile);
            string personalDir = target.getProperty(DirectoryConfProperties.LOCAL_PERSONAL_DIR);
            string corporateDir = target.getProperty(DirectoryConfProperties.LOCAL_CORPORATE_DIR);
            Assert.IsTrue(personalDir.Length > 0);
            Assert.IsTrue(corporateDir.Length > 0);
            
        }

        /**
         * <summary>
         * Cria exemplo do arquivo de configuracao
         * </summary>
         */
        private static void createConfigurationFile() {
            FileInfo dirConfFile = new FileInfo(DirectoryConfPropertiesTest.TEXT_CONF_FILEPATH);
            if (dirConfFile.Exists) {
                dirConfFile.Delete();
            }
            StreamWriter textWriter = dirConfFile.CreateText();
            textWriter.WriteLine("# Diretorios de sincronizacao");
            textWriter.WriteLine();
            textWriter.WriteLine(DirectoryConfProperties.LOCAL_PERSONAL_DIR+"=C:\\temp\\test\\personal");
            textWriter.WriteLine(DirectoryConfProperties.LOCAL_CORPORATE_DIR+"=C:\\temp\\test\\corporate");
            textWriter.Flush();
            textWriter.Close();
        }
    }
}
