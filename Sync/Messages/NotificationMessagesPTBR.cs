﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Sync.Messages
{
    class NotificationMessagesPTBR : NotificationMessages
    {

        private static NotificationMessagesPTBR instance;

        public static NotificationMessagesPTBR getInstance() {
            if (instance == null) {
                instance = new NotificationMessagesPTBR();
            }
            return instance;
        }

        private NotificationMessagesPTBR() : base() {
            this.addMessage(NotificationMessageKey.ERROR_AUTHENTICATING_ON_DESTINY, "Falha ao autenticar no servidor.");
            this.addMessage(NotificationMessageKey.ERROR_CREATING_DIRECTORY_ON_DESTINY, "Falha ao criar diretório no servidor.");
            this.addMessage(NotificationMessageKey.ERROR_CREATING_DIRECTORY_ON_SOURCE, "Falha ao criar diretório local.");
            this.addMessage(NotificationMessageKey.ERROR_DOWNLOADING_FILE_FROM_DESTINY, "Falha ao fazer o download do arquivo.");
            this.addMessage(NotificationMessageKey.ERROR_REMOVING_DIRECTORY_ON_DESTINY, "Falha ao remover diretório no servidor.");
            this.addMessage(NotificationMessageKey.ERROR_REMOVING_DIRECTORY_ON_SOURCE, "Falha ao remover diretório local.");
            this.addMessage(NotificationMessageKey.ERROR_REMOVING_FILE_ON_DESTINY, "Falha ao remover o arquivo no servidor.");
            this.addMessage(NotificationMessageKey.ERROR_REMOVING_FILE_ON_SOURCE, "Falha ao remover arquivo localmente.");
            this.addMessage(NotificationMessageKey.ERROR_UPLOADING_FILE_TO_DESTINY, "Falha ao enviar arquivo para o servidor.");
            this.addMessage(NotificationMessageKey.ERROR_SERVER_NOT_RESPONDING, "O servidor não responde, verifique sua conexão.");
            this.addMessage(NotificationMessageKey.ERROR_SYNCRONIZING, "Falha ao sincronizar item!");

            this.addMessage(NotificationMessageKey.ERROR_SYNCRONIZING_PERSONAL_FOLDER, "Falha ao sincronizar pasta pessoal.");
            this.addMessage(NotificationMessageKey.ERROR_SYNCRONIZING_CORP_FOLDER, "Falha ao sincronizar pasta corporativa.");

            this.addMessage(NotificationMessageKey.INFO_SYNC_STARTED, "Sincronização iniciada.");
            this.addMessage(NotificationMessageKey.INFO_SYNC_STOPPED, "Sincronização parada.");
            this.addMessage(NotificationMessageKey.INFO_APP_RUNNING, "Aplicação foi inicializada.");
            this.addMessage(NotificationMessageKey.INFO_APP_SYNC_PAUSE, "A sincronização foi pausada.");

            this.addMessage(NotificationMessageKey.SUCCESS_CREATING_DIRECTORY_ON_DESTINY, "Novo diretório criado no servidor.");
            this.addMessage(NotificationMessageKey.SUCCESS_CREATING_DIRECTORY_ON_SOURCE, "Novo diretório criado localmente.");
            this.addMessage(NotificationMessageKey.SUCCESS_DOWNLOADING_FILE_FROM_DESTINY, "Novo arquivo sincronizado localmente.");
            this.addMessage(NotificationMessageKey.SUCCESS_REMOVING_DIRECTORY_ON_DESTINY, "Diretório removido no servidor.");
            this.addMessage(NotificationMessageKey.SUCCESS_REMOVING_DIRECTORY_ON_SOURCE, "Diretório removido localmente.");
            this.addMessage(NotificationMessageKey.SUCCESS_REMOVING_FILE_ON_DESTINY, "Arquivo removido no servidor.");
            this.addMessage(NotificationMessageKey.SUCCESS_REMOVING_FILE_ON_SOURCE, "Arquivo removido localmente.");
            this.addMessage(NotificationMessageKey.SUCCESS_SYNC, "Arquivos sincronizado!");
            this.addMessage(NotificationMessageKey.SUCCESS_UPLOADING_FILE_TO_DESTINY, "Arquivos enviado para o servidor com sucesso!");
        }
    }
}
