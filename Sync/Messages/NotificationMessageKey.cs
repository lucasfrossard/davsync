﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Sync.Messages
{
    class NotificationMessageKey
    {
        public static readonly NotificationMessageKey ERROR_DOWNLOADING_FILE_FROM_DESTINY = new NotificationMessageKey("ERROR_DOWNLOADING_FILE_FROM_DESTINY");
        public static readonly NotificationMessageKey ERROR_UPLOADING_FILE_TO_DESTINY = new NotificationMessageKey("ERROR_UPLOADING_FILE_TO_DESTINY");
        public static readonly NotificationMessageKey ERROR_REMOVING_FILE_ON_DESTINY = new NotificationMessageKey("ERROR_REMOVING_FILE_ON_DESTINY");
        public static readonly NotificationMessageKey ERROR_REMOVING_FILE_ON_SOURCE = new NotificationMessageKey("ERROR_REMOVING_FILE_ON_SOURCE");
        public static readonly NotificationMessageKey ERROR_REMOVING_DIRECTORY_ON_DESTINY = new NotificationMessageKey("ERROR_REMOVING_DIRECTORY_ON_DESTINY");
        public static readonly NotificationMessageKey ERROR_REMOVING_DIRECTORY_ON_SOURCE = new NotificationMessageKey("ERROR_REMOVING_DIRECTORY_ON_SOURCE");
        public static readonly NotificationMessageKey ERROR_AUTHENTICATING_ON_DESTINY = new NotificationMessageKey("ERROR_AUTHENTICATING_ON_DESTINY");
        public static readonly NotificationMessageKey ERROR_CREATING_DIRECTORY_ON_DESTINY = new NotificationMessageKey("ERROR_CREATING_DIRECTORY_ON_DESTINY");
        public static readonly NotificationMessageKey ERROR_CREATING_DIRECTORY_ON_SOURCE = new NotificationMessageKey("ERROR_CREATING_DIRECTORY_ON_SOURCE");
        public static readonly NotificationMessageKey ERROR_SYNCRONIZING_PERSONAL_FOLDER = new NotificationMessageKey("ERROR_SYNCRONING_PERSONAL_FOLDER");
        public static readonly NotificationMessageKey ERROR_SYNCRONIZING_CORP_FOLDER = new NotificationMessageKey("ERROR_SYNCRONING_CORP_FOLDER");
        public static readonly NotificationMessageKey ERROR_SERVER_NOT_RESPONDING = new NotificationMessageKey("ERROR_SERVER_NOT_RESPONDING");


        public static readonly NotificationMessageKey ERROR_SYNCRONIZING = new NotificationMessageKey("ERROR_SYNCRONIZING");
        

        public static readonly NotificationMessageKey INFO_SYNC_STARTED = new NotificationMessageKey("INFO_SYNC_STARTED");
        public static readonly NotificationMessageKey INFO_SYNC_STOPPED = new NotificationMessageKey("INFO_SYNC_STOPPED");
        public static readonly NotificationMessageKey INFO_APP_RUNNING = new NotificationMessageKey("INFO_APP_RUNNING");
        public static readonly NotificationMessageKey INFO_APP_SYNC_PAUSE = new NotificationMessageKey("INFO_APP_SYNC_PAUSE");

        public static readonly NotificationMessageKey SUCCESS_DOWNLOADING_FILE_FROM_DESTINY = new NotificationMessageKey("SUCCESS_DOWNLOADING_FILE_FROM_DESTINY");
        public static readonly NotificationMessageKey SUCCESS_UPLOADING_FILE_TO_DESTINY = new NotificationMessageKey("SUCCESS_UPLOADING_FILE_TO_DESTINY");
        public static readonly NotificationMessageKey SUCCESS_REMOVING_FILE_ON_DESTINY = new NotificationMessageKey("SUCCESS_REMOVING_FILE_ON_DESTINY");
        public static readonly NotificationMessageKey SUCCESS_REMOVING_FILE_ON_SOURCE = new NotificationMessageKey("SUCCESS_REMOVING_FILE_ON_SOURCE");
        public static readonly NotificationMessageKey SUCCESS_REMOVING_DIRECTORY_ON_DESTINY = new NotificationMessageKey("SUCCESS_REMOVING_DIRECTORY_ON_DESTINY");
        public static readonly NotificationMessageKey SUCCESS_REMOVING_DIRECTORY_ON_SOURCE = new NotificationMessageKey("SUCCESS_REMOVING_DIRECTORY_ON_SOURCE");
        public static readonly NotificationMessageKey SUCCESS_CREATING_DIRECTORY_ON_DESTINY = new NotificationMessageKey("SUCCESS_CREATING_DIRECTORY_ON_DESTINY");
        public static readonly NotificationMessageKey SUCCESS_CREATING_DIRECTORY_ON_SOURCE = new NotificationMessageKey("SUCCESS_CREATING_DIRECTORY_ON_SOURCE");

        public static readonly NotificationMessageKey SUCCESS_SYNC = new NotificationMessageKey("SUCCESS_SYNC");



        

        private string key;

        public string Key
        {
            get { return key; }
        }

        private NotificationMessageKey (string key) {
            this.key = key;
        }

        
        override
        public bool Equals(Object key)
        {
            if (key is NotificationMessageKey)
            {
                NotificationMessageKey realKey = (NotificationMessageKey)key;
                bool equals = true;
                equals = equals && realKey.Key.Equals(this.Key);
                return equals;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.Key.GetHashCode();
        }
    }
}
