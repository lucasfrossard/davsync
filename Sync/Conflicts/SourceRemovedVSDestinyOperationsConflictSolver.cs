﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades;
using WebDAVContent.Sync;

namespace WebDAVContent.GUID.Sync.Conflicts
{
    class SourceRemovedVSDestinyOperationsConflictSolver<T, U> : ConflictSolver<T, U> where T : IFileProperties where U : IFileProperties
    {
        public SourceRemovedVSDestinyOperationsConflictSolver(IDictionary<string, T> toBeUpdated, 
                                                           ChangeManager<T> sourceChangeManager, ChangeManager<U> destinyChangeManager)
            : base(toBeUpdated, sourceChangeManager, destinyChangeManager)
        {

        }

        protected override void solveCreatedFileConflict(string operationKey)
        {
            // conflito de removido na origem e arquivo criado no destino - estado inalcançável
            throw new SyncManagerException("Bad, bad, bad. Removed file vs created file is not  valid configuration.");
        }

        protected override void solveUpdatedFileConflict(string operationKey)
        {
            // conflito de arquivo na origemn deletado vs update no destino
            // origem ganha
            this.destinyUpdated.Remove(operationKey);
        }

        protected override void solveRemovedFileConflict(string operationKey)
        {
            // conflito de remocao local vs remocao no servidor
            this.sourceRemovedItems.Remove(operationKey);
            this.destinyRemoved.Remove(operationKey);
            // TODO atualizar o snapshot?
        }

        protected override void solveRemovedDirectoryConflict(string operationKey)
        {
            // não precisa fazer nenhuma das duas operacoes
            this.destinyRemoved.Remove(operationKey);
        }
    }
}
