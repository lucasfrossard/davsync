﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Sync.Conflicts.Exceptions
{
    class InvalidConflictException : Exception
    {
        public InvalidConflictException(string message) : base(message) { 
        }
    }
}
