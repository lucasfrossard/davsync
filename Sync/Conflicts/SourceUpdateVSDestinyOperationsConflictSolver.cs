﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Sync.Conflicts.Exceptions;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.GUID.Sync.Conflicts
{
    class SourceUpdateVSDestinyOperationsConflictSolver<T, U> : ConflictSolver<T,U> where T : IFileProperties where U : IFileProperties
    {
        public SourceUpdateVSDestinyOperationsConflictSolver(IDictionary<string, T> toBeUpdated, 
                                                           ChangeManager<T>sourceChangeManager, ChangeManager<U> destinyChangeManager)
            : base(toBeUpdated, sourceChangeManager, destinyChangeManager)
        {
            
        }

        protected override void solveCreatedFileConflict(string operationKey)
        {
            // conflito update local vs criacao no servidor - estado inalcançável
            throw new InvalidConflictException("Bad, bad, bad. Updated file vs Created file is not  valid configuration.");
        }

        protected override void solveUpdatedFileConflict(string operationKey)
        {
            // conflito de update local vs update no servidor
            // servidor ganha           
            this.sourceAnalizedOperations.Remove(operationKey);
        }

        protected override void solveRemovedFileConflict(string operationKey)
        {
            // conflito de upload local vs remocao no servidor
            // servidor ganha, ou seja, deletara o arquivo local, logo remove da lista de atualizacao
            this.sourceAnalizedOperations.Remove(operationKey);
        }

        protected override void solveRemovedDirectoryConflict(string operationKey)
        {
            // conflito de upload vs remocao do diretorio no servidor
            // servidor ganha, 
            this.sourceAnalizedOperations.Remove(operationKey);
        }
    }
}
