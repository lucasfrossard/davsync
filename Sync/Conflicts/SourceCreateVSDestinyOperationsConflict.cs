﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades;
using WebDAVContent.Sync;

namespace WebDAVContent.GUID.Sync.Conflicts
{
    class SourceCreateVSDestinyOperationsConflict<T,U> : ConflictSolver<T,U> where T : IFileProperties where U : IFileProperties
    {
        public SourceCreateVSDestinyOperationsConflict(IDictionary<string, T> toBeAnalized, 
                                                           ChangeManager<T> sourceChangeManager, ChangeManager<U> destinyChangeManager)
            : base(toBeAnalized, sourceChangeManager, destinyChangeManager)
        {
            
        }

        protected override void solveCreatedFileConflict(string operationKey)
        {
            // conflito de criado na origem e arquivo criado no destino
            // destino ganha
            this.sourceCreatedItems.Remove(operationKey);  
        }

        protected override void solveUpdatedFileConflict(string operationKey)
        {
            // conflito de criacao na oridem vs update no destino
            throw new SyncManagerException("Bad, bad, bad. Created file vs Updated file is not  valid configuration.");
        }

        protected override void solveRemovedFileConflict(string operationKey)
        {
            // conflito de criacao na origem vs remocao no destino - estado inalcançável
            throw new SyncManagerException("Bad, bad, bad. Created file vs Removed file is not  valid configuration."); 
        }

        protected override void solveRemovedDirectoryConflict(string operationKey)
        {
            // conflito de upload vs remocao do diretorio no destino
            // destino ganha, só remover a operacao na origem
            this.sourceCreatedItems.Remove(operationKey);  
        }

    }
}
