﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades;
using WebDAVContent.Sync;

namespace WebDAVContent.GUID.Sync.Conflicts
{
    /**
     * <summary>
     * Classe para a resolução de conflitos. Compara operacoes em uma fonte qualque com diversas operacoes no destino.
     * </summary>
     */
    abstract class ConflictSolver <T, U> : IConflictSolver where T : IFileProperties where U : IFileProperties
    {

        /** <summary> Operações sendo analizadas. </summary> */
        protected IDictionary<string, T> sourceAnalizedOperations;   
        /** <summary> Arquivos ou diretorios criados no destino  a ser comparado.</summary> */
        protected IDictionary<string, U> destinyCreated;
        /** <summary> Arquivos atualizado no destino a ser comparado. </summary> */
        protected IDictionary<string, U> destinyUpdated;
        /** <summary> Arquivos ou diret´rios removidos no destino  a ser comparado. </summary> */
        protected IDictionary<string, U> destinyRemoved;
        /** <summary> Arquivos ou diretorios criados no destino  a ser comparado.</summary> */
        protected IDictionary<string, T> sourceCreatedItems;
        /** <summary> Arquivos atualizado no destino a ser comparado. </summary> */
        protected IDictionary<string, T> sourceUpdatedItems;
        /** <summary> Arquivos ou diret´rios removidos no destino  a ser comparado. </summary> */
        protected IDictionary<string, T> sourceRemovedItems;

        /**
         * <summary>
         * Construtor
         * </summary>
         */
        // TODO encapsular os tipos de operacoes em uma lista propria para que o programa fique mais robusto
        public ConflictSolver(IDictionary<string, T> analizedOperations, ChangeManager<T> sourceChangeManager, ChangeManager<U> destinyChangeManager)
        {
            if (analizedOperations == null || 
                sourceChangeManager == null || 
                destinyChangeManager == null ||
                sourceChangeManager.NewItems == null ||
                sourceChangeManager.removedItems == null ||
                sourceChangeManager.UpdatedItems == null ||
                destinyChangeManager.UpdatedItems == null ||
                destinyChangeManager.removedItems == null ||
                destinyChangeManager.NewItems == null
                )
            {
                throw new ArgumentNullException("Itens inside ChangeManager cannot be null!");
            }
            this.sourceAnalizedOperations = analizedOperations;
            this.destinyCreated = destinyChangeManager.NewItems;
            this.destinyUpdated = destinyChangeManager.UpdatedItems;
            this.destinyRemoved = destinyChangeManager.removedItems;
            this.sourceCreatedItems = sourceChangeManager.NewItems;
            this.sourceUpdatedItems = sourceChangeManager.UpdatedItems;
            this.sourceRemovedItems = sourceChangeManager.removedItems;
        }


        public void solveConflicts() {
            ICollection<string> toBeAnalized = this.sourceAnalizedOperations.Keys;
            ICollection<string> iterableKeyList = new List<string>();
            foreach (string key in toBeAnalized) {
                iterableKeyList.Add(key);
            }
            IEnumerator<string> iterator = iterableKeyList.GetEnumerator();
            iterator.MoveNext();
            string currentKey = iterator.Current;
            while (currentKey != null)
            {

                if (this.destinyCreated.ContainsKey(currentKey))
                {
                    // conflito update local vs criacao no servidor
                    this.solveCreatedFileConflict(currentKey);
                }
                else if (this.destinyUpdated.ContainsKey(currentKey))
                {
                    // conflito de update local vs update no servidor
                    this.solveUpdatedFileConflict(currentKey);
                }
                else if (this.destinyRemoved.ContainsKey(currentKey))
                {
                    // conflito de upload local vs remocao no servidor
                    this.solveRemovedFileConflict(currentKey);
                }
                else if (this.containsAnySubDirectory<U>(currentKey, this.destinyRemoved))
                {
                    // conflito de upload vs remocao do diretorio no servidor
                    this.solveRemovedDirectoryConflict(currentKey);
                }
                else
                {
                    // sem conflitos, somente adiciona a operacao a ser executada
                }
                iterator.MoveNext();
                currentKey = iterator.Current;
            }
        }


        /** 
        * <summary>
        * Reolve conflito da operação com um novo arquivo.
        * </summary>
        * <param name="operationKey">
        * Chave da operação.
        * </param>
        */
        protected abstract void solveCreatedFileConflict(string operationKey);

        /** 
         * <summary>
         * Reolve conflito da operação com um arquivo que foi atualizado.
         * </summary>
         * <param name="operationKey">
         * Chave da operação.
         * </param>
         */
        protected abstract void solveUpdatedFileConflict(string operationKey);

        /** 
         * <summary>
         * Reolve conflito da operação com um arquivo que foi removido.
         * </summary>
         * <param name="operationKey">
         * Chave da operação.
         * </param>
         */
        protected abstract void solveRemovedFileConflict(string operationKey);

        /** 
         * <summary>
         * Reolve conflito da operação com um diretório que foi removido.
         * </summary>
         * <param name="operationKey">
         * Chave da operação.
         * </param>
         */
        protected abstract void solveRemovedDirectoryConflict(string operationKey);

       
        /**
         * <summary>
         * Verifica se o caminho esta contido em algum dos locais.
         * </summary>
         * <param name="path">
         * Caminho a ser comparado.
         * </param>
         * <param name="comparisonDestiny">
         * Destino de comparação.
         * </param>
         */
        private bool containsAnySubDirectory<T>(string path, IDictionary<string, T> comparisonDestiny) where T : IFileProperties {
            path = path.Replace("\\", "/");

            bool ignoreLastElement = path.Length > 0 && !(path.ElementAt(path.Length - 1) == '/');
            string[] splitted = path.Split(new Char[] {'/'});
            string aPath = "/";
            for (int i = 0; i < splitted.Length; i++) {
                // faz a comparacao aqui! Quando for true retorna, se não retorna falso no final do método  
                if (comparisonDestiny.ContainsKey(aPath))
                {
                    return true;
                }

                if (!(i == (splitted.Length - 1) && ignoreLastElement) && splitted[i].Trim().Length > 0)
                {
                    aPath = aPath + splitted[i] + "/";
                }

            }
            // faz a comparacao aqui! Quando for true retorna, se não retorna falso no final do método  
            if (comparisonDestiny.ContainsKey(aPath))
            {
                return true;
            }

            return false;
        }

    }
}
