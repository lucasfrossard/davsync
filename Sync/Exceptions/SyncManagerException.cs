﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Sync
{
    class SyncManagerException : Exception
    {
        /**
         * <summary>
         * Construtor
         * </summary>
         */
        public SyncManagerException(string erroMessage, Exception e) : base (erroMessage, e)
        {
        }


        /**
         * <summary>
         * Construtor
         * </summary>
         */
        public SyncManagerException(string erroMessage)
            : base(erroMessage)
        {
        }
    }
}
