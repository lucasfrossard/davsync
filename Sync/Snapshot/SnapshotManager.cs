﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using WebDAVContent.Util;
using System.IO;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.Sync.Snapshot
{
    // TODO explicar o conceito de snapshot
    public class SnapshotManager<T> where T : IFileProperties
    {

        /** <summary>Prefixo do nome do arquivo de snapshot</summary> */
        private static readonly string SNAPSHOT_FILENAME_PREFIX = "snapshot";

        /**
         * <summary>
         * Classe internar de comparador para dois nomes de arquivos de snapshot.
         * </summary>
         */
        private class SnapshotFileNameComparer : IComparer<string> {


            private static SnapshotFileNameComparer instance;

            /**
             * Construtor privado - singleton
             */
            private SnapshotFileNameComparer() { 
            
            }

            /**
             * <summary>
             * Obtem instancia unica da classe - singleton.
             * </summary>
             */
            public static SnapshotFileNameComparer getInstance() {
                if (SnapshotFileNameComparer.instance == null) {
                    SnapshotFileNameComparer.instance = new SnapshotFileNameComparer();
                }
                return SnapshotFileNameComparer.instance;
            }

            /**
             * <summary>
             * Compara qual a ordenacao dos nomes dos arquivos de snapshot. A data é ignorada pois ela é verificada nas propriedades do arquivo, portanto somente o último dígito é valido.
             * </summary>
             * <param name="firstFilename">
             * Nome do primeiro arquivo.
             * </param>
             * <param name="secondFilename">
             * Nome do segundo arquivo.
             * </param>
             * <returns>
             * 1 se o primeiro argumento vem depois (possui o último número maior).
             * -1 se o primeiro argumento vem antes (possui o último número maior).
             * 0 se são iguais.
             * </returns>
             * 
             */
            public int Compare(string firstFilename, string secondFilename) {
                if (firstFilename.Equals(secondFilename)) {
                    return 0;
                }
                // o nome possui tres partes separadas por underscore (_)
                // a parte que contem a hora de geracao do nome e a segunda
                String[] firstFilenameSplitted = firstFilename.Split('_');
                String[] secondFilenameSplitted = secondFilename.Split('_');
                // cria data e compara, se a nova for menor, se torna a mais velha, se for igual, olha o digito externo
                if ( !(firstFilenameSplitted.Length >= 3) || !(secondFilenameSplitted.Length >= 3)) {
                    throw new ArgumentException("O nome do arquivo de um dos argumentos é invalido");
                }
                // olha o criterio de desempate
                int firstFilenameCounter = int.Parse(firstFilenameSplitted[3]);
                int secondFilenameCounter = int.Parse(secondFilenameSplitted[3]);

                if (firstFilenameCounter < secondFilenameCounter)
                {
                    return -1;
                }
                else if (firstFilenameCounter == secondFilenameCounter)
                {
                    return 0;
                }
                return 1;
            }

        }


        /**
         * <summary>
         * Classe internar de comparador para dois nomes de arquivos de snapshot.
         * </summary>
         */
        private class FileInfoBySnapshotNameComparer : IComparer<FileInfo>
        {


            private static FileInfoBySnapshotNameComparer instance;

            /**
             * Construtor privado - singleton
             */
            private FileInfoBySnapshotNameComparer()
            {

            }

            /**
             * <summary>
             * Obtem instancia unica da classe - singleton.
             * </summary>
             */
            public static FileInfoBySnapshotNameComparer getInstance()
            {
                if (FileInfoBySnapshotNameComparer.instance == null)
                {
                    FileInfoBySnapshotNameComparer.instance = new FileInfoBySnapshotNameComparer();
                }
                return FileInfoBySnapshotNameComparer.instance;
            }

            /**
             * <summary>
             * Compara qual a ordenacao dos nomes dos arquivos de snapshot. O arquivo que contem no nome a data mais antiga vem antes na lista.
             * </summary>
             * <param name="firstFilename">
             * Nome do primeiro arquivo.
             * </param>
             * <param name="secondFilename">
             * Nome do segundo arquivo.
             * </param>
             * <returns>
             * 1 se o primeiro argumento vem depois (possui uma data mais atual em seu nome).
             * -1 se o primeiro argumento vem antes (possui uma data mais antiga em seu nome).
             * 0 se são iguais.
             * </returns>
             * 
             */
            public int Compare(FileInfo firstFile, FileInfo secondFile)
            {
                if (firstFile == null || secondFile == null) {
                    throw new ArgumentNullException("Argumentos não podem ser nulos!");
                }

                if (firstFile.FullName.Equals(secondFile.FullName)) {
                    return 0;
                }

                if (firstFile.CreationTime.Ticks < secondFile.CreationTime.Ticks)
                {
                    return -1;
                }
                else if (firstFile.CreationTime.Ticks == secondFile.CreationTime.Ticks)
                {
                    string firstFilename = firstFile.Name;
                    string secondFilename = secondFile.Name;

                    SnapshotFileNameComparer comparer = SnapshotFileNameComparer.getInstance();
                    return comparer.Compare(firstFilename, secondFilename);

                }
                return 1;
            }

        }



        /**
         * <summary>
         * Instancia unica do snapshot manager
         * </summary>
         */
        private static SnapshotManager<T> instance = null;

        /**
         * <summary>
         * Construtor privado para o singleton
         * </summary>
         */
        private SnapshotManager()
            : base()
        { 
        
        }

        /**
         * <summary>
         * Obtem instancia unica do SnapshotManager
         * </summary>
         */
        public static SnapshotManager<T> getInstance()
        {
            if (SnapshotManager<T>.instance == null)
            {
                SnapshotManager<T>.instance = new SnapshotManager<T>();
            }
            return SnapshotManager<T>.instance;
        }


        /**
         * <summary>
         * Carrega o snapshot mais antigo presente na pasta.
         * </summary>
         * <param name="snapshotDir">
         * Diretorio de snapshot
         * </param>
         * <returns>
         * Lista de propriedades de arquivos no momento do snapshot.
         * </returns>
         */
        public IDictionary<string, T> loadOldestSnapshot(String snapshotDir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(snapshotDir);
            if (directoryInfo.Exists)
            {
                // Lista arquivos nos diretorios
                IEnumerable<FileInfo> files = directoryInfo.EnumerateFiles();
                // Escolhe o arquivo mais antigo
                /**
                 * Numa execucao normal, o ideal e que exista somente um unico arquivo nessa pasta, 
                 * salvo o curto periodo de tempo em que um novo snapshot foi salvo e que o ultimo 
                 * ainda nao foi removido (essas duas operacoes devem ocorrer sequencialmente).
                 */
                if (files != null && files.Count() > 0)
                {
                    string oldestSnapshotFilePath = SnapshotManager<T>.discoverOldestSnapshotFilepath(files);

                    // le arquivo importanto o XML
                    using (StreamReader reader = File.OpenText(oldestSnapshotFilePath))
                    {
                        IDictionary<string, T> filePropertiesSet = new Dictionary<string, T>();
                        Type t = typeof(T);
                        IXmlConverterUtil<T> xmlConverter = XMLConverterUtilFactory<T>.getXmlConverterUtil(t);
                        xmlConverter.importFromXML(reader, filePropertiesSet);
                        reader.Close();
                        // retorna a lista de propriedades de arquivo.
                        return filePropertiesSet;
                    }                    
                }
                // retorna uma lista vazia indicando que o snapshot mais velho é vazio.
                return new Dictionary<string, T>();
            }
            else
            {
                throw new SnapshotManagerException("Fail reading snapshot directory. Does it exists?");
            }
        }

        /**
         * <summary>
         * Descobre o caminho para o arquivo que contem o snapshot mais antigo.
         * </summary>
         * <param name="files">
         * Lista de arquivos existentes no diretorio
         * </param>
         * <returns>
         * Caminho do arquivo que contem o snapshot mais velho.
         * </returns>
         */
        private static string discoverOldestSnapshotFilepath(IEnumerable<FileInfo> files)
        {
            if (files == null || !(files.Count() > 0)) {
                throw new ArgumentException("There must be at least on element on the list");
            }
            SortedSet<FileInfo> sortedFileInfo = new SortedSet<FileInfo>(files, FileInfoBySnapshotNameComparer.getInstance());
            return sortedFileInfo.First().FullName;
        }


        /**
         * <summary>
         * Salva o snapshot.
         * </summary>
         * <param name="fileList">
         * Lista de propriedades de arquivo a ser salva.
         * </param>
         * <param name="snapshotDir">
         * Diretorio para salvar o snapshot;
         * </param>
         * <returns>
         * Nome do arquivo de snapshot criado
         * </returns>
         */
        public string saveSnapshot(IEnumerable<T> fileList, string snapshotDir)
        {

            // exporta para xml
            StringBuilder newSnapshotFilepath = this.generateNextSnapshotName(snapshotDir);

            // exporta o xml
            StringBuilder exportedXML = new StringBuilder();
            Type type = typeof(T);
            IXmlConverterUtil<T> xmlConverter = XMLConverterUtilFactory<T>.getXmlConverterUtil(type);
            xmlConverter.exportToXML(fileList, exportedXML);

            
            // Cria todo o diretorio
            if (!Directory.Exists(snapshotDir))
            {
                Directory.CreateDirectory(snapshotDir);
            }
            // Cria e escreve no arquivo
            using (FileStream fs = new FileStream(newSnapshotFilepath.ToString(), FileMode.CreateNew, FileAccess.Write, FileShare.None))
            {
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(exportedXML);
                sw.Flush();
                sw.Close();
            }
            return newSnapshotFilepath.ToString();
        }

        /**
         * <summary>
         * Deleta os snapshots mais antigo contidos nos diretorios.
         * </summary>
         * <param name="keepMostRecent">
         * Lista quantidade de snapshots mais recentes a serem mantidos.
         * </param>
         * <param name=param name="snapshotDir">
         * Diretorio de snapshot
         * </param>
         */
        public void deleteOldestSnapshots(int keepMostRecent, string snapshotDir ) { 
            DirectoryInfo directoryInfo = new DirectoryInfo(snapshotDir);
            if (directoryInfo.Exists)
            {
                // Lista arquivos nos diretorios
                IEnumerable<FileInfo> files = directoryInfo.EnumerateFiles();
                SortedSet<FileInfo> sortedFileInfo = new SortedSet<FileInfo>(files, FileInfoBySnapshotNameComparer.getInstance());

                for (int i = 0; i < (sortedFileInfo.Count() - keepMostRecent); i++) {
                    FileInfo fi = sortedFileInfo.ElementAt(i);
                    fi.Delete();
                }
            }
        
        }

        /**
         * <summary>
         * Gera o nome do proximo snapshot
         * </summary>
         * <param name="snapshotDir">
         * Diretorio do snapshot
         * </param>
         * <return>
         * Nome do proximo snapshot 
         * </return>
         */
        private StringBuilder generateNextSnapshotName(String snapshotDir)
        {
            // gera o nome do arquivo SNAPSHOT_DATAGERACAO_CONTADOR
            int counter = 0;
            StringBuilder newfilepath = new StringBuilder();
            newfilepath.Append(snapshotDir).Append(@"\").Append(SnapshotManager<T>.SNAPSHOT_FILENAME_PREFIX).Append("_").Append(DateTime.Now.Ticks).Append("_");
            while (File.Exists(newfilepath.ToString() + counter))
            {
                counter++;
            }
            newfilepath.Append(counter);
            // nome do arquivo gerado
            return newfilepath;
        }

    }
}
