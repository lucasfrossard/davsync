﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.Sync
{
    class SnapshotManagerException :Exception
    {
         /**
         * <summary>
         * Construtor
         * </summary>
         */
        public SnapshotManagerException(string erroMessage, Exception e)
            : base(erroMessage, e)
        {
        }

        /**
         * <summary>
         * Construtor
         * </summary>
         */
        public SnapshotManagerException(string erroMessage)
            : base(erroMessage)
        {
        }
    }
}
