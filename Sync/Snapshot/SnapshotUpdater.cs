﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Facades;
using WebDAVContent.Manager;

namespace WebDAVContent.GUID.Sync.Snapshot
{
    abstract class SnapshotUpdater
    {
        public abstract void updateSnapshot<T>(T fileProperties, ChangeManager<T> changeManager, LocationController<T> locationController)
            where T : IFileProperties;
    }
}
