﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Facades;
using WebDAVContent.Sync.Snapshot;

namespace WebDAVContent.GUID.Sync.Snapshot
{
    class RemoveFromSnapshot : SnapshotUpdater
    {
        private static RemoveFromSnapshot instance;

        private RemoveFromSnapshot() { 
        
        }

        public static RemoveFromSnapshot getInstance() {
            if (instance == null) {
                instance = new RemoveFromSnapshot();
            }
            return instance;
        }


        public override void updateSnapshot<T>(T fileProperties, ChangeManager<T> changeManager, LocationController<T> locationController) { 
            IDictionary<string, T> lastExecutionSnapshot = changeManager.LastExecutionSnapshot;
            lastExecutionSnapshot.Remove(fileProperties.getRelativePath());
            SnapshotManager<T>.getInstance().saveSnapshot(lastExecutionSnapshot.Values, locationController.SnapshotFolder);
            SnapshotManager<T>.getInstance().deleteOldestSnapshots(1, locationController.SnapshotFolder);
        }
    }


}
