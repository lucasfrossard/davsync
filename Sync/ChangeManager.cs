﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using WebDAVContent.Sync.Snapshot;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Sync;

namespace WebDAVContent.Sync
{

    
    /**
     * <summary>
     * Objetos desta classe lista modificacoes em arquivos de uma mesma origem - ou de um cliente, ou de um servidor, por exemplo - fazendo a 
     * comparação entre entre adois estados, por exemplo o estado atual dos arquivos de um determinado diretorio e um estado anterior.
     * </summary>
     */
    public class ChangeManager <T> where T : IFileProperties
    {

        
        /** <summary>Lista de arquivos a serem atualizadoss</summary> */
        private IDictionary<string, T> updatedItems = new Dictionary<string, T>();

        public IDictionary<string, T> UpdatedItems
        {
            get { return this.updatedItems; }
        }
        /** <summary>Lista de novos arquivos</summary>*/
        private IDictionary<string, T> newItems = new Dictionary<string, T>();

        public IDictionary<string, T> NewItems
        {
            get { return this.newItems; }
        }
        /** <summary>Lista de arquivos a serem removidos</summary> */
        private IDictionary<string, T> removeItems = new Dictionary<string, T>(); // será composto do que restar da lista do snapshot da ultima execucao

        public IDictionary<string, T> removedItems
        {
            get { return this.removeItems; }
        }



        /** SNAPSHOTS */
        private IDictionary<string, T> lastExecutionSnapshot;

        /** <summary> Snapshot da ultima execução </summary> */
        public IDictionary<string, T> LastExecutionSnapshot
        {
            get { return lastExecutionSnapshot; }
        }

        private IDictionary<string, T> currentSnapshot;

        /** <summary> Snapshot atual </summary> */
        public IDictionary<string, T> CurrentSnapshot
        {
            get { return currentSnapshot; }
        }

        /** <summary>Gerenciador de arquivos</summary> */
        private IFileManagerFacade<T> fileManagerFacade;

        public ChangeManager(IFileManagerFacade<T> fileManagerFacade) {
            if (fileManagerFacade == null) {
                throw new ArgumentNullException("O argumento não pode ser nulo!");
            }
            this.fileManagerFacade = fileManagerFacade;
        }


        /**
         * <summary>
         * Compara o estado atual de um determinado local (cliente ou servidor) de arquivo com o estado anterior e checa por mudanças.
         * </summary>
         * * <param name="relativeSyncDirectoryPath">
         * Caminho relativo do diretorio a ser sincronizado
         * </param>
         * <param name="snapshotDir">
         * Diretorio do snapshot
         * </param>
         */
        public void discoverChanges(PathDescriptor relativeSyncDirectoryPath, string snapshotDir) {

            // obtem snapshots - anterior e atual
            this.lastExecutionSnapshot = SnapshotManager<T>.getInstance().loadOldestSnapshot(snapshotDir);
            this.currentSnapshot = this.fileManagerFacade.listFilesProperties(relativeSyncDirectoryPath, false);
            this.discoverChanges(this.lastExecutionSnapshot, this.currentSnapshot);
        }

        /**
         * <summary>
         * Compara o estado atual de um determinado local (cliente ou servidor) de arquivo com o estado anterior e checa por mudanças.
         * </summary>
         * <param name="lastExecutionSnapshot">
         * Snapshot da ultima execuçao
         * </param>
         * <param name="currentSnapshot">
         * Snapshot atual.
         * </param>
         */
        private void discoverChanges(IDictionary<string, T> lastExecutionSnapshot, IDictionary<string, T> currentSnapshot)
        {
            // copia auzilia
            IDictionary<string, T> auxLastExecutionSnapshot = new Dictionary<string, T>();
            foreach (KeyValuePair<string, T> pair in lastExecutionSnapshot)
            {
                auxLastExecutionSnapshot[pair.Key]= pair.Value;
                
            }
            // TODO explicar este trecho
            foreach (KeyValuePair<string, T> kvpCurrentSnapshot in currentSnapshot)
            {
                string relativePath = kvpCurrentSnapshot.Key;
                T fileProperties = kvpCurrentSnapshot.Value;
                // se contem a chave, verifica se o arquivo foi modificado
                if (auxLastExecutionSnapshot.ContainsKey(relativePath))
                {
                    // se foi modificado
                    if (!auxLastExecutionSnapshot[relativePath].Equals(fileProperties))
                    {
                        // inclui na lista de atualizacao somente se for arquivo, um diretorio atualizado nao serve pra nada
                        if (!fileProperties.IsDirectory)
                        {
                            this.updatedItems.Add(relativePath, fileProperties);
                        }
                    }
                    // se sao iguais nao a nada a fazer.
                    // remove o arquivo da lista, para que ao final da iteracao fique sobrando somente aqueles que sumiram, que formarao entao os arquivos a serem deletados
                    auxLastExecutionSnapshot.Remove(relativePath);

                }
                else
                {
                    // o arqvuio não existia, inclui na lista de novos arquivos
                    this.newItems.Add(relativePath, fileProperties);

                }
            }

            // ou seja, será removido o que sobrou do snapshot da ultimo execuçao e que não consta na listagem atual.
            foreach (KeyValuePair<string, T> pair in auxLastExecutionSnapshot)
            {
                this.removeItems[pair.Key]=pair.Value;
            }
        }
    }
}
