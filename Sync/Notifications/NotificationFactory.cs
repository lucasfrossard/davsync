﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Notifications;
using WebDAVContent.GUID.Sync.Messages;
using System.Windows.Forms;

namespace WebDAVContent.GUID.Sync.Notifications
{
    class NotificationFactory
    {
        private static NotificationFactory instance = null;

        private NotificationMessagesPTBR messages = NotificationMessagesPTBR.getInstance();

        private NotificationFactory() { 

        }

        public static NotificationFactory getInstance()
        {
           if (instance == null){
                instance = new NotificationFactory();
            }
            return instance;
        }

        /**
         * <summary>
         * Obtem notificação.
         * </summary>
         * <param name="key">
         * Chave da notificação.
         * </param>
         */
        public Notification getNotification(NotificationMessageKey key) {
            Notification notification = null;
            if (key.Equals(NotificationMessageKey.ERROR_AUTHENTICATING_ON_DESTINY) ||
                key.Equals(NotificationMessageKey.ERROR_CREATING_DIRECTORY_ON_DESTINY) ||
                key.Equals(NotificationMessageKey.ERROR_CREATING_DIRECTORY_ON_SOURCE) ||
                key.Equals(NotificationMessageKey.ERROR_DOWNLOADING_FILE_FROM_DESTINY) ||
                key.Equals(NotificationMessageKey.ERROR_REMOVING_DIRECTORY_ON_DESTINY) ||
                key.Equals(NotificationMessageKey.ERROR_REMOVING_DIRECTORY_ON_SOURCE) ||
                key.Equals(NotificationMessageKey.ERROR_REMOVING_FILE_ON_DESTINY) ||
                key.Equals(NotificationMessageKey.ERROR_REMOVING_FILE_ON_SOURCE) ||
                key.Equals(NotificationMessageKey.ERROR_UPLOADING_FILE_TO_DESTINY) ||
                key.Equals(NotificationMessageKey.ERROR_SERVER_NOT_RESPONDING) ||
                key.Equals(NotificationMessageKey.ERROR_SYNCRONIZING_PERSONAL_FOLDER) ||
                key.Equals(NotificationMessageKey.ERROR_SYNCRONIZING_CORP_FOLDER)
                ) {
                String messages = this.messages.getMessage(key);
                notification = new Notification(ToolTipIcon.Error, "Erro!", messages, true, 2000);
            } else if (key.Equals(NotificationMessageKey.INFO_SYNC_STARTED) ||
                key.Equals(NotificationMessageKey.INFO_SYNC_STOPPED) ||
                key.Equals(NotificationMessageKey.SUCCESS_CREATING_DIRECTORY_ON_DESTINY) ||
                key.Equals(NotificationMessageKey.SUCCESS_CREATING_DIRECTORY_ON_SOURCE) ||
                key.Equals(NotificationMessageKey.SUCCESS_DOWNLOADING_FILE_FROM_DESTINY) ||
                key.Equals(NotificationMessageKey.SUCCESS_REMOVING_DIRECTORY_ON_DESTINY) ||
                key.Equals(NotificationMessageKey.SUCCESS_REMOVING_DIRECTORY_ON_SOURCE) ||
                key.Equals(NotificationMessageKey.SUCCESS_REMOVING_FILE_ON_DESTINY) ||
                key.Equals(NotificationMessageKey.SUCCESS_REMOVING_FILE_ON_SOURCE) ||
                key.Equals(NotificationMessageKey.SUCCESS_SYNC) ||
                key.Equals(NotificationMessageKey.SUCCESS_UPLOADING_FILE_TO_DESTINY
                )){
                    String messages = this.messages.getMessage(key);
                    notification = new Notification(ToolTipIcon.Info, "Sincronizando ...", messages, true, 2000);

                }
            else if (key.Equals(NotificationMessageKey.INFO_APP_RUNNING))
            {
                String messages = this.messages.getMessage(key);
                notification = new Notification(ToolTipIcon.Info, "MobDisk", messages, true, 2000);
            }
            return notification;
        }
    }
}
