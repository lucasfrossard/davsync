﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Sync.Operations;
using WebDAVContent.GUID.Sync;
using WebDAVContent.GUID.Notifications;

namespace WebDAVContent.Sync
{
    /**
     * <summary>
     * Operação a ser realizada. Para realizar uma operação sobre um arquivo é necessário 
     * definir o arquivo e qual o gerenciador do mesmo, ou a Fachada (Facade) que opera com o arquivo.
     * A Fachada utilizada define o destino da operação.
     * </summaru>
     */
    abstract class SyncOperation<OperationFileProperties, SourceFileProperties, DestinyFileProperties> : ISyncOperation<OperationFileProperties>
        where OperationFileProperties : IFileProperties
        where SourceFileProperties : IFileProperties
        where DestinyFileProperties : IFileProperties
    {

        LocationController<SourceFileProperties> sourceLocationController = null;

        /** <summary> Obtem o objeto de contole da localização e gerencia de arquivo. </summary> */
        LocationController<DestinyFileProperties> destinyLocationController = null;

        /** <summary> Resultado da notificação </summary> */
        private Notification notification;

        ChangeManager<SourceFileProperties> sourceChangeManager;

        ChangeManager<DestinyFileProperties> destinyChangeManager;



        /**
         * <summary>
         * Construtor
         * </summary>
         * <param name=param name="operationItem">
         * Arquivo a ser atualizado.
         * </param>
         * <param name="fileManager">
         * Operador do arquivo, ou a facade que ira realizar a operacao
         * </param>
         */
        protected SyncOperation( ChangeManager<SourceFileProperties> sourceChangeManager,
                                 ChangeManager<DestinyFileProperties> destinyChangeManager,
                                 LocationController<SourceFileProperties> sourceLocationController,
                                 LocationController<DestinyFileProperties> destinyLocationController)
        {
            this.destinyLocationController = destinyLocationController;
            this.sourceLocationController = sourceLocationController;
            this.destinyChangeManager = destinyChangeManager;
            this.sourceChangeManager = sourceChangeManager;
        }

        /** <summary> Resultado da notificação </summary> */
        public Notification Notification
        {
            get { return notification; }
        }


        public LocationController<SourceFileProperties> SourceLocationController
        {
            get { return sourceLocationController; }
        }

        /** <summary> Obtem o objeto de contole da localização e gerencia de arquivo.  </summary> */
        public LocationController<DestinyFileProperties> DestinyLocationController
        {
            get { return destinyLocationController; }
        }

        public ChangeManager<SourceFileProperties> SourceChangeManager
        {
            get { return sourceChangeManager; }
        }

        public ChangeManager<DestinyFileProperties> DestinyChangeManager
        {
            get { return destinyChangeManager; }
        }


        public void execute(OperationFileProperties operationItem)
        {
            this.executeOperation(operationItem);
            this.notification = this.retrieveNotification();
        }

        protected abstract void executeOperation(OperationFileProperties operationItem);

        protected abstract Notification retrieveNotification();

        public abstract SourceFileProperties retrieveSourceFileProperties();

        public abstract DestinyFileProperties retrieveDestinyFileProperties();


    }
}
