﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Notifications;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Sync.Notifications;
using WebDAVContent.GUID.Sync.Messages;

namespace WebDAVContent.GUID.Sync.Operations
{
    class RemoveSyncOperation<SourceFileProperties, DestinyFileProperties> : SyncOperation<SourceFileProperties, SourceFileProperties, DestinyFileProperties>
        where DestinyFileProperties : IFileProperties
        where SourceFileProperties : IFileProperties
    {
        private Notification notification = null;

        private Notification directoryNotification = null;
        private Notification fileNotification = null;

        private SourceFileProperties sourceFileProperties;

        public RemoveSyncOperation(ChangeManager<SourceFileProperties> sourceChangeManager,
                                            ChangeManager<DestinyFileProperties> destinyChangeManager,
                                            LocationController<SourceFileProperties> sourceLocationController,
                                            LocationController<DestinyFileProperties> destinyLocationController, 
                                            // TODO melhorar a factory de notificações para que elas não precisem serem passadas como parâmetro
                                            Notification directoryNotification, 
                                            Notification fileNotification)
            : base(sourceChangeManager,
                   destinyChangeManager,
                   sourceLocationController,
                   destinyLocationController)
        {
            this.directoryNotification = directoryNotification;
            this.fileNotification = fileNotification;
        
        }

        protected override void executeOperation(SourceFileProperties operationItem)
        {
            // Limpa qualquer notificação anterior
            this.notification = null;
            this.sourceFileProperties = operationItem;
            // deleta no servidor Jesus!
            PathDescriptor destinySyncDirectory = this.DestinyLocationController.SyncDirectory;
            PathDescriptor toBeDeletedPath = new PathDescriptor(destinySyncDirectory, operationItem.getRelativePath());
            this.DestinyLocationController.FileManager.delete(toBeDeletedPath);
            // notifica
            if (operationItem.IsDirectory)
            {
                this.notification = this.directoryNotification;
            }
            else
            {
                this.notification = this.fileNotification;
            }
        }

        protected override Notification retrieveNotification()
        {
            return this.notification;
        }

        // TODO: isso aqui não ficou bom. Melhorar!
        public override DestinyFileProperties retrieveDestinyFileProperties()
        {
            return this.DestinyChangeManager.LastExecutionSnapshot[this.sourceFileProperties.getRelativePath()];
        }

        public override SourceFileProperties retrieveSourceFileProperties()
        {
            return this.sourceFileProperties;
        }
    }
}
