﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Notifications;
using System.IO;
using WebDAVContent.Util;
using WebDAVContent.GUID.Sync.Notifications;
using WebDAVContent.GUID.Sync.Messages;

namespace WebDAVContent.GUID.Sync.Operations
{
    class ReceiveUpdatedSyncOperation<OperationFileProperties, SourceFileProperties> : SyncOperation<OperationFileProperties, SourceFileProperties, OperationFileProperties>
        where OperationFileProperties : IFileProperties
        where SourceFileProperties : IFileProperties, new()
    {
        private static ReceiveNewSyncOperation<OperationFileProperties, SourceFileProperties> instance;

        private SourceFileProperties receivedFileOrDirectoryProperties;
        private OperationFileProperties destinyFileProperties;

        private Notification notification = null;



        public ReceiveUpdatedSyncOperation(ChangeManager<SourceFileProperties> sourceChangeManager,
                                         ChangeManager<OperationFileProperties> destinyChangeManager,
                                         LocationController<SourceFileProperties> sourceLocationController,
                                         LocationController<OperationFileProperties> destinyLocationController
                                        )
            : base(sourceChangeManager,
                   destinyChangeManager,
                   sourceLocationController,
                   destinyLocationController)
        {
           
        }

        // TODO bem parecido com o ReceiveNewFileSyncOperation, evitar a duplicidade
        protected override void executeOperation(OperationFileProperties operationItem)
        {
            // Limpa qualquer notificação anterior
            this.notification = null;
            this.destinyFileProperties = operationItem;
                if (!operationItem.IsDirectory)
                {
                    // recebe arquivo do servidor
                    // Remove o que tinha la
                    PathDescriptor sourceSyncDirectory = this.SourceLocationController.SyncDirectory;
                    PathDescriptor localToUpdateFilePath = new PathDescriptor(sourceSyncDirectory, operationItem.getRelativePath());
                    this.SourceLocationController.FileManager.delete(localToUpdateFilePath);
                    FileInfo fi = new FileInfo(localToUpdateFilePath.getFullPath());

                    // Manda o arquivo atualizado
                    PathDescriptor destinySyncDirectory = this.DestinyLocationController.SyncDirectory;


                    PathDescriptor serverSourceFilePath = new PathDescriptor(destinySyncDirectory, operationItem.getRelativePath());
                    PathDescriptor localFileDestinyFolder = new PathDescriptor(sourceSyncDirectory, operationItem.getRelativeDirectory(false));
                    FileInfo fileInfo = this.DestinyLocationController.FileManager.receiveFile(serverSourceFilePath, localFileDestinyFolder, operationItem.Name);


                    this.receivedFileOrDirectoryProperties = new SourceFileProperties();
                    FilePropertiesUtil.export(fileInfo, localFileDestinyFolder, this.receivedFileOrDirectoryProperties);
                    // notifica
                    this.notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_DOWNLOADING_FILE_FROM_DESTINY);
                }
                else {
                    PathDescriptor sourceSyncDirectory = this.SourceLocationController.SyncDirectory;
                    DirectoryInfo dirInfo = new DirectoryInfo(sourceSyncDirectory.getFullPath() + operationItem.RelativePath);
                    this.receivedFileOrDirectoryProperties = new SourceFileProperties();
                    FilePropertiesUtil.export(dirInfo, sourceSyncDirectory, this.receivedFileOrDirectoryProperties);
                }
        }

        public override SourceFileProperties retrieveSourceFileProperties() {
            return this.receivedFileOrDirectoryProperties;
        }



        public override OperationFileProperties retrieveDestinyFileProperties()
        {
            return this.destinyFileProperties;
        }

        protected override Notification retrieveNotification() {
            return this.notification;
        }
    }

}
