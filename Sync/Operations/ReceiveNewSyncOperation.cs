﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Notifications;
using System.IO;
using WebDAVContent.Util;
using WebDAVContent.GUID.Sync.Notifications;
using WebDAVContent.GUID.Sync.Messages;

namespace WebDAVContent.GUID.Sync.Operations
{
    class ReceiveNewSyncOperation<OperationFileProperties, SourceFileProperties> : SyncOperation<OperationFileProperties, SourceFileProperties, OperationFileProperties>
        where OperationFileProperties : IFileProperties
        where SourceFileProperties : IFileProperties, new()
    {
        private static ReceiveNewSyncOperation<OperationFileProperties, SourceFileProperties> instance;

        private SourceFileProperties receivedFileOrDirectoryProperties;
        private OperationFileProperties destinyFileProperties;

        private Notification notification = null;



        public ReceiveNewSyncOperation(ChangeManager<SourceFileProperties> sourceChangeManager,
                                         ChangeManager<OperationFileProperties> destinyChangeManager,
                                         LocationController<SourceFileProperties> sourceLocationController,
                                         LocationController<OperationFileProperties> destinyLocationController
                                        )
            : base(sourceChangeManager,
                   destinyChangeManager,
                   sourceLocationController,
                   destinyLocationController)
        {
           
        }

        protected override void executeOperation(OperationFileProperties operationItem)
        {
            // Limpa qualquer notificação anterior
            this.notification = null;
            this.destinyFileProperties = operationItem;
            PathDescriptor sourceSyncDirectory = this.SourceLocationController.SyncDirectory;
            if (!operationItem.IsDirectory)
            {
                // TODO deletar localmente só pra garantir?
                PathDescriptor destinySyncDirectory = this.DestinyLocationController.SyncDirectory;
                PathDescriptor serverSourceFilePath = new PathDescriptor(destinySyncDirectory, operationItem.getRelativePath());
                PathDescriptor localFileDestinyFolder = new PathDescriptor(sourceSyncDirectory, operationItem.getRelativeDirectory(false));

                FileInfo fileInfo = this.DestinyLocationController.FileManager.receiveFile(serverSourceFilePath, localFileDestinyFolder, operationItem.Name);
                this.receivedFileOrDirectoryProperties = new SourceFileProperties();
                FilePropertiesUtil.export(fileInfo, localFileDestinyFolder, this.receivedFileOrDirectoryProperties);
                // notifica
                this.notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_DOWNLOADING_FILE_FROM_DESTINY);
                
            }
            else
            {
                PathDescriptor localNewFolderDestiny = new PathDescriptor(sourceSyncDirectory.ABase, sourceSyncDirectory.RootFolder, operationItem.getRelativePath());
                this.receivedFileOrDirectoryProperties = this.SourceLocationController.FileManager.createDir(localNewFolderDestiny);
                // notifica
                this.notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_CREATING_DIRECTORY_ON_SOURCE);
            }            
        }

        public override SourceFileProperties retrieveSourceFileProperties() {
            return this.receivedFileOrDirectoryProperties;
        }



        public override OperationFileProperties retrieveDestinyFileProperties()
        {
            return this.destinyFileProperties;
        }

        protected override Notification retrieveNotification() {
            return this.notification;
        }
    }
}
