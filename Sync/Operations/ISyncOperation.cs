﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.GUID.Sync.Operations
{
    public interface ISyncOperation<T> where T : IFileProperties
    {

        void execute(T operationItem);

    }
}
