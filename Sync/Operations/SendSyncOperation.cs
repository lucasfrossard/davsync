﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using System.IO;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Sync;
using WebDAVContent.GUID.Notifications;
using WebDAVContent.GUID.Sync.Notifications;
using WebDAVContent.GUID.Sync.Messages;

namespace WebDAVContent.Sync.Operations
{
    /**
     * <summary>
     * Operacao de envio de arquivo.
     * </summary>
     */
    class SendSyncOperation<SourceType, DestinyType> : SyncOperation<SourceType,SourceType,DestinyType> 
        where DestinyType : IFileProperties 
        where SourceType : IFileProperties
    {

        private SourceType sourceFileOrDirectoryProperties;
        private DestinyType sentFileOrDirectoryProperties;


        private Notification notification = null;



        public SendSyncOperation(ChangeManager<SourceType> sourceChangeManager,
                                     ChangeManager<DestinyType> destinyChangeManager,
                                     LocationController<SourceType> sourceLocationController, 
                                     LocationController<DestinyType> destinyLocationController)
            : base(sourceChangeManager,
                   destinyChangeManager, 
                   sourceLocationController, 
                   destinyLocationController)
        { 
        
        }

        protected override void executeOperation(SourceType operationItem)
        {
            // Limpa qualquer notificação anterior
            this.notification = null;
            this.sourceFileOrDirectoryProperties = operationItem;
            PathDescriptor destinySyncDirectory = this.DestinyLocationController.SyncDirectory;
            if (operationItem.IsDirectory)
            {
                // envia para o servidor
                PathDescriptor destinyToBeCreatedPath = new PathDescriptor(destinySyncDirectory.ABase, destinySyncDirectory.RootFolder, operationItem.RelativePath);
                this.sentFileOrDirectoryProperties = this.DestinyLocationController.FileManager.createDir(destinyToBeCreatedPath);
                // notifica
                this.notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_CREATING_DIRECTORY_ON_DESTINY);
            }
            else
            {
                FileInfo fi = new FileInfo(operationItem.AbsolutePath);
                // envia para o servidor
                
                PathDescriptor destinyToBeCreatedPath = new PathDescriptor(destinySyncDirectory.ABase, destinySyncDirectory.RootFolder, operationItem.getRelativeDirectory(false));
                this.sentFileOrDirectoryProperties = this.DestinyLocationController.FileManager.sendFile(fi, destinyToBeCreatedPath);
                // notifica
                this.notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_UPLOADING_FILE_TO_DESTINY);
            }
        }

        protected override Notification retrieveNotification() {
            return this.notification;
        }

        public override DestinyType retrieveDestinyFileProperties()
        {
            return this.sentFileOrDirectoryProperties;
        }

        public override SourceType retrieveSourceFileProperties() {
            return this.sourceFileOrDirectoryProperties;
        }

        
    }
}
