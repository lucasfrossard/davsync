﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Notifications;
using WebDAVContent.GUID.Sync.Notifications;
using WebDAVContent.GUID.Sync.Messages;
using System.IO;

namespace WebDAVContent.GUID.Sync.Operations
{
    class UpdateSyncOperation<SourceType, DestinyType> : SyncOperation<SourceType, SourceType, DestinyType> 
        where SourceType : IFileProperties
        where DestinyType :  IFileProperties
    {        private SourceType sourceFileOrDirectoryProperties;
        private DestinyType sentFileOrDirectoryProperties;


        private Notification notification = null;



        public UpdateSyncOperation(ChangeManager<SourceType> sourceChangeManager,
                                     ChangeManager<DestinyType> destinyChangeManager,
                                     LocationController<SourceType> sourceLocationController, 
                                     LocationController<DestinyType> destinyLocationController)
            : base(sourceChangeManager,
                   destinyChangeManager, 
                   sourceLocationController, 
                   destinyLocationController)
        { 
        
        }

        // TODO ta quase igual ao SendFileProperties, evitar a duplicacao de código
        protected override void executeOperation(SourceType operationItem)
        {
            // Limpa qualquer notificação anterior
            this.notification = null;
            this.sourceFileOrDirectoryProperties = operationItem;

            if (!operationItem.IsDirectory)
            {
                FileInfo fi = new FileInfo(operationItem.AbsolutePath);
                // envia para o servidor
                PathDescriptor serverDestinyFolderPath = new PathDescriptor(this.DestinyLocationController.SyncDirectory, operationItem.getRelativeDirectory(false));
                this.sentFileOrDirectoryProperties = this.DestinyLocationController.FileManager.sendFile(fi, serverDestinyFolderPath);
                // notifica
                this.notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_UPLOADING_FILE_TO_DESTINY);
            }
            else
            {
                PathDescriptor serverToUpdateFilePath = new PathDescriptor(this.DestinyLocationController.SyncDirectory, operationItem.getRelativePath());
                IDictionary<string, DestinyType> directoriesProperties = this.DestinyLocationController.FileManager.listFilesProperties(serverToUpdateFilePath, true);
                this.sentFileOrDirectoryProperties = directoriesProperties[operationItem.RelativePath];
            }
        }

        protected override Notification retrieveNotification() {
            return this.notification;
        }

        public override DestinyType retrieveDestinyFileProperties()
        {
            return this.sentFileOrDirectoryProperties;
        }

        public override SourceType retrieveSourceFileProperties() {
            return this.sourceFileOrDirectoryProperties;
        }
    }
}
