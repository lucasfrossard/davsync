﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using System.IO;
using WebDAVContent.Util;
using WebDAVContent.Configuration;
using WebDAVContent.Sync.Operations;
using WebDAVContent.GUID.Sync;
using WebDAVContent.GUID.Facades;
using WebDAVContent.GUID.Sync.Operations;
using WebDAVContent.Sync.Snapshot;
using System.Net.NetworkInformation;
using WebDAVContent.GUID.Notifications;
using System.Windows.Forms;
using WebDAVContent.GUID.Facades.WebDAV.Exceptions;
using WebDAVContent.GUID.Sync.Conflicts;
using WebDAVContent.GUID.Sync.Notifications;
using WebDAVContent.GUID.Sync.Messages;
using WebDAVContent.GUID.Sync.Snapshot;
using WebDAVContent.GUID.Log;

namespace WebDAVContent.Sync
{
    /**
     * Regras de negócio de sincronização.
     */
    class SyncManager
    {
        /** <summary> Caminho dos snapshots  </summary>*/
        private static readonly string SNAPSHOT_FOLDER = @".\snapshot";
        /** <summary> Caminho dos snapshots dos arquivos locais </summary>*/
        private static readonly string LOCAL_PERSONAL_SNAPSHOT_FOLDER =SyncManager.SNAPSHOT_FOLDER +  @"\localpersonalsnapshot";
        /** <summary> Caminho dos snapshots dos arquivos locais </summary>*/
        private static readonly string LOCAL_CORPORATE_SNAPSHOT_FOLDER = SyncManager.SNAPSHOT_FOLDER + @"\localcorporatesnapshot";
        /** <summary> Caminho dos snapshots dos arquivos do servidor </summary>*/
        private static readonly string SERVER_PERSONAL_SNAPSHOT_FOLDER = SyncManager.SNAPSHOT_FOLDER + @"\serverpersonalsnapshot";
        /** <summary> Caminho dos snapshots dos arquivos do servidor </summary>*/
        private static readonly string SERVER_CORPORATE_SNAPSHOT_FOLDER = SyncManager.SNAPSHOT_FOLDER + @"\servercorporatesnapshot";
        /** <summary> Número de tentativas do ping antes de notificar </summary>*/
        private static readonly int PING_TRIES = 10;


        private static SyncManager instance;

        private DirectoryConfProperties syncDirectoriesProperties;

        private Notifications notifications = Notifications.getInstance();

        private SyncManager() {
            // carrega configuracoes
            this.syncDirectoriesProperties = DirectoryConfProperties.getInstance();
            this.syncDirectoriesProperties.load(DirectoryConfProperties.DEFAUL_PROPERTIES_FILE_PATH);
        }

        private SyncManager(DirectoryConfProperties directoryConfProperties) {
            this.syncDirectoriesProperties = directoryConfProperties;
        }


        public static SyncManager getInstance()
        {
            if (SyncManager.instance == null)
            {
                SyncManager.instance = new SyncManager();
            }
            return SyncManager.instance;
        }

        public static SyncManager getInstance(DirectoryConfProperties directoryConfProperties) {
            if (SyncManager.instance == null)
            {
                SyncManager.instance = new SyncManager(directoryConfProperties);
            }
            return SyncManager.instance;
        }


        // TODO remover o comentário abaixo mais tarde
        /** O snapshot pós sincronizacao deve ser feito com base no snapshot feito antes de sincronizacao + operacoes realizadas. 
         * Uma listagem após todas as operações não é seguro, tendo em vista que qualquer arquivo adicionado pelo usuario, no periodo entre o snapshot atual e até o momento que todos os arquivos tenham sido
         * sincronizados, seria considerado também como sincronizado.
         */

        public void sycn() {
            // TODO mover para um outro local de setup inicial
            // Sincroniza arquivos locais
            // verifica diretorios de snapshot
            this.checkSnapshotDirectories();

            // endenreco do servidor
            string serverAddress = WebDAVProperties.getInstance().getProperty(WebDAVProperties.SERVER_ADDRESS);

            // Facades
            WebDAVClientFacade webDAV = WebDAVClientFacade.getInstance();
            LocalClientFacade localClientFacade = LocalClientFacade.getInstance();

            // diretorio de arquivos pessoais no servidor
            string serverPersonalDir = this.syncDirectoriesProperties.getProperty(DirectoryConfProperties.SERVER_PERSONAL_DIR);
            PathDescriptor serverPersonalDirectory = WebDAVClientFacade.generatePathDescriptor(serverAddress, serverPersonalDir, "");
            LocationController<WebDAVFileProperties> serverPersonalController = new LocationController<WebDAVFileProperties>(webDAV, SyncManager.SERVER_PERSONAL_SNAPSHOT_FOLDER, serverPersonalDirectory);


            // diretorio de arquivos pessoais local
            string personalRootFolder = this.syncDirectoriesProperties.getProperty(DirectoryConfProperties.LOCAL_PERSONAL_DIR);
            PathDescriptor localPersonalDirectory = new PathDescriptor("", personalRootFolder, "");
            LocationController<FileProperties> localPersonalController = new LocationController<FileProperties>(localClientFacade, SyncManager.LOCAL_PERSONAL_SNAPSHOT_FOLDER, localPersonalDirectory);
                       


            // diretorio de arquivos corporativos no servidor
            string serverCorporatelDir = this.syncDirectoriesProperties.getProperty(DirectoryConfProperties.SERVER_CORPORATE_DIR);
            PathDescriptor serverCorporateDirectory = WebDAVClientFacade.generatePathDescriptor(serverAddress, serverCorporatelDir, "");
            LocationController<WebDAVFileProperties> serverCorporateController = new LocationController<WebDAVFileProperties>(webDAV, SyncManager.SERVER_CORPORATE_SNAPSHOT_FOLDER, serverCorporateDirectory);

            // Dados da origem da sincronização - Pasta corporativo
            string corporateRootFolder = this.syncDirectoriesProperties.getProperty(DirectoryConfProperties.LOCAL_CORPORATE_DIR);
            PathDescriptor localCorporateDirectory = new PathDescriptor("", corporateRootFolder, "");
            LocationController<FileProperties> localCorporateController = new LocationController<FileProperties>(localClientFacade, SyncManager.LOCAL_CORPORATE_SNAPSHOT_FOLDER, localCorporateDirectory);



            // Sincronizacao da pasta pessoal e corporativa
            while (WebDAVProperties.getInstance().KeepSyncroning)
            {
                try
                {
                    int pingTries = PING_TRIES;

                    bool pingSucceed = false;

                    while (!pingSucceed && pingTries > 0)
                    {
                        Ping pingSender = new Ping();
                        PingReply reply = pingSender.Send(serverAddress);
                        pingSucceed = reply.Status == IPStatus.Success;
                        pingTries--;
                    }

                    if (pingSucceed)
                    {
                        // sincronizacao dos arquivos pessoais
                        try
                        {
                            webDAV.authenticate(serverPersonalDirectory);
                            this.sync<FileProperties, WebDAVFileProperties>(localPersonalController, serverPersonalController);

                        }
                        catch (WebDAVAuthenticateException e)
                        {
                            Notification authFailed = NotificationFactory.getInstance().getNotification(NotificationMessageKey.ERROR_AUTHENTICATING_ON_DESTINY);
                            this.notifications.Enqueue(authFailed);
                            LogManager.LogMessage("Falha na sincronização de arquivos pessoais:");
                            LogManager.LogError(e);

                        }
                        catch (Exception e)
                        {
                            LogManager.LogMessage("Falha na sincronização de arquivos pessoais:");
                            LogManager.LogError(e);
                            Notification authFailed = NotificationFactory.getInstance().getNotification(NotificationMessageKey.ERROR_SYNCRONIZING_PERSONAL_FOLDER);
                            this.notifications.Enqueue(authFailed);
                        }



                        // sincronizacao dos arquivos corporativo
                        try
                        {
                            webDAV.authenticate(serverCorporateDirectory);
                            this.sync<FileProperties, WebDAVFileProperties>(localCorporateController, serverCorporateController);

                        }
                        catch (WebDAVAuthenticateException e)
                        {
                            Notification authFailed = NotificationFactory.getInstance().getNotification(NotificationMessageKey.ERROR_AUTHENTICATING_ON_DESTINY);
                            this.notifications.Enqueue(authFailed);

                            LogManager.LogMessage("Falha na sincronização de arquivos corporativos:");
                            LogManager.LogError(e);
                        }
                        catch (Exception e)
                        {

                            Notification authFailed = NotificationFactory.getInstance().getNotification(NotificationMessageKey.ERROR_SYNCRONIZING_CORP_FOLDER);
                            this.notifications.Enqueue(authFailed);

                            LogManager.LogMessage("Falha na sincronização de arquivos corporativos:");
                            LogManager.LogError(e);
                        }
                    }
                    else
                    {
                        //Se o server ta off e o user ta online 
                        Notification notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.ERROR_SERVER_NOT_RESPONDING);
                        this.notifications.Enqueue(notification);

                        LogManager.LogMessage("Server off");
                    }
                }
                catch
                {
                    //Se o user ta offline, o ping lança uma exc.
                    Notification notification = NotificationFactory.getInstance().getNotification(NotificationMessageKey.ERROR_SERVER_NOT_RESPONDING);
                    this.notifications.Enqueue(notification);

                    LogManager.LogMessage("Usuário offline");
                }

                for (int i = 0; i < 30 && WebDAVProperties.getInstance().KeepSyncroning; i++)
                    System.Threading.Thread.Sleep(1000);
            }
        }

        /**
         * <summary>
         * Sincroniza os arquivos de um determinado diretório
         * </summary>
         * <param name="syncDirectory">
         * Diretorio a ser sincronizado.
         * </param>
         * <param name="sourceLocaltionController">
         * Dados do local fonte da sincronização.
         * </param>
         * <param name="destinyLocationController">
         * Dados do local destino da sincronização.
         * </param>
         */
        public void sync <SourceType, DestinyType>(LocationController<SourceType> sourceLocaltionController, LocationController<DestinyType> destinyLocationController) where SourceType : IFileProperties, new() where DestinyType : IFileProperties, new()
        {

            // utilizar esta mesma logica para sincronizacao local de do servidor e para os diretorios pessoais e corporativos
    

            ChangeManager<SourceType> sourceChangeManager = new ChangeManager<SourceType>(sourceLocaltionController.FileManager);
            sourceChangeManager.discoverChanges(sourceLocaltionController.SyncDirectory, sourceLocaltionController.SnapshotFolder);
                          
            // obtem operacoes do servidor, atualizando corretamente o snapshot atual
            ChangeManager<DestinyType> destinyChangeManager = new ChangeManager<DestinyType>(destinyLocationController.FileManager);
            destinyChangeManager.discoverChanges(destinyLocationController.SyncDirectory, destinyLocationController.SnapshotFolder);

            // trata conflitos
            // verifica conflitos de updates locais com criacoes, updates, remocoes no servidor
            ConflictSolver<SourceType, DestinyType> updateConflictSolver = 
                new SourceUpdateVSDestinyOperationsConflictSolver<SourceType, DestinyType>(sourceChangeManager.UpdatedItems, sourceChangeManager, destinyChangeManager);
            updateConflictSolver.solveConflicts();

            ConflictSolver<SourceType, DestinyType> createConflictSolver = 
                new SourceCreateVSDestinyOperationsConflict<SourceType, DestinyType>(sourceChangeManager.NewItems, sourceChangeManager, destinyChangeManager);
            createConflictSolver.solveConflicts();

            ConflictSolver<SourceType, DestinyType> removeConflictSolver = 
                new SourceRemovedVSDestinyOperationsConflictSolver<SourceType, DestinyType>(sourceChangeManager.removedItems, sourceChangeManager, destinyChangeManager);
            removeConflictSolver.solveConflicts();


            // Novos arquivos locais
            IDictionary<string, SourceType> sourceNewFilesDictionary =  sourceChangeManager.NewItems;
            // TODO: criar factory para as operações?
            SendSyncOperation<SourceType, DestinyType> sendFileSyncOp = 
                new SendSyncOperation<SourceType, DestinyType>(sourceChangeManager,
                                                                   destinyChangeManager,
                                                                   sourceLocaltionController,
                                                                   destinyLocationController);
            AddToOrUpdateSnapshot snapshotUpdater = AddToOrUpdateSnapshot.getInstance();
            // executa operações para os arquivos
            this.executeOperations<SourceType, DestinyType, SourceType>(sourceNewFilesDictionary.Values, 
                                                                        sendFileSyncOp, 
                                                                        snapshotUpdater);


            // Novos arquivos do servidor
            IDictionary<string, DestinyType> destinyNewFiles = destinyChangeManager.NewItems;
            ReceiveNewSyncOperation<DestinyType, SourceType> receiveFileSyncOperation = 
                new ReceiveNewSyncOperation<DestinyType, SourceType>(sourceChangeManager,
                                                                      destinyChangeManager,
                                                                      sourceLocaltionController,
                                                                       destinyLocationController);
            // executa operações para os arquivos
            this.executeOperations<SourceType, DestinyType, DestinyType>(destinyNewFiles.Values,
                                                                         receiveFileSyncOperation,
                                                                         snapshotUpdater);


            // Arquivos locais atualizados
            IDictionary<string, SourceType> sourceUpdatedFiles = sourceChangeManager.UpdatedItems;
            UpdateSyncOperation<SourceType, DestinyType> updateFileSyncOp =
                new UpdateSyncOperation<SourceType, DestinyType>(sourceChangeManager,
                                                                   destinyChangeManager,
                                                                   sourceLocaltionController,
                                                                   destinyLocationController);
            // executa operações para os arquivos
            this.executeOperations<SourceType, DestinyType, SourceType>(sourceUpdatedFiles.Values,
                                                                        sendFileSyncOp,
                                                                        snapshotUpdater);


            // Arquivos no servidor atualizados
            // recebe os arquivos de volta
            IDictionary<string, DestinyType> destinyUpdatedFiles = destinyChangeManager.UpdatedItems;
            ReceiveUpdatedSyncOperation<DestinyType, SourceType> receiveUpdatedFileSyncOp =
                new ReceiveUpdatedSyncOperation<DestinyType, SourceType>(sourceChangeManager,
                                                                      destinyChangeManager,
                                                                      sourceLocaltionController,
                                                                       destinyLocationController);
            // executa operações para os arquivos
            this.executeOperations<SourceType, DestinyType, DestinyType>(destinyUpdatedFiles.Values,
                                                                         receiveUpdatedFileSyncOp,
                                                                         snapshotUpdater);

            
            // Arquivos deletados localmente
            // remove no servidor
            IDictionary<string, SourceType> localRemovedFiles = sourceChangeManager.removedItems;
            RemoveSyncOperation<SourceType, DestinyType> removeSyncOp =
                new RemoveSyncOperation<SourceType, DestinyType>(sourceChangeManager,
                                                                   destinyChangeManager,
                                                                   sourceLocaltionController,
                                                                   destinyLocationController,
                                                                   NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_REMOVING_DIRECTORY_ON_DESTINY),
                                                                   NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_REMOVING_FILE_ON_DESTINY));
            // executa operações para os arquivos
            RemoveFromSnapshot removeFromSnapshot = RemoveFromSnapshot.getInstance();
            this.executeOperations<SourceType, DestinyType, SourceType>(localRemovedFiles.Values,
                                                                        removeSyncOp,
                                                                        removeFromSnapshot);

            // Arquivos deletados no servidor
            IDictionary<string, DestinyType> destinyRemovedFiles = destinyChangeManager.removedItems;
            RemoveSyncOperation<DestinyType, SourceType> removeOnDestinySyncOp =
                new RemoveSyncOperation<DestinyType, SourceType>(destinyChangeManager,
                                                                          sourceChangeManager,
                                                                          destinyLocationController,
                                                                          sourceLocaltionController,
                                                                          NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_REMOVING_DIRECTORY_ON_SOURCE),
                                                                          NotificationFactory.getInstance().getNotification(NotificationMessageKey.SUCCESS_REMOVING_FILE_ON_SOURCE)
                                                                          );
            // executa operações para os arquivos
            this.executeOperations<DestinyType, SourceType, DestinyType>(destinyRemovedFiles.Values,
                                                                        removeOnDestinySyncOp,
                                                                        removeFromSnapshot);

        }


        // TODO SyncOperation e SnapshotUpdater estão extremamente ligados, seria interessante associa-los em um objeto? Além do ChangeManager e o LocationController juntos oq ue eles formam?
        // TODO documentar este método
        private void executeOperations<ST, DT, OPST>(ICollection<OPST> files,
                                                SyncOperation<OPST, ST, DT> operation, 
                                                SnapshotUpdater snapshotUpdater
                                                )
            where ST : IFileProperties
            where DT : IFileProperties 
            where OPST : IFileProperties
        {
            foreach (OPST fileProperties in files)
            {
                bool succes = false;
                try
                {
                    operation.execute(fileProperties);
                    if (operation.Notification != null)
                    {
                        this.notifications.Enqueue(operation.Notification);
                    }
                    succes = true;
                } catch (Exception e) {
                    this.notifications.Enqueue(NotificationFactory.getInstance().getNotification(NotificationMessageKey.ERROR_SYNCRONIZING));

                    LogManager.LogError(e);
                    
                    // TODO logar o erro!
                }
                if (succes)
                {
                    snapshotUpdater.updateSnapshot<ST>(operation.retrieveSourceFileProperties(), operation.SourceChangeManager, operation.SourceLocationController);
                    snapshotUpdater.updateSnapshot<DT>(operation.retrieveDestinyFileProperties(), operation.DestinyChangeManager, operation.DestinyLocationController);
                }
            }
            
        
        }

        private void updateSnapshot<T>(T fileProperties, ChangeManager<T> changeManager, LocationController<T> locationController) where T : IFileProperties {
            IDictionary<string, T> lastExecutionSnapshot = changeManager.LastExecutionSnapshot;
            lastExecutionSnapshot[fileProperties.getRelativePath()]=fileProperties;
            SnapshotManager<T>.getInstance().saveSnapshot(lastExecutionSnapshot.Values, locationController.SnapshotFolder);
            SnapshotManager<T>.getInstance().deleteOldestSnapshots(1, locationController.SnapshotFolder);
        }


        private void removeSnapshot<T>(string key, ChangeManager<T> changeManager, LocationController<T> locationController) where T : IFileProperties
        {
            IDictionary<string, T> lastExecutionSnapshot = changeManager.LastExecutionSnapshot;
            lastExecutionSnapshot.Remove(key);
            SnapshotManager<T>.getInstance().saveSnapshot(lastExecutionSnapshot.Values, locationController.SnapshotFolder);
            SnapshotManager<T>.getInstance().deleteOldestSnapshots(1, locationController.SnapshotFolder);
        }


        private void checkSnapshotDirectories() {
            Directory.CreateDirectory(SyncManager.LOCAL_CORPORATE_SNAPSHOT_FOLDER);
            Directory.CreateDirectory(SyncManager.LOCAL_PERSONAL_SNAPSHOT_FOLDER);
            Directory.CreateDirectory(SyncManager.SERVER_CORPORATE_SNAPSHOT_FOLDER);
            Directory.CreateDirectory(SyncManager.SERVER_PERSONAL_SNAPSHOT_FOLDER);
            string directory = this.syncDirectoriesProperties.getProperty(DirectoryConfProperties.LOCAL_PERSONAL_DIR);
            Directory.CreateDirectory(directory);
            directory = this.syncDirectoriesProperties.getProperty(DirectoryConfProperties.LOCAL_CORPORATE_DIR);
            Directory.CreateDirectory(directory);
            
        }

    }
}
