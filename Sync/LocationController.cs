﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebDAVContent.Manager;
using WebDAVContent.GUID.Facades;

namespace WebDAVContent.GUID.Sync
{
    /**
     * <summary>
     * Classe de contole da localização e gerencia dos arquivos. 
     * Contém o controlador do local de sincronizacao bem com o local de gerencia de Snapshot deste controlador.
     * </summary>
     */
    class LocationController<T> where T : IFileProperties
    {
        /** <summary> Facade de controle de arquivos </summary> */
        private IFileManagerFacade<T> fileManager;

        public IFileManagerFacade<T> FileManager
        {
            get { return fileManager; }
        }

        /** <summary> Localizacao do snapshots que gerenciam a localização </summary> */
        private string snapshotFolder;

        public string SnapshotFolder
        {
            get { return snapshotFolder; }
        }


        // TODO voltar pra string
        /** <summary> Localizacao do diretorio a ser sincronizado </summary> */
        private PathDescriptor syncDirectory;

        public PathDescriptor SyncDirectory
        {
            get { return syncDirectory; }
        }


        /**
        * <summary>
        * Construtor.
        * </summary>
        * <param name="fileManager">
        *  Controlador de arquivos
        *  </param>
        *  <param name="snapshotFolder">
        *  Pasta do snapshot dos arquivos gerenciados pelo Controlador de arquivos 
        *  </param>
         * <param name="syncDirectory">
         * Diretorio de sincronizacao
         * </param>
        */
        public LocationController(IFileManagerFacade<T> fileManager, string snapshotFolder, PathDescriptor syncDirectory)
        {
            this.fileManager = fileManager;
            this.snapshotFolder = snapshotFolder;
            this.syncDirectory = syncDirectory;
        }

    }
}
