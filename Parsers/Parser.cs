﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WebDAVContent.GUID.Sync;

namespace WebDAVContent.Parsers
{
    public abstract class Parser <T>
    {
        //tag que indica o namespace
        protected static readonly string MULTISTATUS_ELEMENT = "multistatus";
        // tag que indica o caminho do arquivo do servidor
        protected static readonly string PATH_NAME_ELEMENT = "href";

        public abstract T parse(Stream xml, PathDescriptor root);
          
    }
}

