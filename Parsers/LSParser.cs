﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using WebDAVContent.Manager;
using WebDAVContent.Util;
using System.Xml.Linq;
using WebDAVContent.Configuration;
using WebDAVContent.GUID.Sync;

namespace WebDAVContent.Parsers
{
    /**
     * Faz o parser da resposta de um comando de listagem de arquivos (PROPFIND).
     */
    class LSParser : Parser<IDictionary<string, WebDAVFileProperties>>
    {
        /**<summary> tag que indica a lista de propriedades dos status dos arquivos </summary> */
        protected static readonly string FILE_LIST_PROPSTAT_ELEMENT = "propstat";
        /**<summary> taq que contem a lista de várias propriedades dos arquivos </summary> */
        protected static readonly string FILE_LIST_PROP_ELEMENT = "prop";
        /**<summary> tag que indica o status do response do arquivo </summary> */
        protected static readonly string FILE_LIST_PROPERTIE_STATUS = "status";
        /**<summary> tag que indica o nome do arquivo  </summary> */
        protected static readonly string FILE_LIST_PROPERTIE_DISPLAY_NAME = "displayname";
        /**<summary> tag que indica o elemento que contem o caminho relativo do arquivo </summary> */
        protected static readonly string FILE_PROPERTIE_RELATIVE_PATH_ELEMENT = "filerelativepath";
        /**<summary> tag que indica o elemento que contem o caminho absoluto do arquivo </summary> */
        protected static readonly string FILE_PROPERTIE_CREATION_TIME_ELEMENT = "creationdate";
        /**<summary> tag que indica o elemento que contem a data de  modificacao do elemento </summary> */
        protected static readonly string FILE_PROPERTIE_LAST_MODIFIED_TIME_ELEMENT = "getlastmodified";
        /**<summary> tag que indica modificacao do arquivo </summary> */
        protected static readonly string FILE_PROPERTIE_ETAG_ELEMENT = "getetag";
        /**<summary> tag que indica modificacao do arquivo </summary> */
        protected static readonly string FILE_PROPERTIE_LENGTH_ELEMENT = "getcontentlength";
        /**<summary> tag que indica o tipo do resourcetype </summary> */
        protected static readonly string FILE_PROPERTIE_RESOURCE_TYPE = "resourcetype";

        /** Valores possiveis para as propriedades WebDAV */
        // TODO transformar num enum
        /**<summary> valor possível para o resourcetype </summary> */
        protected static readonly string FILE_PROPERTIE_RESOURCE_TYPE_COLLECTION_VALUE = "collection";

        private static LSParser instance = null;

        private LSParser() { 
        
        }


        public static LSParser getInstance() {
            if (instance == null) {
                instance = new LSParser();
            }
            return instance;
        }


        static XNamespace aNamespace = null;

        override
        public IDictionary<string, WebDAVFileProperties> parse(Stream responseStream, PathDescriptor path)
        {
            IDictionary<string, WebDAVFileProperties> webDAVFilesNavigadorList = new Dictionary<string, WebDAVFileProperties>();

            XDocument doc = XDocument.Load(responseStream);

            // Obtem elementos do XML
            IEnumerable<XElement> elements = doc.Elements();

            if (elements.First() != null)
            {
                //Este XML possui apenas um elemento que possui filhos
                XElement first = elements.First();

                //Obtém o namespace
                aNamespace = first.Name.Namespace;

                //Monta o nodo pai para fazer o parser 
                XName multistatusNode = getNameWithServerNamespace(MULTISTATUS_ELEMENT);

                foreach (XElement xElement in doc.Element(multistatusNode).Elements())
                {
                     WebDAVFileProperties wdFileProperties = this.extractFileListProperties(xElement, path);
                     // inclui na lista 
                    webDAVFilesNavigadorList.Add(wdFileProperties.RelativePath, wdFileProperties);
                }
            }


            return webDAVFilesNavigadorList;
        }

        private WebDAVFileProperties extractFileListProperties(XElement xElement, PathDescriptor path)
        {
            WebDAVFileProperties fileProp = new WebDAVFileProperties();
            
            // variavel auziliar para fazer o parse
            XElement aux = null;
            
            // caminho abosluto: <D:href>http://192.168.1.2:333/</D:href>
            aux = xElement.Element(getNameWithServerNamespace(PATH_NAME_ELEMENT));

            //Ajusta os caracteres especiais
            aux.Value = Uri.UnescapeDataString(aux.Value);

            // TODO resolver melhor esta gambs
            string fullPath = aux.Value.Replace("\\", "/");
            if (fullPath.Length > 0 && fullPath.ElementAt(0) == '/') {
                fullPath = fullPath.Substring(1, fullPath.Length - 1);
            }

            string relativePath = fullPath.Replace(path.ABase.Replace("\\", "/"), "");
            if (fullPath.Length == relativePath.Length) {
                string ignoreStartString = path.RootFolder;
                string fullPathBeggining = fullPath.Substring(0, ignoreStartString.Length);
                if (fullPathBeggining.Equals(ignoreStartString))
                {
                    relativePath = relativePath.Substring(ignoreStartString.Length, fullPath.Length - ignoreStartString.Length);
                }
            }
            
            fileProp.FilePathNameURI = aux.Value;
            fileProp.RelativePath = relativePath;

            //TODO Tratar os casos onde há displayname
            if (aux.Value.Length > 1)
                fileProp.DisplayName = aux.Value.Replace('/', ' ');
            else
                fileProp.DisplayName = String.Empty;

            //IEnumerable<XElement> filePropertiesXML = first.Elements(XmlConverterUtil.getNameWithMFPNamespace(XmlConverterUtil.FILE_PROPERTIE_ELEMENT));

            //Busca a elemento propstat e percorre os seus filhos
            foreach (XElement xElementChildren in xElement.Elements(getNameWithServerNamespace(FILE_LIST_PROPSTAT_ELEMENT)))
            {
                //Obtém o status dos response dos arquivos
                aux = xElementChildren.Element(getNameWithServerNamespace(FILE_LIST_PROPERTIE_STATUS));
                fileProp.Status = aux.Value;

                foreach (XElement xElemChildrenProperties in xElementChildren.Elements(getNameWithServerNamespace(FILE_LIST_PROP_ELEMENT)))
                {
                    //Obtém todas as outras propriedades restatntes
                    aux = xElemChildrenProperties.Element(getNameWithServerNamespace(FILE_PROPERTIE_ETAG_ELEMENT));
                    fileProp.ETag = aux.Value;

                    aux = xElemChildrenProperties.Element(getNameWithServerNamespace(FILE_PROPERTIE_CREATION_TIME_ELEMENT));
                    fileProp.CreationTime = DateTime.Parse(aux.Value, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    
                    aux = xElemChildrenProperties.Element(getNameWithServerNamespace(FILE_PROPERTIE_LAST_MODIFIED_TIME_ELEMENT));
                    fileProp.LastModifiedTime = DateTime.Parse(aux.Value, null, System.Globalization.DateTimeStyles.RoundtripKind);

                    aux = xElemChildrenProperties.Element(getNameWithServerNamespace(FILE_PROPERTIE_LENGTH_ELEMENT));
                    if (aux != null && !aux.IsEmpty && aux.Value != null) {
                        fileProp.Length = long.Parse(aux.Value);
                    }

                    aux = xElemChildrenProperties.Element(getNameWithServerNamespace(FILE_PROPERTIE_RESOURCE_TYPE));
                    if (aux != null && !aux.IsEmpty && aux.Value != null)
                    {
                        aux = aux.Element(getNameWithServerNamespace(FILE_PROPERTIE_RESOURCE_TYPE_COLLECTION_VALUE));
                        if (aux != null && aux.Name.LocalName.ToLower().Equals(FILE_PROPERTIE_RESOURCE_TYPE_COLLECTION_VALUE.ToLower()))
                        {
                            fileProp.IsDirectory = true;
                        }
                    }
                }
            }
            
            return fileProp;
        }

        private static XName getNameWithServerNamespace(String elementName)
        {
            // Cria um namespace para o fazer o parse
            return aNamespace + elementName;
        }
    }
}
