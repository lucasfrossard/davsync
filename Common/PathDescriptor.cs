﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Sync
{
    public class PathDescriptor
    {
        /** <summary> Base do endereço </summary> */
        private string aBase;

        public string ABase
        {
            get { return aBase; }
        }

        /** <summary> Pasta raiz </summary> */
        private string rootFolder;

        public string RootFolder
        {
            get { return rootFolder; }       
        }

        /** <summary> Caminho relativo (caminho comum) </summary> */
        private string relativePath;

        public string RelativePath
        {
            get { return relativePath; }
        }

        /**
         * <summary>
         * Construtor
         * </summary>
         * <param name="aBase">
         * Base (opcional)
         * </param>
         * <param name="rootFolder">
         * Pasta taíz
         * </param>
         * <param name="relativePath">
         * Caminho relativo ou comum
         * </param>
         */
        public PathDescriptor(string aBase, string rootFolder, string relativePath) {
            if (aBase == null || rootFolder == null || relativePath == null) {
                throw new ArgumentNullException();
            }
            this.aBase = correctSlash(aBase);
            this.rootFolder = correctSlash(rootFolder);
            this.relativePath = relativePath;
        }

        /**
         * <summary>
         * Construtor
         * </summary>
         * <param name="aBase">
         * Descritor que servira de base para geracao de um novo PathDescriptor que contera a mesma base e pasta raiz deste descritor
         * </param>
         * <param name="relativePath">
         * Caminho relativo ou comum
         * </param>
         */
        public PathDescriptor(PathDescriptor baseDescriptor, string relativePath)
        {
            if (baseDescriptor == null || relativePath == null) {
                throw new ArgumentNullException();
            }
            this.aBase = baseDescriptor.ABase;
            this.rootFolder = baseDescriptor.RootFolder;
            this.relativePath = relativePath;
        }

        public string getFullRootFolderPath() {
            return this.aBase + this.rootFolder;
        }

        public string getFullPath() {
            return this.aBase + this.rootFolder + this.relativePath;
        }

        public static PathDescriptor generateSyncPathFromAbsolutePath(string aBase, string root, string fullpath)
        {
            aBase = correctSlash(aBase);
            root = correctSlash(root);
            fullpath = correctSlash(fullpath);
            string relativePath = fullpath.Replace(aBase + root, "");
            return new PathDescriptor(aBase, root, relativePath);
        }

        private static string correctSlash(string slashed){
            slashed = slashed.Replace(@"\", "/");
            return slashed;
        }

        public override bool Equals(object pathDescriptor)
        {
            bool equals = true;
            if (pathDescriptor is PathDescriptor) {
                PathDescriptor path = (PathDescriptor)pathDescriptor;
                equals = equals && path.RelativePath.Equals(this.RelativePath);
                equals = equals && path.ABase.Equals(this.ABase);
                equals = equals && path.RootFolder.Equals(this.RootFolder);
                return equals;
            } else {
                return false;
            }
        }
    }
}
