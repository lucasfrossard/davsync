﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Common
{
    public abstract class Messages<T> : IMessages<T>
    {
        private IDictionary<T, string> messages = new Dictionary<T, string>();

        // TODO criar um enum ou classe de modo a restringir as entradas possíveis para este método e utilizar o tipo genérico para ter esta restrição
        public string getMessage(T key) {
            return this.messages[key];
        }

        protected void addMessage(T key, string message) {
            this.messages[key] = message;
        }
    }
}
