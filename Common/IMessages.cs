﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebDAVContent.GUID.Common
{
    public interface IMessages<T>
    {
        string getMessage(T key);
    }
}
