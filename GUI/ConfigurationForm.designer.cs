﻿namespace WebDAVContent
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtServPath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtServCorpPath = new System.Windows.Forms.TextBox();
            this.lblEndServPastaCorp = new System.Windows.Forms.Label();
            this.btnSearchCorpFolder = new System.Windows.Forms.Button();
            this.txtCorpFolderPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSearchLocalFolder = new System.Windows.Forms.Button();
            this.txtLocalFolderPath = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtUserPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSaveConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.sairToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verManualDoUsuárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStartSync = new System.Windows.Forms.Button();
            this.btnStopSync = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Endereço do servidor MOBDisk:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(204, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Endereço local da pasta pessoal:";
            // 
            // txtServPath
            // 
            this.txtServPath.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServPath.Location = new System.Drawing.Point(282, 26);
            this.txtServPath.Name = "txtServPath";
            this.txtServPath.Size = new System.Drawing.Size(239, 25);
            this.txtServPath.TabIndex = 3;
            this.txtServPath.Validating += new System.ComponentModel.CancelEventHandler(this.serverAdressValidating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtServCorpPath);
            this.groupBox1.Controls.Add(this.lblEndServPastaCorp);
            this.groupBox1.Controls.Add(this.btnSearchCorpFolder);
            this.groupBox1.Controls.Add(this.txtCorpFolderPath);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnSearchLocalFolder);
            this.groupBox1.Controls.Add(this.txtLocalFolderPath);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtServPath);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(616, 169);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Diretórios principais";
            // 
            // txtServCorpPath
            // 
            this.txtServCorpPath.Location = new System.Drawing.Point(282, 57);
            this.txtServCorpPath.Name = "txtServCorpPath";
            this.txtServCorpPath.Size = new System.Drawing.Size(239, 25);
            this.txtServCorpPath.TabIndex = 10;
            this.txtServCorpPath.Validating += new System.ComponentModel.CancelEventHandler(this.servCorpValidating);
            // 
            // lblEndServPastaCorp
            // 
            this.lblEndServPastaCorp.AutoSize = true;
            this.lblEndServPastaCorp.Location = new System.Drawing.Point(6, 61);
            this.lblEndServPastaCorp.Name = "lblEndServPastaCorp";
            this.lblEndServPastaCorp.Size = new System.Drawing.Size(270, 19);
            this.lblEndServPastaCorp.TabIndex = 9;
            this.lblEndServPastaCorp.Text = "Endereço da pasta corporativa no servidor:";
            // 
            // btnSearchCorpFolder
            // 
            this.btnSearchCorpFolder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchCorpFolder.Location = new System.Drawing.Point(527, 128);
            this.btnSearchCorpFolder.Name = "btnSearchCorpFolder";
            this.btnSearchCorpFolder.Size = new System.Drawing.Size(75, 25);
            this.btnSearchCorpFolder.TabIndex = 8;
            this.btnSearchCorpFolder.Text = "Buscar...";
            this.btnSearchCorpFolder.UseVisualStyleBackColor = true;
            this.btnSearchCorpFolder.Click += new System.EventHandler(this.btnSearchCorpFolder_Click);
            // 
            // txtCorpFolderPath
            // 
            this.txtCorpFolderPath.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorpFolderPath.Location = new System.Drawing.Point(282, 128);
            this.txtCorpFolderPath.Name = "txtCorpFolderPath";
            this.txtCorpFolderPath.Size = new System.Drawing.Size(239, 25);
            this.txtCorpFolderPath.TabIndex = 7;
            this.txtCorpFolderPath.Validating += new System.ComponentModel.CancelEventHandler(this.corpFolderValidating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(228, 19);
            this.label5.TabIndex = 6;
            this.label5.Text = "Endereço local da pasta corporativa:";
            // 
            // btnSearchLocalFolder
            // 
            this.btnSearchLocalFolder.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchLocalFolder.Location = new System.Drawing.Point(527, 92);
            this.btnSearchLocalFolder.Name = "btnSearchLocalFolder";
            this.btnSearchLocalFolder.Size = new System.Drawing.Size(75, 25);
            this.btnSearchLocalFolder.TabIndex = 5;
            this.btnSearchLocalFolder.Text = "Buscar...";
            this.btnSearchLocalFolder.UseVisualStyleBackColor = true;
            this.btnSearchLocalFolder.Click += new System.EventHandler(this.btnSearchLocalFolder_Click);
            // 
            // txtLocalFolderPath
            // 
            this.txtLocalFolderPath.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalFolderPath.Location = new System.Drawing.Point(282, 92);
            this.txtLocalFolderPath.Name = "txtLocalFolderPath";
            this.txtLocalFolderPath.Size = new System.Drawing.Size(239, 25);
            this.txtLocalFolderPath.TabIndex = 4;
            this.txtLocalFolderPath.Validating += new System.ComponentModel.CancelEventHandler(this.localFolderValidating);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtUserPassword);
            this.groupBox2.Controls.Add(this.txtUsername);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(616, 93);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Configuração de conta do usuário";
            // 
            // txtUserPassword
            // 
            this.txtUserPassword.Location = new System.Drawing.Point(125, 57);
            this.txtUserPassword.Name = "txtUserPassword";
            this.txtUserPassword.Size = new System.Drawing.Size(211, 25);
            this.txtUserPassword.TabIndex = 3;
            this.txtUserPassword.UseSystemPasswordChar = true;
            this.txtUserPassword.Validating += new System.ComponentModel.CancelEventHandler(this.passwordValidating);
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(125, 23);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(211, 25);
            this.txtUsername.TabIndex = 2;
            this.txtUsername.Validating += new System.ComponentModel.CancelEventHandler(this.usernameValidating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 19);
            this.label4.TabIndex = 1;
            this.label4.Text = "Senha:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nome de usuário:";
            // 
            // btnSaveData
            // 
            this.btnSaveData.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveData.Location = new System.Drawing.Point(458, 342);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(75, 25);
            this.btnSaveData.TabIndex = 6;
            this.btnSaveData.Text = "Salvar";
            this.btnSaveData.UseVisualStyleBackColor = true;
            this.btnSaveData.Click += new System.EventHandler(this.btnSaveData_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(539, 342);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 25);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajudaToolStripMenuItem,
            this.sobreToolStripMenuItem,
            this.sobreToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(640, 25);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemSaveConfig,
            this.sairToolStripMenuItem,
            this.sairToolStripMenuItem1});
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(65, 21);
            this.ajudaToolStripMenuItem.Text = "Arquivo";
            // 
            // menuItemSaveConfig
            // 
            this.menuItemSaveConfig.Name = "menuItemSaveConfig";
            this.menuItemSaveConfig.Size = new System.Drawing.Size(199, 22);
            this.menuItemSaveConfig.Text = "Salvar Configurações";
            this.menuItemSaveConfig.Click += new System.EventHandler(this.menuItemSaveConfigClick);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(196, 6);
            // 
            // sairToolStripMenuItem1
            // 
            this.sairToolStripMenuItem1.Name = "sairToolStripMenuItem1";
            this.sairToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.sairToolStripMenuItem1.Text = "Sair";
            this.sairToolStripMenuItem1.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verManualDoUsuárioToolStripMenuItem});
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(53, 21);
            this.sobreToolStripMenuItem.Text = "Ajuda";
            // 
            // verManualDoUsuárioToolStripMenuItem
            // 
            this.verManualDoUsuárioToolStripMenuItem.Name = "verManualDoUsuárioToolStripMenuItem";
            this.verManualDoUsuárioToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.verManualDoUsuárioToolStripMenuItem.Text = "Ver Manual do Usuário";
            // 
            // sobreToolStripMenuItem1
            // 
            this.sobreToolStripMenuItem1.Name = "sobreToolStripMenuItem1";
            this.sobreToolStripMenuItem1.Size = new System.Drawing.Size(55, 21);
            this.sobreToolStripMenuItem1.Text = "Sobre";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "MOBDisk";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraçõesToolStripMenuItem,
            this.sairToolStripMenuItem2});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(170, 70);
            // 
            // configuraçõesToolStripMenuItem
            // 
            this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
            this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.configuraçõesToolStripMenuItem.Text = "Configurações...";
            this.configuraçõesToolStripMenuItem.Click += new System.EventHandler(this.btnConfigNotify);
            // 
            // sairToolStripMenuItem2
            // 
            this.sairToolStripMenuItem2.Name = "sairToolStripMenuItem2";
            this.sairToolStripMenuItem2.Size = new System.Drawing.Size(169, 22);
            this.sairToolStripMenuItem2.Text = "Sair";
            this.sairToolStripMenuItem2.Click += new System.EventHandler(this.NotifyExitApp);
            // 
            // btnStartSync
            // 
            this.btnStartSync.Font = new System.Drawing.Font("Segoe UI", 8.830189F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartSync.Location = new System.Drawing.Point(137, 340);
            this.btnStartSync.Name = "btnStartSync";
            this.btnStartSync.Size = new System.Drawing.Size(123, 25);
            this.btnStartSync.TabIndex = 9;
            this.btnStartSync.Text = "Sincronizar...";
            this.btnStartSync.UseVisualStyleBackColor = true;
            this.btnStartSync.Click += new System.EventHandler(this.btnStartSync_Click);
            // 
            // btnStopSync
            // 
            this.btnStopSync.Font = new System.Drawing.Font("Segoe UI", 8.830189F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopSync.Location = new System.Drawing.Point(266, 340);
            this.btnStopSync.Name = "btnStopSync";
            this.btnStopSync.Size = new System.Drawing.Size(82, 25);
            this.btnStopSync.TabIndex = 10;
            this.btnStopSync.Text = "Parar sincronização...";
            this.btnStopSync.UseVisualStyleBackColor = true;
            this.btnStopSync.Click += new System.EventHandler(this.btnStopSync_Click);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 379);
            this.Controls.Add(this.btnStartSync);
            this.Controls.Add(this.btnStopSync);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnSaveData);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnCancelar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ConfigurationForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Configuração do MOBDisk";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormClosed);
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.Resize += new System.EventHandler(this.FormResize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtServPath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSearchLocalFolder;
        private System.Windows.Forms.TextBox txtLocalFolderPath;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtUserPassword;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItemSaveConfig;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem verManualDoUsuárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem2;
        private System.Windows.Forms.Button btnSearchCorpFolder;
        private System.Windows.Forms.TextBox txtCorpFolderPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnStartSync;
        private System.Windows.Forms.Button btnStopSync;
        private System.Windows.Forms.TextBox txtServCorpPath;
        private System.Windows.Forms.Label lblEndServPastaCorp;
    }
}