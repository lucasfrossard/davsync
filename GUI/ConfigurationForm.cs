﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using WebDAVContent.Configuration;
using WebDAVContent.Sync;
using WebDAVContent.GUID.Notifications;
using WebDAVContent.GUID.Sync.Notifications;
using WebDAVContent.GUID.Log;
using WebDAVContent.GUID.Sync.Messages;

namespace WebDAVContent
{
    public partial class ConfigurationForm : Form
    {
        DirectoryConfProperties directoryConfProperties;
        WebDAVProperties webdavProperties;

        System.Threading.Thread notificationThread;
        System.Threading.Thread syncThread;

        private static ConfigurationForm instance;

        private static string CONST_S_LOCATIONS_INFO_FILE = Application.StartupPath + "\\MobLogFile.log";

        private ConfigurationForm()
        {
            InitializeComponent();
            CenterToScreen();
            loadData();

            LogManager.InitializeLogger(CONST_S_LOCATIONS_INFO_FILE); 

            notificationThread = new System.Threading.Thread(Notify.getInstance().consumeNotifications);
            notificationThread.Start();
            //Chama o método que carrega todas as informações das telas
        }

        public static ConfigurationForm getInstance()
        {
            if (instance == null)
                instance = new ConfigurationForm();

            return instance;
        }

        #region Eventos de botões

        private void btnSearchLocalFolder_Click(object sender, EventArgs e)
        {
            //Abre uma janela pra selecionar a pasta
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            //Se quiser a busca por arquivos utilizar esse código:
            //DialogResult result = openFileDialog1.ShowDialog();

            try
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //Obtém o caminho do diretório que o usuário buscou e seta na janela
                    txtLocalFolderPath.Text = folderBrowserDialog1.SelectedPath;
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSearchCorpFolder_Click(object sender, EventArgs e)
        {
            //Abre uma janela pra selecionar a pasta
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            try
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //Obtém o caminho do diretório que o usuário buscou e seta na janela
                    txtCorpFolderPath.Text = folderBrowserDialog1.SelectedPath;
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSaveData_Click(object sender, EventArgs e)
        {
            saveData();
            Hide();
            notifyIcon1.Visible = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Hide();
            notifyIcon1.Visible = true;
        }

        private void btnConfigNotify(object sender, EventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void btnStartSync_Click(object sender, EventArgs e)
        {
            StartSync();

            Hide();
            notifyIcon1.Visible = true;

            notifySystemBalloon(ToolTipIcon.Info, NotificationMessagesPTBR.getInstance().getMessage(NotificationMessageKey.INFO_SYNC_STARTED), "MOBDisk", true, 2500);
        }

        private void StartSync()
        {
            WebDAVProperties.getInstance().KeepSyncroning = true;

            saveDataToFiles();

            if (!(syncThread != null && syncThread.IsAlive))
            {
                syncThread = new System.Threading.Thread(SyncManager.getInstance().sycn);
                syncThread.Start();
            }

            // TODO notificar que o sync esta ativo!
            btnStartSync.Enabled = false;
            btnStartSync.Text = "Sincronizando...";
        }

        private void btnStopSync_Click(object sender, EventArgs e)
        {
            WebDAVProperties.getInstance().KeepSyncroning = false;
            //while (syncThread != null && syncThread.IsAlive) { }]
            if(syncThread != null)
                syncThread.Abort();

            btnStartSync.Enabled = true;
            btnStartSync.Text = "Sincronizar...";
            
            //Notification notification = NotificationFactory.getInstance().getNotification(WebDAVContent.GUID.Sync.Messages.NotificationMessageKey.INFO_APP_SYNC_PAUSE);
            //this.notifications.Enqueue(notification);
            notifySystemBalloon(ToolTipIcon.Info, NotificationMessagesPTBR.getInstance().getMessage(NotificationMessageKey.INFO_APP_SYNC_PAUSE), "MOBDisk", true, 2500);
        }

        #endregion

        #region Eventos do Menu

        private void menuItemExit_Click(object sender, EventArgs e)
        {
            //DialogResult = DialogResult.Cancel;
            //Exit App
            notifyIcon1.Visible = false;
            Environment.Exit(0);
        }

        private void menuItemSaveConfigClick(object sender, EventArgs e)
        {
            saveData();
        }

        #endregion

        #region Eventos de notificação

        public void ShowApplication()
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void NotifyDoubleClick(object sender, MouseEventArgs e)
        {
            ShowApplication();
        }

        // TODO - Melhorar fechamento do programa
        private void NotifyExitApp(object sender, EventArgs e)
        {
            ApplicationExit();
        }

        private void ApplicationExit()
        {
            if (syncThread != null)
                syncThread.Abort();
            if (notificationThread != null)
                notificationThread.Abort();
            
            notifyIcon1.Visible = false;

            System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
            process.Kill();
            Application.Exit();
        }
        

        private void NotifyOpenConfigForm(object sender, EventArgs e)
        {
            ShowDialog();
        }

        public void notifySystemBalloon(ToolTipIcon objType, string balloonTipText, string balloonTipTitle, bool isVisible, int timeout)
        {
            notifyIcon1.BalloonTipIcon = objType;
            notifyIcon1.BalloonTipText = balloonTipText;
            notifyIcon1.BalloonTipTitle = balloonTipTitle;
            notifyIcon1.Visible = isVisible;
            notifyIcon1.ShowBalloonTip(timeout);
        }

        #endregion

        #region Eventos do Form

        private Notifications notifications = Notifications.getInstance();

        private void MainFormLoad(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;

            //Notification notification = NotificationFactory.getInstance().getNotification(WebDAVContent.GUID.Sync.Messages.NotificationMessageKey.INFO_APP_RUNNING);
            //this.notifications.Enqueue(notification);
            
            notifySystemBalloon(ToolTipIcon.Info, "Aplicação foi inicializada...", "MOBDisk", true, 2500);

            LogManager.LogMessage("Aplicação foi inicializada");

            if (isFieldsNotEmpty())
            {
                StartSync();
                notifySystemBalloon(ToolTipIcon.Info, "Sincronizando...", "MOBDisk", true, 2500);

                // TODO notificar que o sync esta ativo!
                btnStartSync.Enabled = false;
                btnStartSync.Text = "Sincronizando...";
            }
        }

        private bool isFieldsNotEmpty()
        {
            return ((txtServPath.Text.Length != 0) && (txtLocalFolderPath.Text.Length != 0) && (txtCorpFolderPath.Text.Length != 0) && (txtServCorpPath.Text.Length != 0) &&
                    (txtUsername.Text.Length != 0) && (txtUserPassword.Text.Length != 0));
        }


        private void FormResize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void MainFormClosed(object sender, FormClosedEventArgs e)
        {
            ApplicationExit();
        }

        #endregion

        #region Salvar/Carregar dados dos arquivos

        private void saveData()
        {
            if (saveDataToFiles())
                MessageBox.Show("Dados salvos com sucesso!", "MOBDisk", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("Ocorreu erro na escrita do arquivo, dados não foram salvos.", "MOBDisk", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SetPropertiesFromFields()
        { 
            String servPath = String.Empty;
            String localPath = String.Empty;
            String corpPath = String.Empty;
            String username = String.Empty;
            String password = String.Empty;
            String servCorpPath = String.Empty;

            //Obtém os dados do formulário
            if (txtServPath != null)
            {
                servPath = txtServPath.Text;
                if (servPath.IndexOf("http://") != -1)
                {
                    servPath = servPath.Substring(7);
                }

                if (servPath.Substring(servPath.Length - 1, 1).Equals("/"))
                    servPath = servPath.Substring(0, servPath.Length - 1);
            }

            if (txtLocalFolderPath != null)
            {
                localPath = txtLocalFolderPath.Text;
                if (!localPath.Substring(localPath.Length-1, 1).Equals("\\"))
                    localPath += "\\";
            }

            if (txtCorpFolderPath != null)
            {
                corpPath = txtCorpFolderPath.Text;
                if (!corpPath.Substring(corpPath.Length - 1, 1).Equals("\\"))
                    corpPath += "\\";
            }

            if (txtUsername != null)
                username = txtUsername.Text;

            if (txtUserPassword != null)
                password = txtUserPassword.Text;

            if (txtServCorpPath != null)
            {
                servCorpPath = txtServCorpPath.Text;

                if (!servCorpPath.Substring(servCorpPath.Length - 1, 1).Equals("/"))
                    servCorpPath += "/";
            }
            //Obtém as instancias
            directoryConfProperties = DirectoryConfProperties.getInstance();
            webdavProperties = WebDAVProperties.getInstance();

            //Guarda os caminhos do diretórios
            directoryConfProperties.setProperty(DirectoryConfProperties.LOCAL_PERSONAL_DIR, localPath);
            directoryConfProperties.setProperty(DirectoryConfProperties.LOCAL_CORPORATE_DIR, corpPath);
            directoryConfProperties.setProperty(DirectoryConfProperties.SERVER_PERSONAL_DIR, username+"/");
            directoryConfProperties.setProperty(DirectoryConfProperties.SERVER_CORPORATE_DIR, servCorpPath);

            //Guarda as configuracoes do usuáario
            //TODO - guardar o pass criptografado
            webdavProperties.setProperty(WebDAVProperties.USERNAME, username);
            webdavProperties.setProperty(WebDAVProperties.PASSWORD, password);

            //Guarda o endereço do server Webdav
            webdavProperties.setProperty(WebDAVProperties.SERVER_ADDRESS, servPath);

            LogManager.LogMessage("LocalPath: " + localPath + " Corp Path: " + corpPath + " ServerPersonalPath: " + username + " ServerCorpPath: " + servCorpPath);
            LogManager.LogMessage("Username: " + username + " ServerAddress: " + servPath);
        }

        private bool saveDataToFiles()
        {
            SetPropertiesFromFields();
            
            try
            {
                //Salva as informaçoes em arquivo
                directoryConfProperties.save(DirectoryConfProperties.DEFAUL_PROPERTIES_FILE_PATH);
                webdavProperties.save(WebDAVProperties.DEFAUL_PROPERTIES_FILE_PATH);
            }
            catch (IOException ex)
            {
                // IO Exception
                MessageBox.Show(ex.Message);
                return false;
            }
            catch (Exception ex)
            {
                // Qualquer outro exception
                MessageBox.Show(ex.Message);
                return false;
            }

            return true;
        }

        public void loadData()
        {
            //Obtém as instancias
            directoryConfProperties = DirectoryConfProperties.getInstance();
            webdavProperties = WebDAVProperties.getInstance();

            directoryConfProperties.load(DirectoryConfProperties.DEFAUL_PROPERTIES_FILE_PATH);
            webdavProperties.load(WebDAVProperties.DEFAUL_PROPERTIES_FILE_PATH);

            //Mostra os dados nos campos
            if (WebDAVProperties.SERVER_ADDRESS != null)
                txtServPath.Text = webdavProperties.getProperty(WebDAVProperties.SERVER_ADDRESS);

            if (DirectoryConfProperties.LOCAL_PERSONAL_DIR != null)
                txtLocalFolderPath.Text = directoryConfProperties.getProperty(DirectoryConfProperties.LOCAL_PERSONAL_DIR);

            if (DirectoryConfProperties.LOCAL_CORPORATE_DIR != null)
                txtCorpFolderPath.Text = directoryConfProperties.getProperty(DirectoryConfProperties.LOCAL_CORPORATE_DIR);

            if (DirectoryConfProperties.SERVER_CORPORATE_DIR != null)
                txtServCorpPath.Text = directoryConfProperties.getProperty(DirectoryConfProperties.SERVER_CORPORATE_DIR);

            if (WebDAVProperties.USERNAME != null)
                txtUsername.Text = webdavProperties.getProperty(WebDAVProperties.USERNAME);

            if (WebDAVProperties.PASSWORD != null)
                txtUserPassword.Text = webdavProperties.getProperty(WebDAVProperties.PASSWORD);
        }

        #endregion

        #region Validação dos dados

        private void serverAdressValidating(object sender, CancelEventArgs e)
        {
            System.Net.IPAddress ot;

            if (txtServPath.Text.Length == 0)
            {
                MessageBox.Show("O endereço do servidor não pode ser vazio.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            /*else if (txtServPath.Text.Contains("http://") || txtServPath.Text.Contains("dav://"))
            {
                MessageBox.Show("O endereço do servidor não deve estar associado a nenhum protocolo, por favor preencha somente com o IP.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }*/
        }

        private void localFolderValidating(object sender, CancelEventArgs e)
        {
            if (txtLocalFolderPath.Text.Length == 0)
            {
                MessageBox.Show("O caminho da pasta local deve ser preenchido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void corpFolderValidating(object sender, CancelEventArgs e)
        {
            if (txtCorpFolderPath.Text.Length == 0)
            {
                MessageBox.Show("O caminho da pasta corporativa deve ser preenchido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void servCorpValidating(object sender, CancelEventArgs e)
        {
            if (txtServCorpPath.Text.Length == 0)
            {
                MessageBox.Show("O endereço da pasta corporativa do servidor deve ser preenchida.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void usernameValidating(object sender, CancelEventArgs e)
        {
            if (txtUsername.Text.Length == 0)
            {
                MessageBox.Show("O nome de usuário deve ser preenchido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void passwordValidating(object sender, CancelEventArgs e)
        {
            if (txtUserPassword.Text.Length == 0)
            {
                MessageBox.Show("A senha deve ser preenchida.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        #endregion
    }
}
