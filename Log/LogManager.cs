﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WebDAVContent.GUID.Log
{
    public class LogManager
    {
        public static void InitializeLogger(string sLogFilePath)
        {
            LogFilePath = sLogFilePath;
        }

        private static string LogFilePath { get; set; }
        private static readonly object _objWriteLocker = new object();

        public static void LogMessage(string sMessage)
        {
            //It restricts code from being executed by more than one thread at the same time. 
            lock (_objWriteLocker)
            {
                TextWriter objWriter = new StreamWriter(LogFilePath, true);
                try
                {
                    objWriter.WriteLine(sMessage + " (at " + DateTime.Now.ToString() + ")");
                }
                finally
                {
                    objWriter.Close();
                }
            }
        }

        public static void LogError(Exception excError)
        {
            //It restricts code from being executed by more than one thread at the same time.
            lock (_objWriteLocker)
            {
                TextWriter objWriter = new StreamWriter(LogFilePath, true);
                try
                {
                    objWriter.WriteLine("Begining error: " + excError.Message + " (at " + DateTime.Now.ToString() + ")");
                    string sTab = "\t";
                    Exception excAuxError = excError;
                    while (excAuxError != null)
                    {
                        objWriter.WriteLine(sTab + "Message error: " + excAuxError.Message);
                        objWriter.WriteLine(sTab + "Message exception:");
                        objWriter.WriteLine(excAuxError.StackTrace.Replace("\r\n", "$NL$").Replace("\n", "$NL$").Replace("\n", "$NL$").Replace("$NL$", "\r\n" + sTab));

                        sTab += "\t";
                        excAuxError = excAuxError.InnerException;
                    }
                }
                finally
                {
                    objWriter.Close();
                }
            }
        }
    }
}
