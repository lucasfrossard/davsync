﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Threading;

using WebDAVContent.Manager;
using System.Windows.Forms;

namespace WebDAVContent
{
    class WEBDav
    {
        /** Endereco do servidor */
        string serverAddress = "http://192.168.1.3/"; // TODO: obter endereço de um arquivo

        /** Endereco do servidor do Samuel */
        string serverAddressSamuelServ = "http://200.150.39.180/";
        string serverAddressLucas = "http://192.168.1.3/";
        String serverAddressViniciusLocal = "http://vinicius-pc:333/WebDavWin/";

        /** Nome de usuario*/
        string userName = "frossard"; // TODO: obter nome de usuario de um arquivo
        string userNameVinicius = "vgpm"; // TODO: obter nome de usuario de um arquivo


        /** Senha */
        string password = "123456"; // TODO: obter senha de um arquivo

        /** Timeout */
        int timeout = 10000; // TODO: obter timeout de um arquivo

        HttpWebRequest generateBaseRequest(String uri, String method)
        {
            HttpWebRequest request = (HttpWebRequest)System.Net.HttpWebRequest.Create(uri);
            //Set the User Name and Password
            request.Credentials = new NetworkCredential(this.userName, this.password);
            request.Method = method;
            request.Timeout = this.timeout;
            return request;
        }

        void Authenticate()
        {

            Console.WriteLine("Authenticate");
            try
            {
                // Cria a requisicao
                // TODO: verificar se este cara pode ser um mebro da classe
                // TODO: verificar conexão com a internet

                HttpWebRequest request = this.generateBaseRequest(this.serverAddress, WebRequestMethods.Http.Get);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream());
                writeTofile(reader, "auth.xml");
                reader.Close();

                // TODO este tratamento serve para todas as outras operações! Fazer com que todas utilizem isto aqui
                // Falta verificar se a conexao foi bem sucedida ou nao.
                if (response.StatusCode.Equals(HttpStatusCode.OK))
                {
                    Console.WriteLine("Conexao bem sucedida.");
                }
                else if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
                {
                    // Autenticacao falhou
                    Console.WriteLine("Autenticacao falhou.");
                    // TODO: Lançar exceção
                }
                else if (response.StatusCode.Equals(HttpStatusCode.Forbidden))
                {
                    // Acesso proibido
                    Console.WriteLine("Autenticacao falhou.");
                }
                else if (response.StatusCode.Equals(HttpStatusCode.NotFound))
                {
                    // Endereco que se quer acessar é invalido
                    Console.ReadLine();
                }

            }
            catch (WebException ex)
            {
                // TODO: fazer tratamento da exceção
                //  Colocar o treco da exceção com response
                Console.WriteLine(ex);

            }
            // TODO: como manter o estado da conexao?
            Console.WriteLine("DONE!");
        }

        void CreateServerFolder()
        {
            Console.WriteLine("Creating folder");
            HttpWebRequest request = (System.Net.HttpWebRequest)HttpWebRequest.Create(this.serverAddressLucas + "/obafakjrkjkaway");
            request.Credentials = new NetworkCredential("frossard", "123456");
            request.Method = WebRequestMethods.Http.MkCol; ;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            String responseString = reader.ReadToEnd();


            reader.Close();
            response.Close();
            // Se pasta já existir?
            // request.Headers.Add("Translate: f"); // Não tire isso daqui!


        }

        void CreateLocalFolder()
        {
            // Make a new file on the C drive. 
            // Criando assim todos os usuários tem previlégios de acesso de read/write
            FileInfo f = new FileInfo(@"C:\Test.txt");
            FileStream fs = f.Create();

            // Use the FileStream object... 

            // Close down file stream. 
            fs.Close();
        }

        void ListFiles()
        {
            // Variables.
            System.Net.HttpWebRequest request;
            System.Net.WebResponse response;
            System.Net.CredentialCache myCredentialCache;

            string strSrcURI;
            string strUserName;
            string strPassword;
            //string strDomain = "Domain";
            string strBody = "";

            byte[] bytes = null;

            System.IO.Stream requestStream = null;
            System.IO.Stream responseStream = null;

            XmlDocument responseXmlDoc = null;
            XmlNodeList displayNameNodes = null;

            try
            {
                // Build the PROPFIND request body.
                strSrcURI = serverAddressLucas;
                strUserName = "frossard";
                strPassword = "123456";

                strBody = "<?xml version='1.0' encoding='utf-8'?>"
                       + "<d:propfind xmlns:d='DAV:'>"
                        + "<d:allprop/>"
                        + "</d:propfind>";

                myCredentialCache = new System.Net.CredentialCache();
                myCredentialCache.Add(new System.Uri(strSrcURI),
                   "Basic",
                   new System.Net.NetworkCredential(strUserName, strPassword)
                   );

                request = (System.Net.HttpWebRequest)HttpWebRequest.Create(strSrcURI);
                request.Credentials = myCredentialCache;
                // Request.Proxy = GlobalProxySelection.GetEmptyWebProxy();

                request.Method = "PROPFIND";

                // Encode the body using UTF-8.
                bytes = Encoding.UTF8.GetBytes((string)strBody);
                request.ContentLength = bytes.Length;
                requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                // Set the content type header.
                //Request.ContentType = "text/xml";

                request.ContentType = "text/xml";
                request.Headers.Add("Charecterset", "UTF-8");
                request.Headers.Add("Brief", "t");
                request.Headers.Add("Depth", "1");
                request.Headers.Add("Translate", "f");
                response = (HttpWebResponse)request.GetResponse();

                // Get the XML response stream.
                responseStream = response.GetResponseStream();
                /*StreamReader reader = new StreamReader(responseStream);
                writeTofile(reader, "propfind.xml");
                reader.Close();*/
                // String propfindXML  = reader.ReadToEnd();
                // FileStream file = new FileStream("C:/temp/propfind.xml", FileMode.CreateNew);
                // StreamWriter streamWriter = new StreamWriter(file);
                // streamWriter.Write(propfindXML);
                // streamWriter.Flush();
                // streamWriter.Close();
                // Create the XmlDocument object from the XML response stream.
                responseXmlDoc = new XmlDocument();
                responseXmlDoc.Load(responseStream);

                displayNameNodes = responseXmlDoc.GetElementsByTagName("D:href");

                if (displayNameNodes.Count > 0)
                {
                    Console.WriteLine("DAV:DisPlay Folders inside " + strSrcURI);
                    for (int i = 0; i < displayNameNodes.Count; i++)
                    {
                        // Display the item's display name.
                        // ResponseXmlDoc.Load(DisplayNameNodes[i].InnerXml);
                        Console.WriteLine(displayNameNodes[i].InnerText);
                        Console.WriteLine("_________ ");
                    }
                }
                else
                {
                    Console.WriteLine("DAV:displayname property not found...");
                }
                // Clean up.
                responseStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                // Catch any exceptions. Any error codes from the PROPFIND
                // method request on the server will be caught here, also.
                Console.WriteLine(ex.Message);
            }

        }

        void writeTofile(StreamReader stream, String fileName)
        {
            String xml = stream.ReadToEnd();
            FileStream file = new FileStream("C:/temp/" + fileName, FileMode.CreateNew);
            StreamWriter streamWriter = new StreamWriter(file);
            streamWriter.Write(xml);
            streamWriter.Flush();
            streamWriter.Close();

        }


        void DownloadFile()
        {
            //Autheticate
            try
            {
                // Cria a requisicao
                // TODO: verificar se este cara pode ser um mebro da classe
                // TODO: verificar conexão com a internet
                HttpWebRequest request = (HttpWebRequest)System.Net.HttpWebRequest.Create(this.serverAddressLucas);
                //Set the User Name and Password
                request.Credentials = new NetworkCredential("vinicius", "123456");
                request.Method = WebRequestMethods.Http.Get;
                request.Timeout = timeout;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // Falta verificar se a conexao foi bem sucedida ou nao.
                if (response.StatusCode.Equals(HttpStatusCode.OK))
                {
                    Console.WriteLine("Conexao bem sucedida.");
                }
                else
                {
                    Console.WriteLine("Conexao falhou.");
                    // TODO: Lançar exceção
                }
                Console.ReadLine();


                string destination = "c:\\temp\\downloadedFile2.txt";

                //Create the buffer for storing the bytes read from the server
                int byteTransferRate = 4096;
                //4096 bytes = 4 KB
                byte[] bytes = new byte[byteTransferRate];
                int bytesRead = 0;
                //Indicates how many bytes were read 
                long totalBytesRead = 0;
                //Indicates how many total bytes were read
                long contentLength = 0;
                //Indicates the length of the file being downloaded

                //Read the content length
                contentLength = Convert.ToInt64(response.GetResponseHeader("Content-Length"));

                //Create a new file to write the downloaded data to
                System.IO.FileStream fs = new System.IO.FileStream(destination, System.IO.FileMode.Create, System.IO.FileAccess.Write);

                //Get the stream from the server
                System.IO.Stream s = response.GetResponseStream();

                do
                {
                    //Read from the stream
                    bytesRead = s.Read(bytes, 0, bytes.Length);


                    if (bytesRead > 0)
                    {
                        totalBytesRead += bytesRead;

                        //Write to file
                        fs.Write(bytes, 0, bytesRead);

                    }

                } while (bytesRead > 0);




                //Close streams
                s.Close();
                s.Dispose();
                s = null;

                fs.Close();
                fs.Dispose();
                fs = null;

                //Close the response
                response.Close();
                response = null;
            }
            catch (WebException ex)
            {
                // TODO: fazer tratamento da exceção
                Console.WriteLine(ex);

            }
        }

        int UploadFile()
        {
            Console.WriteLine("Starting  upload!");
            String uri = this.serverAddress + "uploadmeoe.txt";

            Console.WriteLine("Upload destination: " + uri);
            HttpWebRequest webRequest = this.generateBaseRequest(uri, WebRequestMethods.Http.Put); ;
            HttpWebResponse webResponse;

            string localFile = "c:\\temp\\uploadme2.txt";


            Stream remoteStream = null;
            FileStream localStream = null;

            byte[] buffer = new byte[1024];
            int bytesRead;
            int totalSent = 0;

            //KeyValuePair<Uri, string> keyValuePair = new KeyValuePair<Uri, string>(uri, localFile);
            //_listUploads.Add(keyValuePair);

            try
            {
                // Read the local file
                FileInfo fileInfo = new FileInfo(localFile);
                localStream = fileInfo.OpenRead();

                // Disable write buffer to avoid OutOfMemory situations with large files
                webRequest.AllowWriteStreamBuffering = true;

                // Have to set the ContentLength when AllowWriteStreamBuffering is disabled
                webRequest.ContentLength = localStream.Length;

                // Get the request stream
                remoteStream = webRequest.GetRequestStream();


                // Loop through stream until no bytes are there anymore
                do
                {
                    // bRun = _listUploads.Contains(keyValuePair);
                    Console.WriteLine("Uplading! " + totalSent + " of " + localStream.Length);
                    // Read into the buffer from the remote stream
                    bytesRead = localStream.Read(buffer, 0, buffer.Length);
                    // Write into the file stream
                    remoteStream.Write(buffer, 0, bytesRead);

                    totalSent += bytesRead;

                    //if (UploadProgressEvent != null)
                    // InvokeProgressEvent(UploadProgressEvent, new ProgressEventArgs(bytesRead <= 0, fileInfo.FullName, uri, totalSent, fileInfo.Length));

                } while (bytesRead > 0 && totalSent <= localStream.Length);



                // TODO: Extract possible status values from the response
            }
            catch (Exception e)
            {
                // TODO: Errorhandling
                Console.WriteLine("Exception");
                Console.WriteLine(e.Message);

                totalSent = -1;
            }
            finally
            {

                if (remoteStream != null)
                {
                    remoteStream.Close();
                    remoteStream.Dispose();
                }

                if (localStream != null)
                {
                    localStream.Close();
                    localStream.Dispose();
                }

                // Clear item of the upload list
                //if (_listUploads.Contains(keyValuePair))
                //_listUploads.Remove(keyValuePair);
                Console.WriteLine("Done!");
            }
            try
            {
                webResponse = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return totalSent;


        }

        static void DeleteFile()
        {

        }


        void listLocalFiles()
        {
            this.listLocalFiles("C:/temp/");
        }

        void listLocalFiles(String dir)
        {
            IEnumerable<string> files = Directory.EnumerateFiles(dir);
            foreach (String filePath in files)
            {
                Console.WriteLine(filePath);
                String[] splittedPath = filePath.Split('/');
                String fileName = splittedPath[splittedPath.Length - 1];
                if (fileName.Equals("azim.txt"))
                {
                    Console.WriteLine("É o aazim!");
                    for (int i = 0; i < 3; i++)
                    {
                        FileInfo fileInfo = new FileInfo(filePath);
                        Console.WriteLine("Ultima escrita: " + fileInfo.LastWriteTime);
                        Console.WriteLine("Ultima escrita ticks: " + fileInfo.LastWriteTime.Ticks);
                        Console.WriteLine("Ultima escrita UTC: " + fileInfo.LastWriteTimeUtc);
                        Console.WriteLine("Tamanho: " + fileInfo.Length);
                        Console.WriteLine("Data criação: " + fileInfo.CreationTime);

                        Console.ReadLine();
                        fileInfo.Refresh();

                    }
                }
            }


            IEnumerable<string> directories = Directory.EnumerateDirectories(dir);
            foreach (String d in directories)
            {
                Console.WriteLine(d);
                this.listLocalFiles(d);

            }

        }

    }
}
